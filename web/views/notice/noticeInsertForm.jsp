<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<!DOCTYPE html >
<html>
<head>
<meta charset="UTF-8">
<title>Insert title</title>
<style>
	.outer{
	  width:800px;
   height:500px;
   background:#FADEDE;
   color:black;
   margin-top:50px;
   margin-left:auto;
   margin-right:auto;
	}
	table{
		border:1px solid black;
	}
	.table-area {
      width:450px;
      height:350px;
      margin:0 auto;
   }
   button{
	 background:#37B4EA;
	}
</style>
</head>
<body>
		 <jsp:include page="../common/menubar.jsp"/>  <!-- 여기서도 메뉴바가 보일수있게 인클루드해준다 -->
		
 		<%-- <c:if test="${!empty sessionScope.loginUser and sessionScope.loginUser.userId.equals('admin') }"> --%>
 		<!-- 관리자일 경우 실행 -->
 		
 		
 			<div class="outer">
 				<br>
 				<h2>공지 사항</h2>
 				<hr>
 				<h4>공지 사항 작성</h4>
 				<div>
 					<form action="${applicationScope.contextPath }/insert.no" method="post"> <!-- 전달하는값이 많아서 포스트로 넘긴다 -->
 						<table align="center">
 							<tr>
 								<td>제목</td>
 								<td colspan="3"><input type="text" size="50" name="title"></td>
 							</tr>
 							<tr>
 								<td>작성자</td>														<!-- 작성자에 관리자로 리드온리해서 못고치게 -->											
 								<td>
 								<input type="hidden" value="<c:out value="${sessionScope.loginUser.nNo }"/>" name="uno" >
 								<input type="text" name="writer" value="<c:out value="${sessionScope.loginUser.memberShipNumber }"/>"readonly>
 								</td>
 								<td>작성일자</td>
 								<td><input type="date" name="date"></td>
 							</tr>
 							<tr>
 								<td>내용</td>
 							</tr>
 							<tr>
 								<td colspan="4">
 									<textarea name ="content" cols="60" rows="15" style="resize:none;"></textarea>
 								</td>
 							</tr>
 						</table>
 						<br>
 						<div align="center">
 							<button type="reset">취소하기</button>
 							<button type="submit">등록하기</button>
 						</div>
 					</form>
 				</div>
 			</div>
 		
 	<%-- 	</c:if> --%>
 		
 		<%-- <c:if test="${empty sessionScope.loginUser or !sessionScope.loginUser.userId.equals('admin') }">
 		<!-- 관리자가 아닐경우 실행할곳 -->
 			<c:set var="message" value="잘못된 경로로 접근하셨습니다." scope="request"/>
 			<jsp:forward page="../common/errorPage.jsp"/>
 		
 		</c:if> --%>
</body>
</html>