<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<script>
	var successCode = "${ requestScope.message}";
	var alertMessage = "";
	var redirectPath = "";
		
	switch(successCode) {
	
	case "updateMember" : alertMessage = " 정보 수정이 완료 되었습니다."
						  redirectPath = "${ applicationScope.contextPath}/views/member/individual/in_memberUpdateForm.jsp";
						  break;
						  
	}
	alert(alertMessage);
	location.href = redirectPath;
	
</script>
</body>
</html>