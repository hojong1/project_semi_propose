<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${ pageContext.request.contextPath }" scope="application"/>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<title>Insert title here</title>
<style>

/* 상단 메뉴바 스타일 설정 */
	body {
		background: #FADEDE;
		margin: auto;
	
	}
	.wrap {
		width:100%;
		height: 109px;
	} 
	
	.tob-nav {
		margin: 0 auto;
		height: 109px; 

	}
	.menu {
		font-family: Ruda;
		font-style: normal;
		font-weight: bold;
		font-size: 20px;
		line-height: 100%;
		color: #FFF7F7;
		display:table-cell;
		vertical-align:middel;
		background: #786A6A;
		padding: 30px;
	}
	#menu-dropdown {
		background: #000000;
    	color: #C4C4C4;
   		font-size: 85px;
   		font-weight: bold;
   		cursor: pointer;
		display: table-cell;
		padding:16px;
	}
	.first_text{
		font-size: 40px;
		line-height: 49px;
		color: #FB7171;
	}
	
	#login, #logout {	
		height: 40px;
		background: #D3A0A0;
		border-radius: 5px;
		font-family: SeoulNamsan;
		font-style: normal;
		font-weight: normal;
		font-size: 20px;
		line-height: 20px;
		text-align: center;
		color: #FFFFFF;
		width: 100px;
		
	}
	/* 드랍 메뉴 설정 부분 */

	 .dropdown-content {
		display: block;
	} 
	
	#menu-dropdown:hover, #login:hover {
	background-color: #3e8e41;
	cursor: pointer;
	}
	
	.dropdown-content {
	display: none;
	position: absolute;
	background-color: #786A6A;
	min-width: 300px;
	min-height: 300px;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	z-index: 1;
	}

	.dropdown-content a {
	padding: 30px 30px;
	text-decoration: none;
	display: block;
	font-family: 맑은고딕;
	font-style: normal;
	font-weight: bold;
	font-size: 20px;
	color: white;
	}

	.dropdown-content a:hover {
	background-color: #f1f1f1
	}
	/* 드랍 메뉴 설정 부분 끝 */
	
 /* 메뉴 바 스타일 설정 끝 */	
</style>
</head>
<body>
	
	<!-- nav-area start -->
	<div class="wrap">
		<div class="tob-nav">
			<div class="dropbtn" id="menu-dropdown" onclick="dropdown();">≡</div>
			<div class="menu" style="width:100%;"><label class="first_text">연</label>애부터 <label class="first_text">결</label>혼까지
			<img src="/sm/resources/image/common/flower.png"></div>
			<div class="menu" style="vertical-align: middle; padding: 10px;"><img alt="" src="/sm/resources/image/common/FAQ_icon.png"></div>
			<c:if test="${ empty sessionScope.loginUser}">
			<div class="menu" style="vertical-align: middle;"><button id="login" onclick="goLogin();">Login</button></div>
			</c:if>
			<c:if test="${ !empty sessionScope.loginUser}">
			<div class="menu" style="vertical-align: middle;"><button id="logout" onclick="goLogout();">Logout</button></div>
			</c:if>

			<div class="dropdown-content">
			<a onclick="location='/sm/selectList.bo';">문의게시판</a> 
			<a href="#" onclick="location='/sm/selectFAQList.bo';">자주묻는
				게시판</a> <a href="#" onclick="location='/sm/views/notice/noticeList.jsp';">공지사항
				게시판</a> <a href="#"
				onclick="location='/sm/selectFAQList.bo';">이벤트
				보러가기</a> <a href="#"
				onclick="location='/sm/logout.me';">로그인 페이지로 가기</a>

			</div>
		
		</div>
	</div>
	<script>
		function dropdown(){
			
			$(".dropdown-content").css("display","block");

			$(".dropdown-content").mouseleave(function(){
				$(this).css("display","none");
			});

		}
		
		function goLogout() {
			var check = window.confirm("정말 로그아웃을 하시겠습니까?");
			
			if(check) {
				location.href="${ request.getContextPath() }/sm/logout.me";
			}
		}
		
		function goLogin() {
			location.href = "${ applicationScope.contextPath}/index.jsp";
		}
	
	</script>
	<!-- nav-area end -->
	
	<!-- <script>
	window.onfocus=function(){
	}
	window.onload=function(){
	 window.focus(); // 현재 window 즉 익스플러러를 윈도우 최상단에 위치
	window.moveTo(0,0); // 웹 페이지의 창 위치를 0,0 (왼쪽 최상단) 으로 고정
	window.resizeTo(1000,800); // 웹페이지의 크기를 가로 1280 , 세로 800 으로 고정(확장 및 축소)
	window.scrollTo(0,250); // 페이지 상단 광고를 바로 볼 수 있게 스크롤 위치를 조정
	}
	
	</script> -->
</body>
</html>