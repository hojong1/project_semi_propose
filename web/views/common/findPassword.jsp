<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<style>
 #rectangle355{
	
	width: 500px;
	height: 100px;
	
	background: rgba(251, 248, 248, 0.7);
	border: 1px solid #4486AB;
	box-sizing: border-box;
	}
#dialog{
	width: 500px;
	height: 500px;
	background:#F8D2D2;
}	

#title{
font-family: 맑은고딕;
font-style: normal;
font-weight: bold;
font-size: 24px;
line-height: 24px;
text-align: center;
}

#title2{
font-family: 맑은고딕;
font-style: normal;
font-weight: bold;
font-size: 20px;
line-height: 24px;
text-align: center;
}

#notice{
font-family: 고딕;
font-style: italic;
font-weight: bold;
font-size: 17px;
line-height: 24px;
text-align: center;

color:gray;
} 

</style>
<title>Insert title here</title>
</head>
<body>


	<form id="joinForm"  action="${ applicationScope.contextPath }/pwdSearch.bo" method="post">
	<!-- <form name="joinForm" method="post" action="${ applicationScope.contextPath }/sendmail.bo" > -->
	
		 <div id="dialog" title="공지사항"> 
			<div id="rectangle355">
				<br> &nbsp;<label id="title">비밀번호 찾기</label><br>
				<br> &nbsp;&nbsp;&nbsp;<label id="notice">이메일로 임시비밀번호를
					발급해드립니다.</label>
			</div>
			<br>
			<br> <label id="title2">아이디 : &nbsp;&nbsp;&nbsp;</label>
			<input type="text" name="userId" id="userId" style="width: 200px; height: 30px" class="id"><br>
			<br> <label id="title2">이메일 : &nbsp;&nbsp;&nbsp;</label>
			<input type="text" name="email" id="email" style="width: 200px; height: 30px"
				class="email"><br>
			<br> <input type="text" name="questionfind"
				style="width: 500px; height: 30px" class="questionfind"
				placeholder="자신의 좌우명은?" readonly><br>
			<br> <input type="text" name="questionanswer"
				style="width: 500px; height: 30px" class="questionanswer">
			<br>
			

	<button id="success" onclick="insertMember();">가입</button>	
		 </div> 
	</form>


	<script>    
    
   
    $("#dialog").dialog({
       	 maxWidth:600,
         maxHeight: 500,
         width: 600,
         height: 500,
         modal: true,
         buttons: {
            "확인": function() {
            	
            	console.log("하이");
            	 
            	
            	var alertMsg = "회원님의 임시 비밀번호가 이메일로 발급되었습니다. 해당 이메일로 로그인 하셔서 비밀번호를 변경해주세요";
            	 alert(alertMsg);
            	 
            	$(function(){
            		$("#joinForm")[0].submit(); 
            	})
            	 
            	 location.href="/sm/index.jsp;"
            	 
            	 
            	//$(this).dialog();
                
            	
            },
            "취소": function() {
            	 location.href="/sm/index.jsp;"
                $(this).dialog("close");
            }
        },
        close: function() {
        	}
    	});
 	
    
    	function insertMember(){
    		
    		 $("#joinForm")[0].submit(); 
    		
    	}
   
    </script>

</body>
</html>