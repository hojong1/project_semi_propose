<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	    
<c:set var="contextPath" value="${ pageContext.request.contextPath }" scope="application"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<title>Insert title here</title>

<style> 
 body{



background: #FADEDE;
 }
 

 
#rectangle{
position: absolute;
width: 796px;
height: 100%;
left: 115px;
top: 143.92px;
font-size: 25px;
line-height: 29px;
background: #FFFFFF;
}

	div.top {
		
        width: 30%;
     	
        box-sizing: border-box;
        
       border: 4px groove #bcbcbc;
    }
    div.bottom {
     
        width: 35%;
     	
        box-sizing: border-box;
        
       border: 4px groove #bcbcbc;
    }
	div#innerBox{
	margin:30px;
	 border: 4px groove #bcbcbc;
	}
	#success{
		width:130px;
		height:80px;
		margin-left:650px;
		font-size:18px;
	}
	input[type="checkbox"]{
	width:25px;
	height:25px;
	}
</style>
</head>
<body>
	<jsp:include page="/views/common/menubar.jsp"/>
	<div id="rectangle">

		<p style="text-align:center;">약관동의</p>
		<div class="top">가입약관</div>
			<input type='checkbox' id="chk_info" value="insertAgree" >가입약관에 동의합니다.
		<div style="overflow:scroll; width:700px; height:200px; font: bold italic 0.7em 돋움체;" id="innerBox">
			제1조 (목적) 이 약관은 주식회사 연애부터결혼까지이 운영하는 온라인 연결 웹페이지( 이하 “페이지”라 한다) 에서 제공하는 인터넷 관련 서비스(이하 “서비스”란 한다)를 이용함에 있어 연결과 이용자의 권리,의무 및 책임사항을 규정함을 목적으로 합니다. 제2조 (정의) 1. “페이지”란 연결이 재화 또는 용역을 이용자에게 제공하기 위하여 컴퓨터 등 정보통신 설비를 이용하여 재화 또는 용역을 거래할 수 있도록 설정한 가상의 영업장을 말하며, 아울러 웹 페이지
			</div>
		<div class="bottom">개인정보처리방침안내</div>
			<input type='checkbox' id="chk_info2" value="insertAgree2" >개인정보 처리방침에 동의합니다.
		<div style="overflow:scroll; width:700px; height:200px; font: bold italic 0.7em 돋움체;" id="innerBox">
			‘연결는(이하”회사”는) 고객님의 개인정보를 중요시하며, ‘정보통신망 이용촉진 및 정보보호”에 관란 법률을 준수하고 있습니다. 회사는 게인정보처리방침을 통하여 고객님께서 제공하시는 개인정보가 어떠한 용도와 방식으로 이용되고 있으며, 개인정보보호를 위하여 어떠한 조치가 취해지고 있는지 알려드립니다. 회사는 개인정보처리방침을 개정하는 경우 웹사이트 공지사항(또는 개별공지)을 통하여 공지할 것입니다. *본 방침은 : 2020.08.12 부터 시행됩니다. 01. 수집하는 개언정보항목	</div>
	<button id="success" onclick="insertMember();">회원정보 등록</button>
	</div>
	
	
	
	<script>
	function insertMember(){
		if($("input:checkbox[id='chk_info']").is(":checked")==true){
			if($("input:checkbox[id='chk_info2']").is(":checked")==true){
				location.href ="${applicationScope.contextPath}/views/member/individual/in_memberJoinForm.jsp";
			}
			else{
				window.alert("개인정보처리방침에 동의하셔야합니다.");
			}
		}else{
			window.alert("가입약관에 동의하셔야합니다.");
		}	
	}
	
	</script>
</body>
</html>