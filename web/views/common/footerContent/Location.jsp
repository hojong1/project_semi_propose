<jsp:directive.page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.content {
	float: left;
	margin-left: 20px;
	margin-top: 20px;
	border-radius: 10px;
	border: 2px solid black;
	min-width: 820px;
	min-height: 400px;
}

.inner {
	margin-left: 20px;
}
</style>
</head>
<body>
	<div>
		<jsp:include page="FContentHeader.jsp" />
		<jsp:include page="sidebar_common.jsp" />
		<div class="content">
			<div class="inner">
				<h2 style="display: inline-block;">위치</h2>
				<div style="display: inline-flex;">
					<p>서울 강남구 테헤란로14길 6 남도빌딩 2층, 3층, 4층</p>
					<br>
					<br>

				</div>
				<div>
					<img src="/sm/resources/image/dongyub/location.png">
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../footer.jsp" />
</body>
</html>