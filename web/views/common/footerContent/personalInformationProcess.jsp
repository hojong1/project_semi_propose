<jsp:directive.page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html >
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>

/*이용약관 스타일 설정*/
body {
	width:100%;
	height:100%;
}

.content {
	float: left;
	margin-left: 20px;
	margin-top: 20px;
	border-radius: 10px;
	border: 2px solid black;
	min-width: 820px;
	min-height: 400px;
}

#contents{
	float:right;
	margin-right:30px;
}
/*이용약관 스타일 설정  끝 */
</style>
</head>
<body>
	<jsp:include page="FContentHeader.jsp"></jsp:include>
	<jsp:include page="sidebar_common.jsp"/>
	<div class="content">
		<h2>개인정보처리방침</h2>
		<div id="contents" style="overflow: scroll; width: 700px; height: 400px;">
			<h3>면책 조항 :</h3>
			이 요약은 개인 정보 보호 정책의 일부가 아니며 법적 문서가 아닙니다. 전체 개인 정보 보호 정책을 이해하기위한
			간단한 참고 자료입니다. 개인 정보 보호 정책에 대한 사용자 친화적 인 인터페이스라고 생각하십시오. 무료 지식 운동에
			참여하기 위해 개인 정보를 제공 할 필요가 없다고 믿기 때문에 다음을 수행 할 수 있습니다. 계정을 등록하지 않고 연결
			사이트 를 읽거나 편집하거나 사용하십시오 . 이메일 주소 또는 실명 을 제공하지 않고 계정 을 등록하십시오 . 우리는 연결
			사이트가 어떻게 사용되는지 이해하여 더 나은 사이트를 만들기 위해 다음과 같은 경우에 몇 가지 정보를 수집합니다. 확인 공공
			기여를 . 계정을 등록 하거나 사용자 페이지를 업데이트하십시오. 연결 사이트를 사용하십시오 . 이메일을 보내 거나 설문
			조사에 참여 하거나 피드백을 제공하십시오 . 우리는 다음을 약속합니다. 이 개인 정보 보호 정책에서 귀하의 정보가 사용되거나
			공유 되는 방법을 설명합니다 . 귀하의 정보를 안전하게 유지하기 위해 합리적인 조치를 사용합니다 . 마케팅 목적으로 귀하의
			정보를 판매 하거나 제 3 자와 공유 하지 마십시오 . 만 공유 등에 관해서는, 제한된 상황에서 귀하의 정보를 연결 사이트를
			개선 ,하는 법률을 준수 하거나하는 당신과 다른 사람을 보호합니다 . 연결 사이트의 유지, 이해 및 개선과 법적 의무와
			일치하는 가능한 한 최단 시간 동안 데이터 를 보관합니다 . 주의 : 추가 한 모든 콘텐츠 또는 연결 사이트에 대한 변경
			사항은 공개적으로 영구적으로 사용할 수 있습니다 . 로그인하지 않고 콘텐츠를 추가하거나 연결 사이트 를 변경하는 경우 해당
			콘텐츠 또는 변경 사항은 사용자 이름이 아닌 당시 사용 된 IP 주소에 공개적으로 영구적으로 귀속됩니다. 우리의 자원 편집자
			및 기고자 커뮤니티는 자치 단체입니다. 커뮤니티에서 선택한 연결 사이트의 특정 관리자는 최근 기여에 대한 비공개 정보에 대한
			제한된 액세스 권한을 부여하는 도구를 사용하여 연결 사이트를 보호하고 정책을 시행 할 수 있습니다.</div>
	</div>
	<!-- 이용약관 내용 끝-->

	<jsp:include page="/views/common/footer.jsp" />
</body>
</html>