<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<style type="text/css">
	.sidebar{
		margin:0;
		float:left;
		width:300px;
		height: 300px;
	}
	.btns{
		margin-top: 60px;
		font-size: 18px;
		font-weight:bold;
		
	}
	a {
	text-decoration: none;
	color:black;
	}

</style>
</head>
<body>
	<div class="sidebar">
	 	<div class="btns">
	 		<a href="/sm/views/common/footerContent/Location.jsp">위치</a>
	 	</div>
	 	<div class="btns">
	 		<a href="/sm/views/common/footerContent/yakgwan.jsp">이용약관</a>
	 	</div>
	 	<div class="btns">
	 		<a href="/sm/views/common/footerContent/introducePage.jsp">회사 소개</a>
	 	</div>
	 	<div class="btns">
	 		<a href="/sm/views/common/footerContent/personalInformationProcess.jsp">회원정보처리방침</a>
	 	</div>
	</div>
</body>
</html>