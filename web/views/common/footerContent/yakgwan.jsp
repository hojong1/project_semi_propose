<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"/>
<%@ taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
   .content {
      float: left;
      margin-left:20px;
      margin-top:20px;
      border-radius: 10px;
      border: 2px solid black;
      min-width: 820px;
      min-height:400px;      
   }
   .inner {
      margin-left: 20px;
   }
</style>

</head>
<body>
   <jsp:include page="FContentHeader.jsp"></jsp:include>
   <jsp:include page="sidebar_common.jsp"/>
   <div class="content">
      <div class="inner">
         <h2>이용약관</h2>
         <div>
            <p>연결에 오신것을 환영합니다! <br>
               본 약관은 회원님의 연결 이용에 적용되며, 아래 설명된 연결 서비스에 관한 정보를 제공합니다.</p>
               <br><br>
            </p>
            <p>데이터 정책 <br><br>
            연결서비스를 제공하기 위해서는 회원님의 정보를 수집하고 이용해야 합니다.<br>
            데이터 정책은 연결 제품에서 당사가 정보를 수집, 이용하고 공유하는 방법에 대해 설명합니다. <br>
            또한 연결 개인정보 보호 및 보안 설정을 포함해 회원님의 정보를 관리할 수 있는 여러방법에 대해 설명합니다.<br>
            연결을 이용하려면 데이터 정책에 동의해야 합니다.   
            
            
         </div>
      </div>
   </div>
   <jsp:include page="../footer.jsp"></jsp:include>
</body>
</html>