<jsp:directive.page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<title>Insert title here</title>
<style>
.footer {
	position: absolute;
	width: 100%;
	left: 0;
	height: 100px;
	background-color: #d38888;
	margin-top: 33%;
}

.button-wrap {
	margin-top: 0.5%;
	text-align: center;
}

.footer-button {
	border: none;
	background: none;
	color: #333333;
}

.sect {
	margin-left: 1%;
	margin-right: 1%;
}

.detailed-info {
	text-align: center;
	font-size: 10pt;
	color: #666666;
}

.footer-button:hover {
	cursor: pointer;
}
</style>
</head>
<body>
	<div class="footer">
		<div class="button-wrap">
			<label
				style="float: left; margin-left: 2%; font-size: 20pt; font-weight: bold; color: #a9a9a9">연결</label>
			<button class="footer-button" onclick="map();">위치</button>
			<label class="sect">|</label>
			<button class="footer-button" onclick="introduce();">회사소개</button>
			<label class="sect">|</label>
			<button class="footer-button" onclick="QnA();">고객센터</button>
			<label class="sect">|</label>
			<button class="footer-button" onclick="personalInformationProcess();">개인정보처리방침</button>
			<label class="sect">|</label>
			<button class="footer-button" onclick="yakgwan();">이용약관</button>
			<label class="sect">|</label>
		</div>
		<div class="detailed-info">
			<p>
				주소: 서울특별시 강남구 테헤란로 14길 6 남도빌딩 4층 전관 / 사업자번호: 105-87-99999 / 상호:
				(주)연결 / 대표: 김준형/ 개인정보관리책임자: 김준형/<br> <label
					style="float: left; margin-left: 1%; font-size: 8pt; font-weight: bold; color: #fff">연애에서
					결혼까지</label> <label>통신판매업신고번호: 제 2020-서울-9999호 / 전화번호:
					010-0000-1111 / 이메일: wnsgud@gmail.com</label>
			</p>
		</div>
	</div>
	<script>
	
	function map(){
		 location.href="/sm/views/common/footerContent/Location.jsp" 
	}
	function introduce(){
		 location.href='/sm/views/common/footerContent/introducePage.jsp'
	}
	function QnA(){
		 location.href='/sm/views/board/FrequentlyAskedQuestionsBoard.jsp'
	}
	function personalInformationProcess() {
		location.href='/sm/views/common/footerContent/personalInformationProcess.jsp'
	}
	function yakgwan(){
		 location.href='/sm/views/common/footerContent/yakgwan.jsp'
	}
	</script>

</body>
</html>