<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	.outer {
	 width:1000px;
   height:500px;
   background:#FADEDE;
   color:black;
   margin-top:50px;
   margin-left:auto;
   margin-right:auto;
   
	}
	table {
		border:1px solid black;
		text-align:center;
	}
	.table-area {
		width:650px;
		height:350px;
		margin:0 auto;
	}
	.search-area {
		width:650px;
		margin:0 auto;
	}
	button{
	 background:#37B4EA;
	}
	input[id*="answer"]{
	display:none;
	}
	input[id*="answer"]+label{
	display:block;
	padding:10px;
	border:1px solid #232188;
	border-bottom:0;
	color:#fff;
	font-weight:900;
	background:#3634a5;
	cursor:pointer;
	/*position:relative;*/
	}
	input[id*="answer"]+label em{
	/*position:absolute;
	top:50%;
	right:10px;
	width:30px;
	height:30px;
	margin-top:-15px;
	display:inline-block;
	background:url("/sm/resources/image/jaemin/arrow.png") 0 0 no-repeat;*/
	}
	input[id*="answer"]+label+div{
	max-height:130px;
	/*transition:all .35s;
	overflow:hidden;*/
	background:#ebf8ff;
	font-size:18px;
	margin-bottom:-15px;
	}
	input[id*="answer"]+label+div p {
	/*display:inline-block;*/
	padding:0px;
	margin-top:-5px;
	}
	input[id*="answer"]:checked+label+div{
	/*max-height:200px;*/
	}
	input[id*="answer"]:checked+label em{
	/*background-position:0 -30px;*/}
	
</style>
</head>
<body>

	<jsp:include page="/views/common/menubar.jsp" />

	<div class="outer">
		<br>
		<h2 class="black">자주묻는 질문</h2>
		<hr>

		<h4 class="gray">질문한 답변에서 발췌합니다.</h4>
		
		
			<!-- <div class="accordion">
			<table align="center" id="listArea">
			
				<c:forEach var="n" items="${ requestScope.list }" >
				
				<tr>
					<th width="700px" height="30px">
					
					<input type="checkbox" name="accordion" id="answer">
					<label for="answer01"><c:out value="${  n.faqTitle }" /><em></em></label>
					<div><p><c:out value="${ n.faqContent }" /></p></div>
					
					</th>	
				</tr>
				</c:forEach>	
		  </table>
		</div>-->
		
		
		<!--  <div class="table-area">
			<table align="center" id="listArea">
				<tr>
					<th width="300px" height="30px">글번호</th>
					<th width="300px" height="30px">글제목</th>
					<th width="300px" height="30px">작성자</th>
					<th width="300px" height="30px">조회수</th>
					<th width="300px" height="30px">작성일</th>
					
				</tr>
				<c:forEach var="n" items="${ requestScope.list }">
					<tr>
						<td><c:out value="${ n.faqNo }" /><input type="checkbox" id="answer"></td>
						<td><c:out value="${ n.faqTitle }" /></td>
					 <td><c:out value="${ n.faqWriter }" /></td> 
						<td><c:out value="${ n.faqPageviews }" /></td>
						<td><c:out value="${ n.faqWriteDate }" /></td>
					</tr>
				</c:forEach>
			</table>
		</div> -->
		
		<c:forEach var="n" items="${ requestScope.list }">
		
		<div class="accordion">
			<input type="checkbox" name="accordion" id="answer">
			<label for="answer"><c:out value="${ n.faqTitle }" /><em></em></label>
			<div><p><c:out value="${ n.faqContent }" /></p></div><br><br>
			
		</div>
		</c:forEach>
		
		<div class="paging-area" align="center">
			<button onclick="location.href='${applicationScope.contextPath}/selectFAQList.bo?currentPage=1'"><<</button>
			
			<c:if test="${ requestScope.pi.currentPage <= 1 }">
				<button disabled><</button>
			</c:if>
			<c:if test="${ requestScope.pi.currentPage > 1 }">
				<button onclick="location.href='${applicationScope.contextPath}/selectFAQList.bo?currentPage=<c:out value="${ requestScope.pi.currentPage - 1 }"/>'"><</button>
			</c:if>
			
			<c:forEach var="p" begin="${ requestScope.pi.startPage }" end="${ requestScope.pi.endPage }" step="1">
				<c:if test="${ requestScope.pi.currentPage eq p }">
					<button disabled><c:out value="${ p }"/></button>
				</c:if>
				<c:if test="${ requestScope.pi.currentPage ne p }">
					<button onclick="location.href='${applicationScope.contextPath}/selectFAQList.bo?currentPage=<c:out value="${ p }"/>'"><c:out value="${ p }"/></button>
				</c:if>
				
			</c:forEach>
			
			
			
			<c:if test="${ requestScope.pi.currentPage >= requestScope.pi.maxPage }">
				<button disabled>></button>
			</c:if>
			<c:if test="${ requestScope.pi.currentPage < requestScope.pi.maxPage }">
				<button onclick="location.href='${applicationScope.contextPath}/selectFAQList.bo?currentPage=<c:out value="${ requestScope.pi.currentPage + 1 }"/>'">></button>
			</c:if>
			
			<button onclick="location.href='${applicationScope.contextPath}/selectFAQList.bo?currentPage=<c:out value="${ requestScope.pi.maxPage }"/>'">>></button>
		</div>
		
		<div class="search-area" align="center">
			<select id="searchCondition" name="searchCondition">
				<option value="writer">작성자</option>
				<option value="title">제목</option>
				<option value="content">내용</option>
			</select> <input type="search" class="but">
			<button type="submit" class="but">검색하기</button>
			<c:if test="${ !empty sessionScope.loginUser and sessionScope.loginUser.userId.equals('admin') }">
				<button onclick="location.href='/sm/views/board/FAQBoardInsertForm.jsp'"
					class="but">작성하기</button>
			</c:if>
		</div>

	</div>
	<script>
      $(function(){
         $("#listArea td").mouseenter(function(){
            $(this).parent().css({"background":"darkgray","cursor":"pointer"});
         }).mouseout(function(){
            $(this).parent().css({"background":"white"});
         }).click(function(){
            var num = $(this).parent().children("input").val();
            
            console.log(num);
            
            location.href = "${applicationScope.contextPath}/selectOneFAQ.no?num="+num;
         });
      });
   
      $("body").on("click", "[id^=answer]", function(event) { 
        $(this).parent().css({
        	"background-position":"0 -30px"});  
         
      });
      
   </script>
	<div>
		


	</div>


</body>
</html>
