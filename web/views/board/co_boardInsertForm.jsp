<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
   .outer{
       width:1000px;
   height:500px;
   background:#FADEDE;
   color:black;
   margin-top:50px;
   margin-left:auto;
   margin-right:auto;
   }
   table {
      border:1px solid black;
   }

   .tableArea {
      width:800px;
      height:350px;
      margin-left:auto;
      margin-right:auto;
   }
   	button{
	 background:#37B4EA;
	}
</style>

</head>
<body>
   <jsp:include page="../common/menubar.jsp"/>
   
  <%--  <c:if test="${ !empty sessionScope.loginUser }"> --%>
   <div class="outer">
      <br>
      <h2>문의 게시판</h2>
      <hr>
      <h4>글 작성</h4>
      <div class="tableArea">
         <form action="${ applicationScope.contextPath }/insert.bo" method="post">
            <table>
               <tr>
                  <td>주제</td>
                  <td>
                     <select name="category">
                        <option value="10">공통</option>
                        <option value="20">업체 등록 문의</option>
                        <option value="30">업체 탈퇴 문의</option>
                        <option value="40">수수료 문의</option>
                        
                     </select>
                  </td>
                  <td>게시글 암호</td>
                  <td>
                  	<input type="password">
                  </td>
               </tr>
               <tr>
                  <td>제목 </td>
                  <td colspan="3"><input type="text" size="58" name="title"></td>
               </tr>
               <tr>
                  <td>내용 </td>
                  <td colspan="3">
                     <textarea name="content" cols="60" rows="15" style="resize:none;"></textarea>
                  </td>
               </tr>
            </table>
            <br>
            <div align="center">
               <button type="reset">취소하기</button>
               <button type="submit">등록하기</button>
            </div>
            
         </form>
         
      </div>
   </div>
  <%--  </c:if> --%>
  
  
   <%-- <c:if test="${ empty sessionScope.loginUser }">
      <c:set var="msg" value="잘못된 경로로 접근하셨습니다." scope="request"/>
      <jsp:forward page="../common/errorPage.jsp"/>
   </c:if> --%>
</body>
</html>