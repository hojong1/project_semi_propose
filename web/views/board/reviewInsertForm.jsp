<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<title>Insert title here</title>
<style>
	.outer {
		width:1000px;
		height:650px;
		background:#FADEDE;
		color:black;
		margin-top:50px;
		margin-left:auto;
		margin-right:auto;
	}
	table {
		border:1px solid black;
		width:700px;
		height:600px;
		background:#C4C4C4;
		
	}
	.btn-area {
		width:150px;
		margin:0 auto;
		
	}
	#titleImgArea {
		width:350px;
		height:200px;
		border:2px dashed white;
		text-align:center;
		display:table-cell;
		vertical-align:middle;
	}
	#titleImgArea:hover, #contentImgArea1:hover, 
	#contentImgArea2:hover, #contentImgArea3:hover {
		cursor:pointer;
	}
	#contentImgArea1, #contentImgArea2, #contentImgArea3 {
		width:150px;
		height:100px;
		border:2px dashed white;
		text-align:center;
		display:table-cell;
		vertical-align:middle;
	}
	.poto{
		
		font-family: Ruda;
		font-style: normal;
		font-weight: bold;
		font-size: 15px;
	
	}
	button{
		background:#37B4EA;
		color:white;
	}
	#content{
		width:450px;
	}
	#title{
	width:300px;
	}
	#bt{
		width:150px;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
	
	<%-- <c:if test="${ !empty sessionScope.loginUser }"> --%>
	
	<div class="outer">
		<br>
		<h2>게시판</h2>
		<hr>
		<h4>리뷰 게시판 작성</h4>								 <!-- 내가 전송하는 데이터가 파일이 포함되어있다면 필수로작성 -->
		<form action="${ applicationScope.contextPath }/insert.tn" method="post" encType="multipart/form-data">
			<div class="insert-area">
				<table align="center">
					<tr>
						<td width="100px"class="poto">제목</td>
						<td colspan="3" ><input type="text" name="title"id="title"></td>

					</tr>
					<tr>
						<td width="100px"class="poto">게시글 암호</td>
						<td colspan="3" ><input type="password" name="bt"id="bt"></td>

					</tr>
					<tr>
						<td class="poto">사진 등록</td>
						<td colspan="3">
							<div id="titleImgArea">
								<img id="titleImg" width="500" height="200">
							</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<div id="contentImgArea1">
								<img id="contentImg1" width="180" height="150">
							</div>
						</td>
						<td>
							<div id="contentImgArea2">
								<img id="contentImg2" width="180" height="150">
							</div>
						</td>
						<td>
							<div id="contentImgArea3">
								<img id="contentImg3" width="180" height="150">
							</div>
						</td>
					</tr>
					<tr>
						<td width="100px" class="poto">게시글</td>
						<td colspan="3">
							<textarea name="content" id="content" rows="5" cols="50" style="resize:none;"></textarea>
						</td>
					</tr>
					
				</table>
				<h5 align="center">* 빈칸에 공백란이 없도록 작성해주세요. *</h5>
				<div id="fileArea">											<!-- onchange의 구문으로 이미지를 체인지함 -->
					<input type="file" id="thumbnailImg1" name="thumbnailImg1" onchange="loadImg(this, 1)">
					<input type="file" id="thumbnailImg2" name="thumbnailImg2" onchange="loadImg(this, 2)">
					<input type="file" id="thumbnailImg3" name="thumbnailImg3" onchange="loadImg(this, 3)">
					<input type="file" id="thumbnailImg4" name="thumbnailImg4" onchange="loadImg(this, 4)">
				</div>
			</div>
			
			<br>
			
			<div class="btn-area">
				<button type="reset">취소하기</button>
				<button type="submit">작성완료</button>
			</div>
		</form>
		
	</div>
<%-- 	</c:if> --%>



<%-- 	<c:if test="${ empty sessionScope.loginUser }">
		<c:set var="msg" value="잘못된 경로로 접근하셨습니다." scope="request"/>
		<jsp:forward page="../common/errorPage.jsp"/>
	</c:if> --%>
	
	
	
	<script>
		$(function() {
			$("#fileArea").hide();//아래있던 안이쁜 인풋타입을 숨기기		
			
			$("#titleImgArea").click(function(){//사진 공간을 클릭했을때 인풋타입이 실행되도록 서로 연결해둠
				$("#thumbnailImg1").click();
			});
			$("#contentImgArea1").click(function(){
				$("#thumbnailImg2").click();
			});
			$("#contentImgArea2").click(function(){
				$("#thumbnailImg3").click();
			});
			$("#contentImgArea3").click(function(){
				$("#thumbnailImg4").click();
			});
		});
		
		function loadImg(value, num) {
			if(value.files && value.files[0]) {/* value에 있는 files [0]번쨰에 있으면 실행 */
				var reader = new FileReader();
				
				reader.onload = function(e) { //사진을 문자열로 바꿔서 저장해놈
					switch(num) {						/* target은 임시로 저장한다는것 */
					case 1 : $("#titleImg").attr("src", e.target.result); break;
					case 2 : $("#contentImg1").attr("src", e.target.result); break;
					case 3 : $("#contentImg2").attr("src", e.target.result); break;
					case 4 : $("#contentImg3").attr("src", e.target.result); break;
					}
				}
				
				reader.readAsDataURL(value.files[0]);
			}
		}
	</script>
</body>
</html>
























