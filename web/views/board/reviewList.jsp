<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html >
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<style>
		.outer{
			width:1000px;
			height:1000px;
			background:#FADEDE;
			color:black;
			margin-top:50px;
			margin-left:auto;
			margin-right:auto;
		}
		.thumbnail-area{
			width:760px;
			height:550px;
			margin:0 auto;
		}
		.search-area{
			width:420px;
			margin-bottom: 20px;
		}
		button{
		background:#37B4EA;
		color:white;
		}
		#re{
			width:700px;
			height:200px;
			background:#C1C1C1;
		}
		#cate{
		width:700px;
		margin:0 auto;
		}
		.category{
			margin-right: 5%;
			margin-left: 5%;
		
		
		}
		.content{
			background:white;
			color:black;
			width:600px;
			height:150px;
			margin:0 auto;
			
		}
		.gory{
			margin-right: 5%;
			margin-left: 5%;
		}
	</style>

</head>
<body>
   <%-- <jsp:include page="../common/menubar.jsp"/> <!-- 여기도 매뉴바가 보일수있게 인클루드 해줌 --> --%>

	<div class="outer">
		<br>
		<h1 align="center" style="background:#786A6A; color:white;">리얼후기!</h1>
		
		<div class="thumbnail-area">
		<table align="center" id="listArea">
		
				<tr>
					<th class="th"><div id="cate"><span class="category">글번호</span>
										<span class="category">닉네임</span>
										<span class="category">이벤트 평점</span>
										<span class="category">조회수</span>
										<span class="category">작성일</span></div></th>
				</tr>
				
				<tr>
						<th><div id="re"><span class="gory">3</span>
										<span class="gory">선스문스섹스</span>
										<span class="gory">★★★★★</span>
										<span class="gory">3</span>
										<span class="gory">2020-09-03</span>
										<hr>
							<div class="content" type="button">제목: 여자친구와 낭만적인 시간
								<hr>
								<div><h5 align="left">너무너무 즐거운시간이였습니다~감사합니다 <br>여자친구도 너무 좋아하네요!다음에 또 이용할께요!</h5></div>
							</div>
							</div></th>
						
				</tr>
				<th><hr></th>
				<tr>
						<th><div id="re"><span class="gory">2</span>
										<span class="gory">즉석만남최고</span>
										<span class="gory">★★★★☆</span>
										<span class="gory">8</span>
										<span class="gory">2020-09-02</span>
										<hr>
							<div class="content" type="button">제목: 즉석만남으로 만난 여자친구
								<hr>
								<div><h5 align="left">사랑하는 사람에겐 여기서 이벤트하세요!!<br>
														즉석만남으로 만났지만 정말 사랑합니다!!<br>
													이게 사랑인가 싶고 결혼까지하고싶어요!!!</h5></div>
							</div>
							</div></th>
						
				</tr>
				<th><hr></th>
				<tr>
						<th><div id="re"><span class="gory">1</span>
										<span class="gory">쮸녕</span>
										<span class="gory">★★★★☆</span>
										<span class="gory">15</span>
										<span class="gory">2020-09-01</span>
										<hr>
							<div class="content" type="button">제목: 여자친구에게 최고의 선물!
								<hr>
								<div><h5 align="left">이벤트업체를 별로 선호하지 않았지만 감동을 줄 아이디어가 고갈돼서 해봤습니다!<br>
														기대이상의 퀄리티에 저마져 감동했습니다!!<br>
													연결 정말 고맙고 사랑합니다!!우리 오래가자 !!</h5></div>
							</div>
							</div></th>
						
				</tr>
				
				
				
				
			<%-- 	<c:forEach var="b" items="${ requestScope.list }">
				
					<tr>
						<td><c:out value="${ b.bno }"/></td>
						<td><c:out value="${ b.cName }"/></td>
						<td><c:out value="${ b.bTitle }"/></td>
						<td><c:out value="${ b.bevt }"/></td>
						<td><c:out value="${ b.bCount }"/></td>
						<td><c:out value="${ b.bDate }"/></td>
					</tr>
					
					
				</c:forEach> --%>
			</table>
		</div>
		
	</div>



</body>
</html>