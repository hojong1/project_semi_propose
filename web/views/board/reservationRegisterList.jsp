<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="java.sql.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	  
<%
        request.setCharacterEncoding("utf-8");
 		String id = (String)session.getAttribute("userId");
        String year = request.getParameter("year");
        String month = request.getParameter("month");
        String day = request.getParameter("day");
        String paymentmethod = request.getParameter("paymentmethod");
        String date ="";
        date += year;
        date += month;
        date += day;
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>예약 리스트</title>
<script type="text/javascript">
function regChk(){
 var id = document.regform.id.value;
 var name = document.regform.name.value;
 if(id.length==0 || id==null){
  alert("아이디를 입력하세요!!");
  document.regform.id.focus();
  return false;
 } 
 if (name.length==0 || name==0){
  alert("이름을 입력하세요!!");
  document.regform.name.focus();
  return false;
 }
 var idx = regform.roomname.options.selectedIndex;
 if(regform.roomname.options[idx].value == "방을 선택하시오"){
 	alert("방을 선택하세요!");
 	regform.roomname.focus(); 
 	return false;
 }
}
 </script>
 <style>
  #table1 {
    width: 70%;
    border-collapse: collapse;
    
  }
  th {
  background-color: #E2A58F;
  color: white;
	}
	th, td {
    border: 1px solid #bcbcbc;
    padding: 5px 10px;
    text-align: left;
  	padding: 8px;
  	font-size:18px;
  }
  table,h2,h5,a{
  margin-left:30px;
  
  }
table {
  border-collapse: collapse;
  
  width: 15%;
}

	tr{
	
	height:50px;
	}
	tr:nth-child(even){background-color: #f2f2f2}
	tr:nth-child(odd){background-color: #9BDE5F}
	
</style>
</head>
<body>
	<jsp:include page="/views/common/menubar.jsp"/>
	<br><br>
	<h2>** <%=year %> 년 <%=month %> 월 <%=day %> 일의  예약 현황  **</h2><br>
	<table border="1" id="table1" >
		<tr>
		<th> 예약 번호</th>
		<th> 예약자 ID </th>
		<th> 예약자 이름</th>
		<th> 신청 날짜 </th>
		<th> 결제 방식</th>
		</tr>
		
	<% 
		
		Connection con = null;
		Statement stmt = null;
			Class.forName("oracle.jdbc.driver.OracleDriver");
			String url = "jdbc:oracle:thin:@localhost:1521:xe";
			con = DriverManager.getConnection(url,"SEMIPROJECT","SEMIPROJECT");
			
	
		stmt = con.createStatement();
		String sql = "SELECT * FROM EVENTRESERVATION WHERE RESERVATIONDATE='"+date+"'";
		ResultSet rs = stmt.executeQuery(sql);
		int row = rs.getRow();
		String payment[]={"신용카드","무통장 입금","계좌 이체"};
		String paymentname="";
		String out_opt="";
		int n=0;
		int n1=0; 
		int n2=0;
			if(rs != null){
			
			while(rs.next()){
			
				
				
		
	%>
	<tr>
		<td> <%=rs.getString("RESERVATIONNUMBER") %> </td>
		<td> <%=rs.getString("ID") %> </td>
		<td> <%=rs.getString("USERNAME") %> </td>
		<td> <%=rs.getString("RESERVATIONDATE") %> </td>
		<td><%=rs.getString("PAYMENTMETHOD") %></td>
	</tr>
	<%
		
	
		if(paymentname.equals(payment[0]))
			n++;
		if(paymentname.equals(payment[1]))
			n1++;
		if(paymentname.equals(payment[2]))
			n2++;
			
			}
		}
			
		
	%> 

	</table>
	<br />
	<h2>** 예약 작성 ** </h2>
	<h5>예약 목록과 중복되는 방은 선택하실 수 없습니다.</h5>
	<form name="regform" method="post" action="/sm/reservationOk.me" onsubmit="return regChk()">
		<table id="table2">
		<tr><td>예약자 이름 : &nbsp;&nbsp;&nbsp; <input type="text" name="name" maxlength="20"value=""><br></td></tr>
		<tr><td>예약자 ID : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<!-- <input type="text" name="userId" maxlength="20" value="<%=id %>" readonly="readonly"> -->
		<input type="text" name="userId" maxlength="20" >
		</td></tr>
		<tr><td>예약자전화번호 : <input type="text" name="phone" maxlength="20" > </td></tr>
		<tr><td>결제 방식: <select name="paymentmethod" >
					<option value="결제 방식 선택하세요" selected>결제 방식 선택하세요</option>
				    <% if(n==0){%>
					<option value="<%=payment[0] %>"><%=payment[0] %></option>
					<%} %>
					<% if(n1==0){%>
					<option value="<%=payment[1] %>"><%=payment[1] %></option>
					<%} %>
					<% if(n2==0){%>
					<option value="<%=payment[2] %>"><%=payment[2] %></option>
					<%} %>	 
			   </select></td></tr>
		<tr><td>예약 날짜: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="regdate" maxlength="20" value="<%=date%>" readonly="readonly"></td></tr>
		<tr><td> <input type="submit" value="예약 신청" width="30px">&nbsp;&nbsp; |&nbsp;&nbsp; <input type="reset" value="다시 작성하기"> </td></tr>
		</table>
	</form> 
	<br />
	<a href="../common/reservation.html">날짜 선택으로 돌아가기</a>
 	<!-- <% if(stmt != null)
			stmt.close();
		if(con != null)
			con.close();
	%>  -->
</body>
</html>