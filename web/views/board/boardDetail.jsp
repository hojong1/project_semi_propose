<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<meta charset="UTF-8">
<style>

	.outer {
		width:1000px;
		height:500px;
		background:#FADEDE;
		color:black;
		margin-top:50px;
		margin-left:auto;
		margin-right:auto;
	}
	td {
		border:1px solid black;
	}
	.table-area {
		border:1px solid black;
		
		width:800px;
		height:350px;
		margin:0 auto;
	}
	#content {
		height:230px;
	}
	.reply-area {
		width:800px;
		color:white;
		background:black;
		margin:0 auto;
	}
	  button{
	 background:#37B4EA;
	}
</style>
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
	<div class="outer">
		<br>
		<h2 align="center">문의사항</h2>
		<div class="table-area">
			<table align="center" width="800px">
				<tr>
					<td>분야</td>
					<td><span><c:out value="${ requestScope.board.cName }"/></span></td>
					<td>제목</td>
					<td colspan="3"><span><c:out value="${ requestScope.board.qTitle }"/></span></td>
				</tr>
				<tr>
					<td>작성자(회원번호)</td>
					<td><span><c:out value="${ requestScope.board.memberShipNumber }"/></span></td>
					<td>조회수</td>
					<td><span><c:out value="${ requestScope.board.qPageView }"/></span></td>
					<td>작성일</td>
					<td><span><c:out value="${ requestScope.board.qWriteDate }"/></span></td>
				</tr>
				<tr>
					<td colspan="6">내용</td>
				</tr>
				<tr>
					<td colspan="6"><p id="content"><c:out value="${ requestScope.board.qContent }"/></p></td>
				</tr>
			</table>
		</div>
		<div align="center">
			<button onclick="location.href='${applicationScope.contextPath}/selectList.bo'">목록으로 돌아가기</button>
		</div>
		
		<div class="reply-area">
			<div class="reply-write-area">
				<table align="center">
					<tr>
						<td>댓글 작성</td>
						<td><textarea rows="3" cols="80" id="replyContent"></textarea></td>
						<td><button id="addReply">댓글 등록</button></td>
					</tr>
				</table>
			</div>
			<div id="replySelectArea">
				<table id="replySelectTable" border="1" align="center">
					<tbody></tbody>
				</table>
			</div>
		</div>
		
	</div>
	<script>
		$("#addReply").click(function(){
			var writer = '<c:out value="${sessionScope.loginUser.membershipNumber}"/>';
			var bid = '<c:out value="${requestScope.board.qNo}"/>';
			var content = $("#replyContent").val();
			
			console.log(writer);
			console.log(bid);
			console.log(content);
			
			$.ajax({
				url: "${applicationScope.contextPath}/insertReply.bo",
				data: {writer: writer, content: content, bid: bid},
				type: "post",
				success: function(data) {
					
					var $replySelectTable = $("#replySelectTable tbody");
					$replySelectTable.html('');
					
					for(var key in data) {
						var $tr = $("<tr>");
						var $writerTd = $("<td>").text(data[key].membershipNumber).css("width", "100px");
						var $contentTd = $("<td>").text(data[key].refContent).css("width", "400px");
						var $dateTd = $("<td>").text(data[key].qWriteDate).css("width", "200px");
						
						$tr.append($writerTd);
						$tr.append($contentTd);
						$tr.append($dateTd);
						
						$replySelectTable.append($tr);
					}
				},
				error: function(err) {
					console.log("댓글 작성 실패!");
				}
			})
			
		});
	</script>
</body>
</html>

































