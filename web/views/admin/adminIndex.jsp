<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	html,body {
		height:100%;
	}
	#wrap {
		width:100%;
		height:100%;
	}
	#logoutBtn{
       padding: 15px;
       display:inline-block;
       text-align:center;
       color:white;
       height:25px;
       width:100px;
       font-size: 20px;
   }
  
   .admin-console {
    	float:left;
   		background-color: #211a1a;
   		color:white;
   		height:100%;
   		width:21.25%;
   }
   
   label {
      line-height: 40px;
      margin-top: 15px;
      font-weight:bold;
      font-size: 18px;
   }
   

  .admin-menu {
     color:white
  }
  a {
     text-decoration: none;
     color:white;
  }
  .admin-btn{ 
   margin-left: 0px;
   padding:10px; 
   font-size: 12pt;
  }
  #welcome {
  	height:5%;
  }
</style>
</head>
<body>
	<jsp:include page="adminMenubar.jsp"/>
      
   <div id="wrap">
	 	<div style=" background: #645555; height:100%; width:20%; float:left;" >
	   </div>
		 <img id="background" src="/sm/resources/image/junhyong/bg4.png"style="height:100%; float: left;">
		<!-- admin-console starts  -->
	   <div class="admin-console">
	      <div id="welcome">
	         <img src="/sm/resources/image/junhyong/admin.png" style=" float: left; width: 100px;">
	         <br><br>
	         <label style="color:#ff8585; margin-left: 15px;">김선중 님 반갑습니다.</label>
         </div>
	         <div id="logoutBtn" style="float:right;"><label>로그아웃</label></div>
	      <br clear="both">
	      <div id="admin-menu" style="background-color: #645555; height:70%" >
	         <br>
	         <label class="admin-btn"><a href="#">※관리자 설정</a></label>
	         <br> <br>
	         <label class="admin-btn"><a href="#">※이용자페이지로 이동</a></label>
	         <br> <br>
	         <label class="admin-btn" ><a href="#">※답변 대기문의</a></label>
	      </div>
		</div>
	</div>
</body>
</html>