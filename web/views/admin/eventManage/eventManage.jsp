<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	body {
	width:100%;
	height:100%;
		background-color: #fadede;
	}
	.outer {
		width:60%;
		height:100%;
		background:white;
		color:black;
		margin-left:20%;
		margin-right:20%;
	}
	table {
		border:1px solid black;
		text-align:center;
	}
	.table-area {
		width:100%;
		min-height:400px;
		max-height:100%;
		margin:0 auto;
	}
	.logout {
		margin-left:95%;
		background-color: #d3a0a0;
		border:none;
		border-radius: 10px;
		padding: 10px;
	}
	.sorting {
		width: 100%;
		height:40px;
		display:table;
		background-color:black;
		color: white;
	}
	
	.sort {
		display:table-cell;
		text-align:center;
		line-height:60px;
	}
	#searching{
		margin-top:2%
	}
	.navbar{
	float:left; 
	height: 650px; 
	width: 10%; 
	background-color:#262020;
	}
	.navs{
		color:white;
		font-size: 20px;
		padding: 20px;
		font-weight: bolder;
	}
</style>
</head> 
 <jsp:include page="../adminMenubar.jsp"/>
<body>
	
	<div class="outer">
		<div class="sorting"> 
			<div class="sort"><label>회원번호 순</label></div>
			<div class="sort"><label>구매액 순</label></div>
			<div class="sort"><label>리뷰 순</label></div>
			<div class="sort"><label>가입일 순</label></div>
			<div class="sort"><label>최근 접속일 순</label></div>
			<div class="sort"><label>주문 진행중</label></div>
		</div>
		<div style="float: left;">
		<h2 style="margin-top:10%; margin-bottom:0;">회원관리</h2><br>
		<label style="font-weight: bold;">회원정보조회</label>
		</div>
		<div align="center" id="searching">
			<input type="search" name="searchValue" value="이벤트 등록코드 검색">
			<button type="submit" style ="background-color: #d3a0a0;color:white; font-weight:bold; border: none; border-radius:5px;">검색</button>
		</div>
			<br><br>
		<div class="table-area">
			<table align="center" id="listArea">
				<tr>
					<th width="200px">등록코드</th>
					<th width="200px">업체명</th>
					<th width="200px">표시대분류</th>
					<th width="250px">이벤트명</th>
				</tr>
				<c:forEach var="b" items="${ requestScope.list }">
					<tr>
						<td><c:out value="${ b.bno }"/></td>
						<td><c:out value="${ b.cName }"/></td>
						<td><c:out value="${ b.bTitle }"/></td>
						<td><c:out value="${ b.nickName }"/></td>
						<td><c:out value="${ b.bCount }"/></td>
						<td><c:out value="${ b.bDate }"/></td>
					</tr>
				</c:forEach>
			</table>
		</div>
		
		<div class="paging-area" align="center">
			<button onclick="location.href='${applicationScope.contextPath}/selectList.bo?currentPage=1'"><<</button>
			
			<c:if test="${ requestScope.pi.currentPage <= 1 }">
				<button disabled><</button>
			</c:if>
			<c:if test="${ requestScope.pi.currentPage > 1 }">
				<button onclick="location.href='${applicationScope.contextPath}/selectList.bo?currentPage=<c:out value="${ requestScope.pi.currentPage - 1 }"/>'"><</button>
			</c:if>
			
			<c:forEach var="p" begin="${ requestScope.pi.startPage }" end="${ requestScope.pi.endPage }" step="1">
				<c:if test="${ requestScope.pi.currentPage eq p }">
					<button disabled><c:out value="${ p }"/></button>
				</c:if>
				<c:if test="${ requestScope.pi.currentPage ne p }">
					<button onclick="location.href='${applicationScope.contextPath}/selectList.bo?currentPage=<c:out value="${ p }"/>'"><c:out value="${ p }"/></button>
				</c:if>
			</c:forEach>
			
			
			
			<c:if test="${ requestScope.pi.currentPage >= requestScope.pi.maxPage }">
				<button disabled>></button>
			</c:if>
			<c:if test="${ requestScope.pi.currentPage < requestScope.pi.maxPage }">
				<button onclick="location.href='${applicationScope.contextPath}/selectList.bo?currentPage=<c:out value="${ requestScope.pi.currentPage + 1 }"/>'">></button>
			</c:if>
			
			<button onclick="location.href='${applicationScope.contextPath}/selectList.bo?currentPage=<c:out value="${ requestScope.pi.maxPage }"/>'">>></button>
		</div>
	</div>
	<br clear="both">	
	
</body>
</html>





















