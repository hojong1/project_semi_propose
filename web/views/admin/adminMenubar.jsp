<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${ pageContext.request.contextPath }" scope="application"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<title>Insert title here</title>
<style>
   body {
      background-color:#fadede;
      background-size:cover;
   }
   .wrap{
      display: table;
      background-color: #7c6262;
      width:100%;
      height:80px;
   }
      .menu {
      display: table-cell;
      line-height:40px;
      border: none;
      align-content: center;
      font-weight: bold;
      background:#D3A0A0;
      color:#753a3a;
      text-align:center;
      margin-top:20px;
      margin-left: 60px;
      margin-bottom:20px;
      float: left;
      width:120px;
      height:40px;
      border-radius: 25px;
   }
   .nav {
      width:80%;
      margin-left:25%; 
     
   }
   #title{ 
   	margin-left:10%;
   }
   a {
	text-decoration: none;
	color:white;
	}
</style>
<script>
	function toIndex() {
		location.href="/sm/views/admin/adminIndex.jsp"
	}
</script>
</head>
<body>  
	<!-- nav-area starts -->
   <div class="wrap">
   		<div id= "title">
     	 	<img id= "titleLine" src="/sm/resources/image/junhyong/title.png" onclick="toIndex()" style="width:230px">
   			<img id= "titleFlower" src="/sm/resources/image/junhyong/flower.png">
   		</div>
      <div class="nav">
         <div class="menu"><a href="/sm/selectList.mo">회원관리</a></div>
         <div class="menu"><a href="/sm/selectList.co">업체관리</a></div>
         <div class="menu"><a href="/sm/views/admin/boardManage/boardManage.jsp">게시판관리</a></div>
         <div class="menu"><a href="/sm/views/admin/reservationManage/reservationManage.jsp">예약관리</a></div>
          <div class="menu"><a href="/sm/views/admin/couponManage/coupon.jsp">쿠폰관리</a></div>
          <div class="menu"><a href="/sm/views/admin/eventManage/eventManage.jsp">이벤트관리</a></div>
          <div class="menu"><a href="/sm/views/admin/transaction/transactionHistory.jsp">거래내역 조회</a></div>
          <div class="menu"><a href="/sm/selectFAQList.bo">공지사항 게시판</a></div>
      </div>
   </div>
   <!-- nav-area ends -->
  
	

	
</body>
</html>