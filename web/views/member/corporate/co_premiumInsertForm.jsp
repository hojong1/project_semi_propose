<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html >
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	.outer {
	 width:1000px;
   height:500px;
   background:#FADEDE;
   color:black;
   margin-top:50px;
   margin-left:auto;
   margin-right:auto;
   
	}
	.insert-area{
	 width:700px;
     height:350px;
   	  background:#FADEDE;
   color:black;
    margin-left:auto;
   margin-right:auto;
  
	}
	#tt{
	width:150px;
	
	}
	label{
	
		background:#C4C4C4;
		border:1px solid black;
	
	}
	.la{
		width:150px;
		
		
		
	}
	button{
		background:#37B4EA;
		color:white;
	}
</style>

</head>
<body>
		<jsp:include page="/views/common/menubar.jsp"/>
	
		<form action="${ applicationScope.contextPath }/insert.tn" method="post" encType="multipart/form-data">
		<div class="outer">
		<br>
		<h2>이벤트</h2>
		<hr>
		<h4>프리미엄 등록</h4>
		
			<div class="insert-area"  align="center" >
			<br>
			<label class="la"> 업체명 : </label>
			<input type="text" id="tt">
			<br>
			<br>
			<label class="la"> 결제인 이름 : </label>
			<input type="text" id="tt">
			<br>
			<br>
			<label class="la"> 예약자 연락처 : </label>
			<input type="text" placeholder="010-0000-0000" id="tt">
			<br>
			<br>
			<br>
			<br>
			<label class="la"> 결제 은행 : </label>
			 <select name="category">
                        <option value="10">신한은행</option>
                        <option value="20">국민은행</option>
                        <option value="30">우리은행</option>
                        <option value="40">카카오뱅크</option>
                        <option value="50">삼성페이</option>
                        </select>
			<br>
			
			<label class="la"> 결제 방식 : </label>
			<select name="category">
                        <option value="10">신용카드</option>
                        <option value="20">체크카드</option>
                        <option value="30">계좌이체</option>
                        <option value="40">휴대폰결제</option>
                        </select>
			</div>
			
			<div class="btn-area" align="center">
				<button type="reset" onclick="location.href='views/member/corporate/co_EventInsertForm.jsp'">취소하기</button>
				<button type="submit" onclick="location.href='views/member/corporate/co_EventInsertForm.jsp'">결제하기</button>

			</div>
		</div>
		
		
		
		</form>
		
</body>
</html>




