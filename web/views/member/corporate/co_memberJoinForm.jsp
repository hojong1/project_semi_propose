<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<style> 
 body{



background: #FADEDE;
 }

 .menubar{
     position: absolute;
width: 100%;
height: 100.91px;
left: 0px;
top: 0px;

background: #786A6A;
border: 1px solid #000000;
box-sizing: border-box;
box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
 }
 
#rectangle{
position: absolute;
width: 1500px;
height: 100%;
left: 115px;
top: 143.92px;
font-size: 25px;
line-height: 29px;
background: #FFFFFF;
}
	div#innerBox{
	margin:30px;
	 border: 4px groove #bcbcbc;
	}
	#success{
		width:15%;
		margin-left:650px;
	}
	#rectangle2{
	position: absolute;
	width: 75%;
	height: 537px;
	left: 131px;
	top: 167px;

	background: #FFFEFE;
	border: 10px solid #F2F2F2;
	box-sizing: border-box;
	}
	 #idCheck {
      background:orangered;
      border-radius:5px;
      width:80px;
      height:25px;
      text-align:center;
   }
   #idCheck:hover{
      cursor:pointer;
   }   
   td {
      text-align:right;
   }
   tr{
   	margin:5px;
   }
   #success{
   
   position: absolute;
width: 94px;
height: 40px;
left: 40px;
top: 85%;
}
  #cancel{
  position: absolute;
width: 93px;
height: 40px;
left: 900px;
top: 85%;
  }
  input[type="radio"]{
  	width:25px;
	height:25px;
  }
</style>

</head>
<body>

	<jsp:include page="/views/common/menubar.jsp"/>
	
	<div id="rectangle">
		<h2 style="text-align:center;">회원정보등록</h2>
		<div id="rectangle2">
		<form id="joinForm" action="${ applicationScope.contextPath }/insertCorp.me" method="post">
			<table align="left">
				<tr>
					<td height="50px">*아이디</td>
					<td><input type="text" maxlength="13" name="userId" id="userId" style="width:200px; height:30px"></td>
					<td width="200px"><div id="idCheck" style="width:150px; height:30px">중복확인</div></td>	
				</tr>
				<tr>
					<td height="50px">*비밀번호</td>
					<td><input type="password" maxlength="13" name="pwd" id="pwd" style="width:200px; height:30px"></td>	
					
				</tr>
				<tr>
					<td height="50px">*비밀번호확인</td>
					<td><input type="password" maxlength="13" name="pwd2" id="pwd2" style="width:200px; height:30px"></td>	
				</tr>
				<tr>
					<td height="50px">*사업자명</td>
					<td><input type="text" maxlength="13" name="userName" id="userName" style="width:200px; height:30px"></td>	
				</tr>
				
				<tr>
               		<td height="50px">이메일 </td>
               		<td><input type="email" name="email" style="width:200px; height:30px"></td>
               		
           		</tr>
           	     <tr>
              		 <td height="50px">대표자연락처 </td>
               		 <td>
                 	 <input type="text" maxlength="3" name="tel1" size="2" style="height:30px">-
                  	 <input type="text" maxlength="4" name="tel2" size="2" style="height:30px">-
                  	 <input type="text" maxlength="4" name="tel3" size="2" style="height:30px">
               		 </td>
               		<td><label id="nnResult"></label></td>
           	   </tr>
           	   <tr>
              		 <td height="50px">회사연락처 </td>
               		 <td>
                 	 <input type="text" maxlength="3" name="corptel1" size="2" style="height:30px">-
                  	 <input type="text" maxlength="4" name="corptel2" size="2" style="height:30px">-
                  	 <input type="text" maxlength="4" name="corptel3" size="2" style="height:30px">
               		 </td>
               		<td><label id="phoneResult"></label></td>
           	   </tr>
           	   </table>
           	   <div>
           	   <table align="center">
           	   	<tr>
           	   		<td height="50px">회사명</td>
               		<td><input type="text" name="copName" style="width:200px; height:30px"></td>
           	   	</tr>
           	  		<tr>
           	   		<td height="50px">사업자 등록 여부</td>
               		<td><input type="radio" name="copDeclarationEnroll" id="yes" >예<input type="radio" name="copDeclarationEnroll" id="no">아니오</td>
           	   	</tr>
           	   	<tr>
           	   		<td height="50px">사업자 등록번호 </td>
               		<td><input type="text" name="copDeclarationNo" style="width:200px; height:30px"></td>
           	   	</tr>
           	   
						<tr>
							<td>우편번호</td>
							<td><input type="text" name="zipCode" style="width:200px; height:30px"></td>
							<td><div id="ckZip" style="cursor:pointer;" onclick="goPopup();">검색</div></td>
						</tr>
						<tr>
							<td>주소</td>
							<td><input type="text" name="address1" style="width:200px; height:30px"></td>
							<td></td>
						</tr>
						<tr>
							<td>상세주소</td>
							<td><input type="text" name="address2" style="width:200px; height:30px"></td>
							<td></td>
						</tr>
						 	<tr>
           	   		<td height="50px">비밀번호찾기 질문</td>
           	   		<td>
           	   			<select style="width:200px; height:30px" name="pwdfindQuestion">
           	   				<option>자신의 좌우명은?</option>
           	   				<option>좋아하는 인물은?</option>
           	   				<option>가장 좋아하는 색깔은?</option>
           	   			</select>
           	   		</td>
           	   	</tr>
           	   	<tr>
           	   		<td height="50px">비밀번호찾기 답 </td>
               		<td><input type="text" name="pwdfindAnswer" style="width:200px; height:30px"></td>
           	   	</tr>
					</table>	
			</div>
				<div>
			<button id="success" onclick="insertMember();">가입</button>	
			<button id="cancel" onclick="insertMember();">취소</button>	
			
			</div>
		</form>
		</div>
			
	</div>
		
	<jsp:include page="../../common/footer.jsp"></jsp:include>
	<script>

	//아이디 유효성 검사(1=중복/ 0 != 중복)
	 function registerCheckFunction(){
		   //id="id_reg" / name ="userId"
		   var userId = $('#userId').val();
		   
		   $.ajax({
			   
			   type : 'GET',
			   url : '${applicationScope.contextPath}/checkCorpId.bo',
			   data : {userId, userId},
			   success : function(result){
				   console.log("1=중복x / 1 != 중복o: : "+result);
				   
				 if(result==1){ //id가 checkMessage인 것에 아래 테스트 출력
					 window.alert('사용할 수 있는 아이디입니다.');
					 $('#checkMessage').text('사용할 수 있는 아이디입니다.');
				 	 $('#checkMessage').css("color","blue");
				 	 $('#idCheck').val("중복확인 완료");
				 	 $('#idCheck').css("backgroundColor","blue");
				 	$('#idCheck').css("color","white");
				 		 
				 }else{
					 $('#checkMessage').text('사용할 수 없는 아이디입니다.');
					 $('#checkMessage').css("color","red");
					 $('#idCheck').val("중복확인");
					 $('#idCheck').css("backgroundColor","red");
					 $('#idCheck').css("color","black");
				 }
				 
				 //id가 checkModal인 모달함수 실행시켜서 모달 실행시키기 위해
				// $('#checkModal').modal("show");
				   
				   
				   
			   }
			      
		   });
		    
	   };   
	
	   
	   
	   function insertMember(){ 
			  
			var objID = document.getElementById("userId");
			var objPWD = document.getElementById("userPwd");
			var objPWD2 = document.getElementById("userPwd2");
			var objUserName = document.getElementById("userName");
			var objEmail = document.getElementById("email");
			var objTel1 = document.getElementById("tel1");
			var objTel2 = document.getElementById("tel2");
			var objTel3 = document.getElementById("tel3");
			var objGender1 = document.getElementById("gender1");
			var objGender2 = document.getElementById("gender2");
			var objZipCode = document.getElementById("zipCode");
			var objAddress1 = document.getElementById("address1");
			var objAddress2 = document.getElementById("address2");
			var objPwdAnswer = document.getElementById("pwdAnswer");
			var objIDconfirm = document.getElementById("idCheck").value;
			
			
			if((objID.value)==""){
				alert("아이디를 입력하지 않았습니다.");
				objID.focus();
				return false;
				
			}else if((objPWD.value)==""){
				alert("비밀번호를 입력하지 않았습니다.");
				objPWD.focus();
				return false;
			}else if((objPWD.value!=objPWD2.value)){
				alert("비밀번호와 비밀번호 확인이 같지 않습니다.");
				objPWD2.focus();
				return false;
			}else if((objEmail.value)==""){
				alert("이메일을 입력하지 않았습니다.");
				objEmail.focus();
				return false;
		  }else if(objTel1.value==""||objTel2.value==""||objTel3.value==""){
				alert("핸드폰 번호를 적어주세요");
				objTel1.focus();
				return false;
			}else if((objGender1.checked ==false)&&(objGender2.checked ==false)){
				alert("성별을 입력 해주세요");
				objGender1.focus();
				return false;
			}else if((objZipCode.value)=="") {
				alert("우편번호를 입력해주세요");
				objZipCode.focus();
				return false;
			}else if((objAddress1.value)==""){
				alert("주소를 입력해주세요");
				objAddress1.focus();
				return false;
			}else if((objAddress2.value)==""){
				alert("상세주소를 입력해주세요");
				objAddress2.focus();
				return false;
			}else if((objPwdAnswer.value)==""){
				alert("비밀번호 정답을 입력 해주세요");
				objPwdAnswer.focus();
				return false;
			}else if(objIDconfirm=="중복확인"){
				alert("아이디 중복확인을 입력 해주세요");
				objID.focus();
				return false;
			}
			

			else{
				
				$("#joinForm")[0].submit(); 
			}
			
	       }
	   
	   function goPopup() {
			var pop = window.open("../individual/jusoPopup.jsp","pop","width=570, height=420, scrollbars=yes, resizable=yes");
		}
		
		/** API 서비스 제공항목 확대 (2017.02) **/
		function jusoCallBack(roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, 
								jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn, 
								detBdNmList, bdNm, bdKdcd, siNm, sggNm, 
								emdNm, liNm, rn, udrtYn, buldMnnm, 
								buldSlno, mtYn, lnbrMnnm, lnbrSlno, emdNo){
			// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
			
			$("input[name=address1]").val(roadAddrPart1);
			$("input[name=address2]").val(addrDetail);
			$("input[name=zipCode]").val(zipNo);
		}
	
	
	
	</script>
	
</body>
</html>