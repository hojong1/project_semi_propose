<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<meta charset="UTF-8">
<title>업체 메인 페이지</title>
<style>
	#co-Main-wrap {
		margin: 0 auto;
		width:1400px;
	}
	#logoutBtn{
       padding: 15px;
       display:inline-block;
       text-align:center;
       color:white;
       height:25px;
       width:100px;
       font-size: 20px;
   }
  
   .admin-console {
    	float:right;
   		background-color: #211a1a;
   		color:white;
   		height: 720px;
   }
   
   #admin-menu label {
      line-height: 40px;
      margin-top: 15px;
      font-weight:bold;
      font-size: 18px;
   }
  #admin-menu a {
     text-decoration: none;
     color:white;
  }
  
  #dynamic-dialog {
  	background:#D3A0A0; 
  	color:white;
  	margin-left: 10px;
    font-size: 15px;
    float: right;
    margin-right: 70px;
  }
  .admin-btn{ 
   margin-left: 0px;
   padding:10px; 
   font-size: 12pt;
  }
	
	
</style>
</head>
<body>
<div id="co-Main-wrap">
	 <div style=" background: #645555; height: 731px; width:20%; float:left;" >
	   </div>
		<div>
	      <img id="background" src="/sm/resources/image/junhyong/bg4.png" style="width:60%; height: 731px; float: left;">
	   </div>
		<!-- admin-console starts  -->
	   <div class="admin-console" style="width:20%;">
	      <div style="margin-top:20px">
	         <img src="/sm/resources/image/junhyong/admin.png" style=" float: left; margin-left:50px;">
	         <br><br><br><br><br><label style="color:#ff8585; margin: 5px;">김선중 </label>님 반갑습니다.</div>
	         <div id="logoutBtn" style="margin-left:37px;"><label>로그아웃</label></div>
	      <br clear="both">
	      <div id="admin-menu" style="background-color: #645555; height: 528px;" >
	         <br>
	         <label class="admin-btn"><a href="#">※업체관리</a></label>
	         <br> <br>
	         <label class="admin-btn"><a href="co_EventInsertForm.jsp">업체 소개 수정</a></label><br>
	         <label class="admin-btn"><a href="co_memberJoinForm.jsp">업체 정보 수정</a></label><br>
	         <label class="admin-btn"><a href="#">등록 상품 현황 조회</a></label><br>
	         <label class="admin-btn"><a href="#">문의 작성</a></label><br>
	         <label class="admin-btn"><a href="co_ReservationManagement.jsp">예약 관리</a></label><br>
	         <label class="admin-btn"><a href="../../common/reservation.html">스케쥴 조회</a></label><br>
	         <label class="admin-btn"><a href="../individual/in_main.jsp">※이용자페이지로 이동</a></label>
	         
	         <br>
	         <br><br>
	      	 <button id="dynamic-dialog">업체 해지 신청</button>
				
	      </div>
		</div>
	</div>
	<!-- admin-console ends  -->
	<br clear="both">
	<br>
	
	
	
	<script>
	
	$(document).ready(function() {
	    // 버튼의 이벤트 핸들러를 붙입니다.
	    $("#dynamic-dialog").button().on("click", function() {
	    	// 다이얼로그 내용을 가져올 URL입니다.
	 	    var url = "/sm/views/html/dialog_memberOut.html";
	    	// 다이얼로그를 생성합니다.
	        $('<div id="DialogDiv">').dialog({
	            // 커스텀 스타일을 줍니다.

	            dialogClass: 'custom-dialog-style',

	            // 모달 다이얼로그로 생성합니다.
	            modal: true,
	            buttons: {
	                "확인": function() {
	                	 var alertMsg = "업체 해지 신청이 완료되었습니다";
	                	 alert(alertMsg);
	                	
	                	 location.href="/sm/views/member/corporate/co_main.jsp";
	                	 
	                	$(this).dialog();
	                    
	                	
	                },
	                "취소": function() {
	                    $(this).dialog("close");
	                }
	            },
	            close: function() {
	            	},
	            // 다이얼로그 열기 콜백
	            open: function () {

	            	// 모달 오버레이 설정
	                $(".ui-widget-overlay").css({
	                    opacity: 0.5,
	                    filter: "Alpha(Opacity=50)",
	                    backgroundColor: "black"
	                });

	            	// 내용을 불러 옵니다.
	                $(this).load(url);
	            },

	            // 닫기 콜백
	            close: function (e) {
	                $(this).empty();
	                $(this).dialog('destroy');
	            },
	            height: 450,
	            width: 450,
	            title: '업체 회원 탈퇴'
	        });
	    });
	});

	
	</script>


</body>
</html>