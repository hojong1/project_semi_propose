<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %> 
<!DOCTYPE html >
<html>
<head>
<meta  charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<title>Insert title here</title>
<style>

.outer {
      width:1000px;
      height:500px;
      background:white;
      color:black;
      margin-top:50px;
      margin-left:auto;
      margin-right:auto;
   }
   
   table {
      border:1px solid black;
      text-align:center;
      color:black;
      
   }
   .table-area {
      width:1000px;
      hegiht:400px;
      margin:0 auto;
      
   }
   .paging-area{
      margin:400px;
   }
</style>
</head>
<body>
   <jsp:include page="/views/common/menubar.jsp"/>
   
   
   <br>
   
   
   <div class="outer2">
      <h2>예약관리</h2>
      <hr>
      <h3>실시간 예약 조회</h3>
   </div>
   
   
   <div class="outer">
      <div class="table-area">
         <table align="center" id="listArea" font="">
            <tr>
               <th width="150px">예약코드</th>
               <th width="150px">개인아이디</th>
               <th width="150px">업체명</th>
               <th width="150px">이벤트명</th>
               <th width="150px">예약확인여부</th>
               <th width="150px">예약신청날짜</th>
               <th width="150px">이벤트진행날짜</th>
            </tr>
            
         </table>
      </div>
      
      <div class="paging-area" align="center" >
         <button onclick="location.href='${applicationScope.contextPath}/selectList.bo?currentPage=1'"><<</button>
         
         <c:if test="${ requestScope.pi.currentPage <= 1 }">
            <button disabled><</button>
         </c:if>
         <c:if test="${ requestScope.pi.currentPage > 1 }">
            <button onclick="location.href='${applicationScope.contextPath}/selectList.bo?currentPage=<c:out value="${ requestScope.pi.currentPage - 1 }"/>'"><</button>
         </c:if>
         
         <c:forEach var="p" begin="${ requestScope.pi.startPage }" end="${ requestScope.pi.endPage }" step="1">
            <c:if test="${ requestScope.pi.currentPage eq p }">
               <button disabled><c:out value="${ p }"/></button>
            </c:if>
            <c:if test="${ requestScope.pi.currentPage ne p }">
               <button onclick="location.href='${applicationScope.contextPath}/selectList.bo?currentPage=<c:out value="${ p }"/>'"><c:out value="${ p }"/></button>
            </c:if>
         </c:forEach>
         
         
         
         <c:if test="${ requestScope.pi.currentPage >= requestScope.pi.maxPage }">
            <button disabled>></button>
         </c:if>
         <c:if test="${ requestScope.pi.currentPage < requestScope.pi.maxPage }">
            <button onclick="location.href='${applicationScope.contextPath}/selectList.bo?currentPage=<c:out value="${ requestScope.pi.currentPage + 1 }"/>'">></button>
         </c:if>
         
         <button onclick="location.href='${applicationScope.contextPath}/selectList.bo?currentPage=<c:out value="${ requestScope.pi.maxPage }"/>'">>></button>
      </div>
      
      
   </div>
   

</body>
</html>