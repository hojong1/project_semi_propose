<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<title>Insert title here</title>
<style>
	.outer {
		width:1000px;
		height:650px;
		background:#FADEDE;
		color:black;
		margin-top:50px;
		margin-left:auto;
		margin-right:auto;
		font-size: 22px;
	}
	#insert-area table {
		border:1px solid black;
		width:700px;
		height:600px;
		background:#C4C4C4;
		
	}
	.btn-area {
	    height: 50px;
		width:350px;
		margin:0 auto;
		text-align: center;
		
	}
	#titleImgArea {
		width:350px;
		height:200px;
		border:2px dashed white;
		text-align:center;
		display:table-cell;
		vertical-align:middle;
	}
	#titleImgArea:hover, #contentImgArea1:hover, 
	#contentImgArea2:hover, #contentImgArea3:hover {
		cursor:pointer;
		background:white;
	}
	#contentImgArea1, #contentImgArea2, #contentImgArea3 {
		width:150px;
		height:100px;
		border:2px dashed white;
		text-align:center;
		display:table-cell;
		vertical-align:middle;
	}
	.poto{
		
		font-family: Ruda;
		font-style: normal;
		font-weight: bold;
		font-size: 15px;
		background:#D4CDCD;
		border:1px solid black;
		text-align: center;
	
	}
	.btn-area button {
		height:50px;
		width:110px;
		background:#37B4EA;
		color:white;
	}
	#content{
		width: 850px;
	}
	#title{
	width:300px;
	}
	#mainCategory, #subdivision, #productName, #price{
		height: 29px;
    	border: 1px solid black;
    	width: 250px;"
	}
	table{
	background:#DFC8C8;
	}
	#premium{
	width:110px;
	height:50px;
	background:#D3A0A0;
	color:white;
	font-weight: bold;
	vertical-align: middle;
	
	}
	.premium-apply{
		background: white;
		font-size:18px;
		border: 1px solid black;
		text-align:center;
		padding-left: 18px;
	}
	#radio-yes, #radio-no {
		width:40px;
		height:17px;
		font-size:30px;
		
	}
</style>
</head>
<body>
	<jsp:include page="/views/common/menubar.jsp"/>
	
	<%-- <c:if test="${ !empty sessionScope.loginUser }"> --%>
	
	<div class="outer">
		<br>
		<h2>이벤트</h2>
		<hr>
		<h4>이벤트 등록 </h4>								 <!-- 내가 전송하는 데이터가 파일이 포함되어있다면 필수로작성 -->
		
		<form action="${ applicationScope.contextPath }/insertEvent.tn" method="post" encType="multipart/form-data">
			<div class="insert-area">
				<table align="center" style="width: 100%; padding-right: 32px;">
					<tr>
						<td width="100px"class="poto">상품 분류</td>
						<td colspan="3" ><input type="text" name="mainCategory"id="mainCategory"></td>

					</tr>
					<tr>
						<td width="100px"class="poto">세부 분류</td>
						<td colspan="3" ><input type="text" name="subdivision"id="subdivision"></td>

					</tr>
					<tr>
						<td width="100px"class="poto">상품명</td>
						<td colspan="3" ><input type="text" name="productName"id="productName"></td>

					</tr>
					<tr>
						<td width="100px"class="poto" >판매가</td>
						<td><input type="text" name="price"id="price" value="0" style="font-size: 20px; text-align: right;">원</td>
						<td width="100px"class="poto">프리미엄 배너 신청</td>
						<td class="premium-apply">
							<label for="radio-yes">예</label>
							<input id="radio-yes" type="radio" name="radio-answer">
							<label for="radio-no">아니오</label>
							<input id="radio-no" type="radio" checked="checked" name="radio-answer">
						</td>
					</tr>
					<tr>
						<td class="poto">대표 이미지</td>
						<td colspan="3">
							<div id="titleImgArea">
								<img id="titleImg" width="850" height="200">
							</div>
						</td>
					</tr>
					
					<tr>
						<td class="poto">개별 이미지</td>
						<td style="width: 180px;">
							<div id="contentImgArea1">
								<img id="contentImg1" width="275" height="150">
							</div>
						</td>
						<td>
							<div id="contentImgArea2">
								<img id="contentImg2" width="275" height="150">
							</div>
						</td>
						<td>
							<div id="contentImgArea3">
								<img id="contentImg3" width="275" height="150">
							</div>
						</td>
					</tr>
					<tr>
						<td width="100px" class="poto">업체 소개</td>
						<td colspan="3">
							<textarea name="content" id="content" rows="5" cols="50" style="resize:none;"></textarea>
						</td>
					</tr>
					
				</table>
				<h5 align="center">* 빈칸에 공백란이 없도록 작성해주세요. *</h5>
				<div id="fileArea">											<!-- onchange의 구문으로 이미지를 체인지함 -->
					<input type="file" id="thumbnailImg1" name="thumbnailImg1" onchange="loadImg(this, 1)">
					<input type="file" id="thumbnailImg2" name="thumbnailImg2" onchange="loadImg(this, 2)">
					<input type="file" id="thumbnailImg3" name="thumbnailImg3" onchange="loadImg(this, 3)">
					<input type="file" id="thumbnailImg4" name="thumbnailImg4" onchange="loadImg(this, 4)">
				</div>
			</div>
			
			<br>
			
		</form>
			<div class="btn-area">
				<button type="reset">취소하기</button>
				<button type="submit">작성완료</button>
				<button type="button" id="premium" align="center" onclick="location.href='/sm/views/member/corporate/co_premiumInsertForm.jsp'">프리미엄 광고<br> 등록</button>
			</div>
		
	</div>
<%-- 	</c:if> --%>



<%-- 	<c:if test="${ empty sessionScope.loginUser }">
		<c:set var="msg" value="잘못된 경로로 접근하셨습니다." scope="request"/>
		<jsp:forward page="../common/errorPage.jsp"/>
	</c:if> --%>
	
	
	
	<script>
		$(function() {
			$("#fileArea").hide();//아래있던 안이쁜 인풋타입을 숨기기		
			
			$("#titleImgArea").click(function(){//사진 공간을 클릭했을때 인풋타입이 실행되도록 서로 연결해둠
				$("#thumbnailImg1").click();
			});
			$("#contentImgArea1").click(function(){
				$("#thumbnailImg2").click();
			});
			$("#contentImgArea2").click(function(){
				$("#thumbnailImg3").click();
			});
			$("#contentImgArea3").click(function(){
				$("#thumbnailImg4").click();
			});
		});
		
		function loadImg(value, num) {
			if(value.files && value.files[0]) {/* value에 있는 files [0]번쨰에 있으면 실행 */
				var reader = new FileReader();
				
				reader.onload = function(e) { //사진을 문자열로 바꿔서 저장해놈
					switch(num) {						/* target은 임시로 저장한다는것 */
					case 1 : $("#titleImg").attr("src", e.target.result); break;
					case 2 : $("#contentImg1").attr("src", e.target.result); break;
					case 3 : $("#contentImg2").attr("src", e.target.result); break;
					case 4 : $("#contentImg3").attr("src", e.target.result); break;
					}
				}
				
				reader.readAsDataURL(value.files[0]);
			}
		}
		
	 	  jQuery(document).ready(function($){
			$("#price").on('focus', function(){
				$("#price").val('');
			});  
			
			$("#price").on('blur', function(){
				var val = $("#price").val();
				if(!isEmpty(val) && isNumeric(val)){
					val = currencyFormatter(val);
					$("#price").val(val);
				} else {
					//기본값인 0 으로 돌려줌
					$("#price").val(val);
				}
			});
			
		}); 
	 	//Null 체크
	 	function isEmpty(value) {
	 		if(value.length == 0 || value == null){
	 			return true;
	 		} else {
	 			return false;
	 		}
	 	};
	 	//정규표현식을 이용한 Number 체크
	 	function isNumeric(value) {
	 		var regExp =/^[0-9]+$/g;
	 		return regExp.test(value);
	 	}
	 	//숫자 세자리 마다 콤마 추가하여 금액 표기 형태로 변환
	 	function currencyFormatter(amount) {
	 		return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g,',');
	 	}
	 	
	</script>
</body>
</html>
























