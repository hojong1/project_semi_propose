<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	body {
		background: #FADEDE;
		margin: auto;
	}
	.wrap {
		width:100%;
		height: 109px;

	}
	.tob-nav {
		margin: 0 auto;
		height: 109px; 

	}
	.menu {
		font-family: 맑은고딕;
		font-style: normal;
		font-weight: bold;
		font-size: 20px;
		line-height: 100%;
		color: #FFF7F7;
		display:table-cell;
		vertical-align:middel;
		background: #786A6A;
		padding: 30px;
		

	}
	#menu-dropdown {
		background: #000000;
    	color: #C4C4C4;
   		font-size: 85px;
	}
	.first_text{
		font-size: 40px;
		line-height: 49px;
		color: #FB7171;
	}
	
	#login {	
		height: 40px;
		background: #D3A0A0;
		border-radius: 5px;
		font-family: SeoulNamsan;
		font-style: normal;
		font-weight: normal;
		font-size: 20px;
		line-height: 20px;
		text-align: center;
		color: #FFFFFF;
		width: 100px;
		
	}
 /* 메뉴 바 스타일 설정 끝 */	
	.profile{
	position: absolute;
	width: 150px;
	height: 35px;
	left: 68px;
	top: 138px;
	
	font-family: Roboto;
	font-style: normal;
	font-weight: bold;
	font-size: 25px;	
	line-height: 29px;
	text-align: center;
	
	}
	#line{
	position: absolute;
	width: 133px;
	height: 0px;
	left: 5px;
	top: 70px;
	border: 1px solid #000000;
	}

	 hr.vertical {
	position: absolute;
  	width: 0px;
  	height: 100%;
  	left:150px;
	top: 130px;
	border: 1px solid #000000;
  	/* or height in PX */
	}
	
	
	/*--------section나누기, aside바 ----------*/
	div.content { width:600px; float:left; }
    aside#aside { width:220px; float:right; }
    section#container::after { content:""; display:block; clear:both; }

	#sideMenu{
	
	position: absolute;
	width: 290px;
	height: 1000px;
	left: 85%;
	top: 15%;

	background: #645555;
	}
	
	#welcome{
	position: absolute;
	width: 290px;
	height: 200px;
	left: 85%;
	top: 15%;

	background: #211A1A;
	}
	
	#user{
	position: absolute;
	width: 56px;
	height: 24px;
	left: 45%;
	top: 30%;
	
	font-family: Ruda;
	font-style: normal;
	font-weight: bold;
	font-size: 20px;
	line-height: 24px;
	
	color: #FFFFFF;
	}
	
	#hello{
	position: absolute;
	width: 150px;
	height: 18px;
	left: 40%;
	top: 40%;
	
	font-family: Ruda;
	font-style: normal;
	font-weight: bold;
	font-size: 15px;
	line-height: 18px;
	
	color: #FF8585;
	}
	
	#list{
	position: absolute;
	width: 250px;
	height: 18px;
	left: 15%;
	top: 20%;
	font-family: Ruda;
	font-style: normal;
	font-weight: bold;
	font-size: 20px;
	line-height: 24px;
	/* identical to box height */
	
	
	color: #FFFFFF;
	}
	
	ul li{
	margin-top:30px;
	}
	p{
	
	  display: inline;
	
	}
	#line1{
	position: absolute;
	width: 250px;
	height: 18px;
	left: 5%;
	top: 29%;
	font-family: Ruda;
	font-style: normal;
	font-weight: bold;
	font-size: 23px;
	line-height: 24px;
	}
	#line2{
	position: absolute;
	width: 250px;
	height: 18px;
	left: 13%;
	top: 29.5%;
	font-family: Ruda;
	font-style: normal;
	font-weight: bold;
	font-size: 18px;
	line-height: 24px;
	color:	#808080;
	}
	
	#line3{
	position: absolute;
	width: 1200px;
	left: 5%;
	top: 35%;
	border: 1px solid #C0C0C0;
	}
	/* aside바 끝 */

	#getCoupon{
	position: absolute;
	left: 10%;
	top: 40%;
	}
	#sort{
	position: absolute;
	left: 60%;
	top: 40%;
	height:30px;
	}
	#couponImage{
	position: absolute;
	left: 60%;
	top: 40%;
	height:30px;
	}
</style>
</head>
<body>
<!-- nav-area start -->
	<div class="wrap">
		<div class="tob-nav">
			<div class="menu" id="menu-dropdown">≡</div>
			<div class="menu" style="width:100%;"><label class="first_text">연</label>애부터 <label class="first_text">결</label>혼까지
			<img src="/sm/resource/image/common/flower.png"></div>
			<div class="menu" style="vertical-align: middle; padding: 10px;"><img alt="" src="/sm/resource/image/common/FAQ_icon.png"></div>
			<div class="menu" style="vertical-align: middle;"><button id="login">Login</button></div>

		</div>
	</div>
	<!-- nav-area end -->
	
	<div class="profile">
		<h2>MY 쿠폰</h2><br>
		
		
		<hr id="line">
		<hr class="vertical" />
		
	</div>
	
	<div id="box1">
	<p id="line1">보유쿠폰</p>
	<p id="line2">사용한 쿠폰</p> 
	
	
	</div>
	
	
	<hr id="line3">
	<section id="container">

		<input type="text" maxlength="13" name="getCoupon" id="getCoupon"
			style="width: 300px; height: 30px"
			placeholder="고객님께서 보유하고 있는 쿠폰은 총 1장 입니다."> <select
			name="sort" id="sort">
			<option value="small">최근 발급순</option>
			<option value="medium">이름순</option>
			<option value="large">할인순</option>
		</select> <img alt="" style="height: 57px;"
			src="/sm/resource/image/jaemin/Coupon.png" id="couponImage">
		<aside id="aside">
			<div id="sideMenu">
				<div id="list">
					<ul>
						<li>내 정보 변경</li>
						<li>메인 페이지로 이동</li>
						<li onclick="myCoupon();" id="coupon">내 쿠폰함</li>
						<li>Q&A 질문하기</li>
					</ul>
				</div>
			</div>
			<div id="welcome">
				<img src="/sm/resource/image/jaemin/MyImage.png"> <label
					id="user">user</label>
				<p id="hello">회원님 반갑습니다.</p>


			</div>
		</aside>


	</section>

	<jsp:include page="../../common/footer.jsp"></jsp:include>
</body>
</html>