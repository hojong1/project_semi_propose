<jsp:directive.page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.reserv-wrap {
	margin-top: 10%;
	font-size:
}
button {
    background: #37B4EA;
    color: white;
    width:80px;
    height: 40px;
}
select {
	font-size:18px;
	width:100px;
}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />

	<div class="reserv-wrap">
		<div id="reserv-area"
			style="width: 800px; height: 800px; padding: 30px; background: #FFEBEB; margin: 0 auto; font-size: 18px;">
			<form>
				<div style="display: grid; float: left; margin: 15px;">
					<label>예약 프로포즈 : 요트프로포즈</label><br> <label>이벤트 주관사 :
						고프로포즈</label><br> <label>예약날짜 : 2020년 9월 7일</label>
				</div>
				<div style="display: grid; text-align: -webkit-center;">
					<img alt="" src="/sm/resources/image/dongyub/reservImg.png">
					<label>결제하실 금액 : 350,000원</label>
				</div>
				<hr style="border: 2px double black; margin-top: 10%;">
				<div style="margin: 15px; padding-top: 10%; text-align: end;
   							display: inline-block;">
					<label>예약자 성함 : </label> 
					<input type="text" id="reservName"
						name="reservName" value="홍길동">
					<button type="button">수정</button>
					<br><br>
					<label>예약자 연락처 : </label> 
					<input type="tel"
						id="reservTel" name="reservTel" value="010-5392-2122">
					<button type="button">수정</button>
				</div>
				<hr style="border: 2px double black; margin-top: 10%;">
				<div style="margin: 15px; padding-top: 10%; 
							text-align: end; display: inline-block;">
					<label>쿠폰 : </label> 
					<select>
						<option>없음</option>
					</select> 
					<br><br>
					<label>결제 수단 : </label> 
					<select>
						<option>신용카드</option>
						<option>계좌이체</option>
						<option>무통장입금</option>
					</select>
					<br><br>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;
					<select>
						<option>BC카드</option>
						<option>우리은행</option>
						<option>KB국민</option>
					</select>
				</div>
				<div style="text-align-last: center;">
					<button onclick="credit();">결제</button>
				</div>
			</form>
		</div>
	</div>

	<jsp:include page="../../common/footer.jsp" />
	
	<script>
		function credit() {
			alert("홍길동님의 요트 프로포즈 이벤트 예약이 완료되었습니다.");
			
		}
	</script>
	
</body>
</html>