<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	.searchAfter {
		margin: 0 auto;
		
	}
	
	*{margin:0; padding:0;}
	.star{
 	 display:inline-block;
 	 width: 30px;height: 60px;
 	 cursor: pointer;
	}
	.star_left{
	 background: url(http://gahyun.wooga.kr/main/img/testImg/star.png) no-repeat 0 0; 
 	 background-size: 60px; 
 	 margin-right: -3px;
	}
	.star_right{
  	background: url(http://gahyun.wooga.kr/main/img/testImg/star.png) no-repeat -30px 0; 
  	background-size: 60px; 
  	margin-left: -3px;
	}
	.star.on{
  	background-image: url(http://gahyun.wooga.kr/main/img/testImg/star_on.png);
	}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp"></jsp:include>
	
	<div class="searchAfter">
		<div style="display: inline-block; padding: 100px; padding-top: 100px; padding-left: 100px; padding-bottom: 10px;">
			<div style="font-family: Roboto; font-style: normal; font-weight: bold; font-size: 25px; line-height: 29px; text-align: center; color: #000000;">
			<label>서울</label>
			<hr style="width: 190px; border: 1px solid #878181;"></div>
			<div style="font-family: Roboto; font-style: normal; font-weight: bold; font-size: 20px; line-height: 23px; text-align: center; color: #C60000;">
			Premium Top 3
			</div>
		</div>
		<div class="companyList" style="display: table; width: 85%; height: 300px; margin-left: 100px; padding-left: 10px; background: #FFFFFF; box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);">
			<div style="display: table-cell;">
			<label style="float: left;">고프로포즈</label>
			<div class="star-box">
 				<span class="star star_left"></span>
				<span class="star star_right"></span>

  				<span class="star star_left"></span>
  				<span class="star star_right"></span>

  				<span class="star star_left"></span>
  				<span class="star star_right"></span>

 				<span class="star star_left"></span>
 				<span class="star star_right"></span>

 				<span class="star star_left"></span>
 				<span class="star star_right"></span>
			</div>
		</div>
		
		<div style="display: table-cell;">
			<label style="float: left;">러브헌터</label>
			<div class="star-box">
 				<span class="star star_left"></span>
				<span class="star star_right"></span>

  				<span class="star star_left"></span>
  				<span class="star star_right"></span>

  				<span class="star star_left"></span>
  				<span class="star star_right"></span>

 				<span class="star star_left"></span>
 				<span class="star star_right"></span>

 				<span class="star star_left"></span>
 				<span class="star star_right"></span>
			</div>
		</div>
		
		<div style="display: table-cell;">
			<label style="float: left;">두근두근</label>
			<div class="star-box">
 				<span class="star star_left"></span>
				<span class="star star_right"></span>

  				<span class="star star_left"></span>
  				<span class="star star_right"></span>

  				<span class="star star_left"></span>
  				<span class="star star_right"></span>

 				<span class="star star_left"></span>
 				<span class="star star_right"></span>

 				<span class="star star_left"></span>
 				<span class="star star_right"></span>
			</div>
		</div>
		
		</div>
		<div class="searchCondition" style="padding-top: 30px; width:650px; height:150px; padding-left: 100px; display: table;">
		<div style="border:1px solid black;  display:table-cell"><img alt="" src=""><label>조회순</label></div>
		<div style="border:1px solid black;  display:table-cell"><img alt="" src=""><label>요금순</label></div>
		<div style="border:1px solid black;  display:table-cell"><img alt="" src=""><label>평점순</label></div>
		</div>
		<div class="searchCondtionDetail" style="margin-top: 30px; width: 85%; height: 300px; margin-left: 100px; background: #FFFFFF; box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);">
		조회순, 요금순, 평점순 에 맞는 화면 출력
		</div>
		<div class="moreAndmore">
		
		</div>
	
	
	</div>
	
	<jsp:include page="../../common/footer.jsp"></jsp:include>
	
	<script>
	//평점 제이쿼리문
	$(".star").on('click',function(){
  		var idx = $(this).index();
  		$(".star").removeClass("on");
  	  	for(var i=0; i<=idx; i++){
      		$(".star").eq(i).addClass("on");
   		}
 	});
	</script>
	
</body>
</html>