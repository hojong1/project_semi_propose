<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
	.headWrapper{
		max-width:800px;
		height:auto;
	}
	.title {
	float:left;
	margin-left: 650px;
	text-align:center;
	}
	#eventName {
		float:left;
		margin-top:60px;
	}
	#reserv_btn{
		background-color: #FFEFEF;
		float:left;
		margin-top:80px;
		margin-left:360px;
		padding: 15px;
	}
	#res_btn {
		border:none;
		color: white;
		background-color: #37B4EA;
		margin-left:5px;
		padding: 3px;
	}
	
	#picture_area {
		float:left;
		margin-left:400px;
	}
	
	.event_info {
		float: left;
		margin-left: 50px;
	}
	.event_infos{
		width: 168px;
		height: 80px;
		background-color: #FFEFEF;
		
		margin-top:20px;
		text-align: center;
	}
	#area3{
	height: 120px;
		
	}
	#event_details{
		margin-left:450px;
	}
	.detail_button{
	border-radius: 10px;
	border: solid 2px pink;
	float:left;
	width:150px;
	height:50px;
	background-color: #786a6a;
	color: white;
	text-align: center;
	line-height: 50px;
	font-weight: bold;
	}
	
	#description_field{
		background-color: #ffefef;
		width:800px;
		min-height: 600px;
	}
	
	
</style>
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp"/>
	<div class=head_wrapper>
		<div class="title">
			<h2 id="eventName">요트 프로포즈</h2><p style="color:#de867e;">조회수: 2424</p>
		</div>
		<div id="reserv_btn">
			<label>350,000원</label><button id="res_btn">예약하기</button>
		</div>
		<br clear="both">
		<div id="picture_area"> 
			<img src="/sm/resources/image/junhyong/event_detail_title.png">
		</div>
		<div class="event_info">
			<div class="event_infos" id="area1">
				<br>이벤트 주관업체: <br>고프로포즈
			</div>
			<div class="event_infos" id="area2">
				<br>이벤트 평점:<br>
				<img>
			</div>
			<div class="event_infos" id="area3">
			<br>
				이런 이벤트는 <br> 어떠세요?
				<img>
			</div>
		</div>
	</div>
	<br clear="both">
	<br><hr><br><br>
	<div id="event_details">
		<div>
			<div class="detail_button" id="button1">
				<label>이벤트 상세설명</label>
			</div>
			<div class="detail_button"  id="button2">
				<label>이벤트평점 리뷰</label>
			</div>
		</div>
			<br clear="both">
			<div id="description_field">
				<div id="space"></div>
				<div id="description" align="center">
				이벤트 상세설명
				</div>
			</div>
	</div>
	<jsp:include page="../../common/footer.jsp"></jsp:include>
</body>
</html>
