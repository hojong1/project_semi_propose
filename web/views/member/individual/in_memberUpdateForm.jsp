<jsp:directive.page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>

	.memberUpdate-area {
		width:800px;
    	height:850px;
    	font-size: 25px;
		line-height: 29px;
		background: #FFFFFF;
    	margin-left:auto;
    	margin-right:auto;
    	margin-top:150px;
	}
	
	.updateForm-area {
		background: #FFFEFE;
		border: 10px solid #F2F2F2;
		width: 600px;
   	 	margin: 0 auto;
    	height: 600px;
	}
	
	#memberjoin > td {
	 	text-align:right;
	 }
	 
	 #meberjoin > tr {
	 	margin:5px;
	 }
	 
	 #idCheck:hover{
      cursor:pointer;
     }
     
     #idCheck {
      background:orangered;
      border-radius:5px;
      width:80px;
      height:25px;
      text-align:center;
	 }	 
	 
	 .memberUpdate-btns > button {
	 	width: 94px;
		height: 40px;
	 }
	 
	 #cancel {
	 	margin-left: 40px;
	 }
	 
	 #success {
	 	float: right;
    	margin-right: 40px;
	 }
	 
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp"></jsp:include>
	<div class="memberUpdate-area">
		<br>
		<h2 align="center">회원정보 수정</h2>
		<div class="updateForm-area">
			<form id="UpdateForm"
				action="${ applicationScope.contextPath }/updateMember.me"
				method="post">
				<table id="memberjoin" align="center">
					<tr>
						<td height="50px">*회원이름</td>
						<td><input type="text" maxlength="13" name="nickName"
							id="userName" style="width: 200px; height: 30px"
							readonly value="${ sessionScope.loginUser.userName }"></td>
					</tr>
					<tr>
						<td height="50px">*회원아이디</td>
						<td><input type="text" maxlength="13" name="userId"
							id="userId" style="width: 200px; height: 30px"
							readonly value="${ sessionScope.loginUser.userId }"></td>
						<td width="200px"><div id="idCheck"
								style="width: 150px; height: 30px">중복확인</div></td>
					</tr>
					<tr>
						<td height="50px">*비밀번호</td>
						<td><input type="password" maxlength="13" name="userPwd"
							id="userPwd" style="width: 200px; height: 30px"></td>
					</tr>
					<tr>
						<td height="50px">*비밀번호확인</td>
						<td><input type="password" maxlength="13" name="userPwd2"
							id="userPwd2" style="width: 200px; height: 30px"></td>
						<td></td>
					</tr>
					<tr>
						<td height="50px">이메일</td>
						<td><input type="email" name="email"
							style="width: 200px; height: 30px" value="${ sessionScope.loginUser.email }"></td>

					</tr>
					<tr>
						<td height="50px">연락처</td>
						<td><input type="text" maxlength="3" name="tel1" size="2"
							style="height: 30px">- <input type="text" maxlength="4"
							name="tel2" size="2" style="height: 30px">- <input
							type="text" maxlength="4" name="tel3" size="2"
							style="height: 30px"></td>
						<td><label id="nnResult"></label></td>
					</tr>
					<tr>
						<td>우편번호</td>
						<td>  <input type="hidden" id="confmKey" name="confmKey" value="">
						<input type="text" name="zipCode" readonly style="width:200px; height:30px"></td>
						<td><div id="ckZip" style="cursor:pointer; margin-left:10px;" onclick="goPopup();">검색</div></td>
					</tr>
					<tr>
						<td>주소</td>
						<td><input type="text" name="address1" style="width:200px; height:30px"></td>
						<td></td>
					</tr>
					<tr>
						<td>상세주소</td>
						<td><input type="text" name="address2" style="width:200px; height:30px"></td>
						<td></td>
					</tr>
					<tr>
						<td height="50px" width="600px">비밀번호찾기 질문</td>
						<td><select style="width: 200px; height: 30px" name="pwdQuestion">
								<option>자신의 좌우명은?</option>
								<option>좋아하는 인물은?</option>
								<option>가장 좋아하는 색깔은?</option>
						</select></td>
					</tr>
					<tr>
						<td height="50px">비밀번호찾기 답</td>
						<td><input type="text" name="pwdAnswer"
							style="width: 200px; height: 30px"></td>
					</tr>

				</table>
				<div class="memberUpdate-btns">
					<button type="reset" id="cancel">취소</button>
					<button type="submit" id="success" >수정</button>
				</div>
			</form>
		</div>
	</div>
	
	<jsp:include page="../../common/footer.jsp"></jsp:include>
	
	<script>
		$(function(){
			$(".footer").css("margin-top","5%");
		});
		
		//전화번호 설정
		$(function(){
			var arr = "${ sessionScope.loginUser.phone}".split("-");
			console.log(arr);
			
			for(var i = 1 ; i <= arr.length ; i++) {
				$("input[name="+ i +"]").each(function(){
					$(this).val(arr[i-1]);
				});
			}
		});
		
		//주소 설정
		$(function(){
			var arr = "${ sessionScope.loginUser.address}".split("$");
			
			$("input[name=zipCode]").val(arr[0]);
			
			for(var i = 1 ; i<= arr.length -1 ; i++) {
				$("input[name=address"+i +"]").each(function(){
					$(this).val(arr[i]);
				});
			}
			
		});
		
		//주소 검색 api
		
		// opener관련 오류가 발생하는 경우 아래 주석을 해지하고, 사용자의 도메인정보를 입력합니다. ("팝업API 호출 소스"도 동일하게 적용시켜야 합니다.)
		//document.domain = "abc.go.kr";
		
		function goPopup() {
			var pop = window.open("jusoPopup.jsp","pop","width=570, height=420, scrollbars=yes, resizable=yes");
		}
		
		/** API 서비스 제공항목 확대 (2017.02) **/
		function jusoCallBack(roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr, 
								jibunAddr, zipNo, admCd, rnMgtSn, bdMgtSn, 
								detBdNmList, bdNm, bdKdcd, siNm, sggNm, 
								emdNm, liNm, rn, udrtYn, buldMnnm, 
								buldSlno, mtYn, lnbrMnnm, lnbrSlno, emdNo){
			// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
			
			$("input[name=address1]").val(roadAddrPart1);
			$("input[name=address2]").val(addrDetail);
			$("input[name=zipCode]").val(zipNo);
		}
	
	</script>
</body>
</html>