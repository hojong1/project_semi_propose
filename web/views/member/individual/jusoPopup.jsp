<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script language="javascript">
//opener관련 오류가 발생하는 경우 아래 주석을 해지하고, 사용자의 도메인정보를 입력합니다. ("주소입력화면 소스"도 동일하게 적용시켜야 합니다.)
//document.domain = "abc.go.kr";
function init(){
	var url = location.href;
	var confmKey = "U01TX0FVVEgyMDIwMDkxNTExMTcwNTExMDE4ODk= ";//승인키
	var resultType = "4"; // 도로명주소 검색결과 화면 출력유형, 1 : 도로명, 2 : 도로명+지번, 3 : 도로명+상세건물명, 4 : 도로명+지번+상세건물명
	var inputYn= "${ param.inputYn }";
	if(inputYn != "Y"){
		document.form.confmKey.value = confmKey;
		document.form.returnUrl.value = url;
		document.form.resultType.value = resultType; 
		document.form.action="http://www.juso.go.kr/addrlink/addrLinkUrl.do"; // 인터넷망
		document.form.submit();
	}else{
		/** API 서비스 제공항목 확대 (2017.02) **/
		opener.jusoCallBack("${ param.roadFullAddr }","${ param.roadAddrPart1 }","${ param.addrDetail }", "${ param.roadAddrPart2 }","${ param.engAddr }"
							, "${ param.jibunAddr }","${ param.zipNo }", "${ param.admCd }", "${ param.rnMgtSn }", "${ param.bdMgtSn }"
							, "${ param.detBdNmList }", "${ param.bdNm }", "${ param.bdKdcd }", "${ param.siNm }", "${ param.sggNm }"
							, "${ param.emdNm }", "${ param.liNm }", "${ param.rn }", "${ param.udrtYn }", "${ param.buldMnnm }"
							, "${ param.buldSlno }", "${ param.mtYn }", "${ param.lnbrMnnm }", "${ param.lnbrSlno }", "${ param.emdNo }");
 		window.close();
	}
}
</script>
<body onload="init();">
<form id="form" name="form" method="post">
		<input type="hidden" id="confmKey" name="confmKey" value=""/>
		<input type="hidden" id="returnUrl" name="returnUrl" value=""/>
		<input type="hidden" id="resultType" name="resultType" value=""/> loading...
	</form>
</body>
</html>