<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	    
<c:set var="contextPath" value="${ pageContext.request.contextPath }" scope="application"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
#rectangle354{
	position: absolute;
	width: 500px;
	height: 600px;
	left: 1600px;
	top: 250px;
	
	background: rgba(251, 248, 248, 0.7);
	border: 1px solid #4486AB;
	box-sizing: border-box;
	}
#rectangle355{
	position: absolute;
	width: 500px;
	height: 100px;
	left: 1600px;
	top: 900px;
	
	background: rgba(251, 248, 248, 0.7);
	border: 1px solid #4486AB;
	box-sizing: border-box;
	}
#rectangle356{
	position: absolute;
	width: 500px;
	height: 100px;
	left: 1600px;
	top: 1050px;
	
	background: rgba(251, 248, 248, 0.7);
	border: 1px solid #4486AB;
	box-sizing: border-box;
	}
body{
background: #F3F3F3;
}
#group1{
width: 600px;
height: 700px;
left: 500px;
top: 300px;
margin-left:300px;
margin-top:230px;
}
#group2{
width: 700px;
height: 400px;
left: 500px;
top: 300px;
margin-left:-500px;
margin-top:230px;
}
#individual{
position: absolute;
width: 170px;
height: 50px;
margin-top:-50px;
}
#admin{
position: absolute;
width: 170px;
height: 50px;
margin-top:-50px;
margin-left:160px;
border-style:hidden;
border:0px;
}
#corp{
position: absolute;
width: 170px;
height: 50px;
margin-top:-50px;
margin-left:330px;
}
#individual:hover,#admin:hover,#corp:hover,#login:hover,#pwdfind:hover,#join:hover,#nonMember:hover{
cursor:pointer;
 transform: scale(1.1);
}
#title{
	font-family: Monospace;
	font-style: italic;
	font-weight: bold;
	font-size: 40px;
	margin-left:200px;

}
#idArea{
font-family: 맑은고딕;
	font-style: normal;
	font-weight: bold;
	font-size: 20px;
	margin-left:80px;
	
}
#pwdArea{
font-family: 맑은고딕;
	font-style: normal;
	font-weight: bold;
	font-size: 20px;
	margin-left:60px;
	
}
#login{

margin-left:150px;
width: 200px;
height:80px;
}

#pwdfind{
margin-left:130px;
font-style: italic;
	font-weight: bold;
	font-size: 20px;
}
#makeId{
	font-family: Monospace;
	font-style: normal;
	font-weight: bold;
	font-size: 18px;
	margin-left:120px;


}#join{
font-family: Monospace;
	font-style: normal;
	font-weight: bold;
	font-size: 20px;
	color:blue;

}#nonMember{
font-family: 맑은고딕;
	font-style: normal;
	font-weight: bolder;
	font-size: 23px;
	margin-left:150px;
	color:#808080;
}
.forLogin {
 	width:300px;
 	height:30px;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	
	
	<section id="container">
		<div>
			<img alt="" src="/sm/resources/image/jaemin/Main1.png" id="group1">
			<img alt="" src="/sm/resources/image/jaemin/Main2.png" id="group2">
		</div>
		
			<aside id="aside">
			
			<div id="rectangle354">
			<img alt="" src="/sm/resources/image/jaemin/individual.png" id="individual" onclick="changeToIdv()">
			<img alt="" src="/sm/resources/image/jaemin/admin.png" id="admin" onclick="changeToAdv()">
			<img alt="" src="/sm/resources/image/jaemin/corp.png" id="corp" onclick="changeToCorp()">
			<br>
			<label id="title">연결</label>
			<img alt="" src="/sm/resources/image/jaemin/miniflower.png" id="miniflower">
			
			<br><br><br><br>
			<form id="loginForm" action="${appicationScope.contextPath }/sm/login.me" method="post">
				<label id="idArea">아이디</label>&nbsp;&nbsp; 
				<input type= "text" class="forLogin" name="id">
				<br><br><br>
				<label id="pwdArea">비밀번호</label>&nbsp;&nbsp; 
				<input type="password" class="forLogin" name="password">
				<br><br><br><br>
				<button id= "login" type= "submit"><img alt="" src="/sm/resources/image/jaemin/login.png"></button>
				<br><br><br>
			</form>
				<label id="pwdfind" onclick="pwdfind();">비밀번호를 잊으셨나요?</label>
			</div>
			
			<div id="rectangle355">
			<br>
			<label id="makeId">계정이 없으신가요?</label>&nbsp;&nbsp;<label id="join" onclick="join();">가입하기</label> 
			</div>
			
			<div id="rectangle356"><br><br>
			<label id="nonMember" onclick="nonMember();">비회원으로 구경하기</label>
			</div>
		</aside>
			
	
	</section>

	<jsp:include page="/views/common/footer.jsp"/>
	
	<script>
	function changeToIdv(){
		var loginForm=document.getElementById("loginForm")
		var login = document.getElementById("loginForm").action
		login="${appicationScope.contextPath }/sm/login.me";
		loginForm.setAttribute("action",login)
		}

	function changeToAdv(){
		var loginForm=document.getElementById("loginForm")
		var login = document.getElementById("loginForm").action
		login="${appicationScope.contextPath }/sm/adminLogin.me";
		loginForm.setAttribute("action",login)
	}
	
	function changeToCorp() {
		var loginForm=document.getElementById("loginForm")
		var login = document.getElementById("loginForm").action
		login="${appicationScope.contextPath }/sm/corpLogin.me";
		loginForm.setAttribute("action",login)
	}
	
	function pwdfind(){
		 location.href='/sm/views/common/tempfindpwd.jsp';
	}
	function join(){
		 location.href='/sm/views/common/insertMemberPage.jsp';
	}
	
	function nonMember(){
		 location.href='/sm/views/member/individual/in_main.jsp';
	}
	
	</script>
</body>
</html>