package com.semi.pose.member.model.vo;

public class Admin {
	private String userId;
	private String pwd;
	private String pwdCheck;
	private String name;
	private String email;
	private String phone;

	
	public Admin() {}


	public Admin(String userId, String pwd, String pwdCheck, String name, String email, String phone) {
		super();
		this.userId = userId;
		this.pwd = pwd;
		this.pwdCheck = pwdCheck;
		this.name = name;
		this.email = email;
		this.phone = phone;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getPwd() {
		return pwd;
	}


	public void setPwd(String pwd) {
		this.pwd = pwd;
	}


	public String getPwdCheck() {
		return pwdCheck;
	}


	public void setPwdCheck(String pwdCheck) {
		this.pwdCheck = pwdCheck;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	@Override
	public String toString() {
		return "Admin [userId=" + userId + ", pwd=" + pwd + ", pwdCheck=" + pwdCheck + ", name=" + name + ", email="
				+ email + ", phone=" + phone + "]";
	}
	
	
	
}
