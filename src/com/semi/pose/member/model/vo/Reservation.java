package com.semi.pose.member.model.vo;

import java.util.Date;

public class Reservation {

	
	private String reservationNumber;
	private String userName;
	private String userId;
	private String phone;
	private int price;
	private String reservationDate;
	private String eventcode;
	private String paymethod;
	
	public Reservation(){}


	public Reservation(String reservationNumber, String userName,String userId, String phone, int price, String reservationDate,
			String eventcode, String paymethod) {
		super();
		this.reservationNumber = reservationNumber;
		this.userName = userName;
		this.userId = userId;
		this.phone = phone;
		this.price = price;
		this.reservationDate = reservationDate;
		this.eventcode = eventcode;
		this.paymethod = paymethod;
	}


	public String getReservationNumber() {
		return reservationNumber;
	}


	public void setReservationNumber(String reservationNumber) {
		this.reservationNumber = reservationNumber;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public String getReservationDate() {
		return reservationDate;
	}


	public void setReservationDate(String reservationDate) {
		this.reservationDate = reservationDate;
	}


	public String getEventcode() {
		return eventcode;
	}


	public void setEventcode(String eventcode) {
		this.eventcode = eventcode;
	}

	
	public String getPaymethod() {
		return paymethod;
	}


	public void setPaymethod(String paymethod) {
		this.paymethod = paymethod;
	}


	@Override
	public String toString() {
		return "Reservation [reservationNumber=" + reservationNumber + ", userName=" + userName +",userId =" + userId + ", phone=" + phone
				+ ", price=" + price + ", reservationDate=" + reservationDate + ", eventcode=" + eventcode + ", paymethod= "+paymethod+"]";
	}

	
	
	
	
	
}
