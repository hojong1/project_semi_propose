package com.semi.pose.member.model.vo;

import java.io.Serializable;

public class CoMember implements Serializable{
	private String membershipNumber; 
	private String pwd;
	private String pwdConfirm;
	private String copId;
	private String representativeName; //대표자 이름
	private String email;
	private String phone;
	private String address;
	private String pwdFindQuestion; //비밀번호찾기 질문
	private String pwdFindAnswer;
	private String blackListOrNotBlackList;
	private String copNo;
	private String copName;
	private String copPhone;
	private String copDeclarationEnroll;
	private String copDeclarationNo; //사업자등록번호?
	private String leaveOrNotLeave;  //탈퇴여부
	
	public CoMember() {}

	public CoMember(String membershipNumber, String pwd, String pwdConfirm, String copId, String representativeName,
			String email, String phone,String address, String pwdFindQuestion, String pwdFindAnswer, String blackListOrNotBlackList,
			String copNo, String copName, String copPhone, String copDeclarationEnroll, String copDeclarationNo,
			String leaveOrNotLeave) {
		super();
		this.membershipNumber = membershipNumber;
		this.pwd = pwd;
		this.pwdConfirm = pwdConfirm;
		this.copId = copId;
		this.representativeName = representativeName;
		this.email = email;
		this.phone = phone;
		this.address=address;
		this.pwdFindQuestion = pwdFindQuestion;
		this.pwdFindAnswer = pwdFindAnswer;
		this.blackListOrNotBlackList = blackListOrNotBlackList;
		this.copNo = copNo;
		this.copName = copName;
		this.copPhone = copPhone;
		this.copDeclarationEnroll = copDeclarationEnroll;
		this.copDeclarationNo = copDeclarationNo;
		this.leaveOrNotLeave = leaveOrNotLeave;
	}

	

	public String getMembershipNumber() {
		return membershipNumber;
	}

	public void setMembershipNumber(String membershipNumber) {
		this.membershipNumber = membershipNumber;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPwdConfirm() {
		return pwdConfirm;
	}

	public void setPwdConfirm(String pwdConfirm) {
		this.pwdConfirm = pwdConfirm;
	}

	public String getCopId() {
		return copId;
	}

	public void setCopId(String copId) {
		this.copId = copId;
	}

	public String getRepresentativeName() {
		return representativeName;
	}

	public void setRepresentativeName(String representativeName) {
		this.representativeName = representativeName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPwdFindQuestion() {
		return pwdFindQuestion;
	}

	public void setPwdFindQuestion(String pwdFindQuestion) {
		this.pwdFindQuestion = pwdFindQuestion;
	}

	public String getPwdFindAnswer() {
		return pwdFindAnswer;
	}

	public void setPwdFindAnswer(String pwdFindAnswer) {
		this.pwdFindAnswer = pwdFindAnswer;
	}

	public String getBlackListOrNotBlackList() {
		return blackListOrNotBlackList;
	}

	public void setBlackListOrNotBlackList(String blackListOrNotBlackList) {
		this.blackListOrNotBlackList = blackListOrNotBlackList;
	}

	public String getCopNo() {
		return copNo;
	}

	public void setCopNo(String copNo) {
		this.copNo = copNo;
	}

	public String getCopName() {
		return copName;
	}

	public void setCopName(String copName) {
		this.copName = copName;
	}

	public String getCopPhone() {
		return copPhone;
	}

	public void setCopPhone(String copPhone) {
		this.copPhone = copPhone;
	}

	public String getCopDeclarationEnroll() {
		return copDeclarationEnroll;
	}

	public void setCopDeclarationEnroll(String copDeclarationEnroll) {
		this.copDeclarationEnroll = copDeclarationEnroll;
	}

	public String getCopDeclarationNo() {
		return copDeclarationNo;
	}

	public void setCopDeclarationNo(String copDeclarationNo) {
		this.copDeclarationNo = copDeclarationNo;
	}

	public String getLeaveOrNotLeave() {
		return leaveOrNotLeave;
	}

	public void setLeaveOrNotLeave(String leaveOrNotLeave) {
		this.leaveOrNotLeave = leaveOrNotLeave;
	}
	
	@Override
	public String toString() {
		return "CoMember [membershipNumber=" + membershipNumber + ", pwd=" + pwd + ", pwdConfirm=" + pwdConfirm
				+ ", copId=" + copId + ", representativeName=" + representativeName + ", email=" + email + ", phone="
				+ phone + "address="+ address +", pwdFindQuestion=" + pwdFindQuestion + ", pwdFindAnswer=" + pwdFindAnswer
				+ ", blackListOrNotBlackList=" + blackListOrNotBlackList + ", copNo=" + copNo + ", copName=" + copName
				+ ", copPhone=" + copPhone + ", copDeclarationEnroll=" + copDeclarationEnroll + ", copDeclarationNo="
				+ copDeclarationNo + ", leaveOrNotLeave=" + leaveOrNotLeave + "]";
	}
	
}
