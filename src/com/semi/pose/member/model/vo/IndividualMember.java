package com.semi.pose.member.model.vo;

public class IndividualMember {
	
	
	private String membershipNumber;
	private String userId;
	private String pwd;
	private String email;
	private String phone;
	private String address;
	private String pwdQuestion;
	private String pwdAnswer;
	private String blacklist;
	private String userName;
	private String introduce;
	private String gender;
	private String birth;
	private String prefer;
	
	public IndividualMember() {}

	public IndividualMember(String membershipNumber, String userId, String pwd, String email, String phone, String address,
			String pwdQuestion, String pwdAnswer, String blacklist, String userName, String introduce, String gender,
			String birth, String prefer) {
		super();
		this.membershipNumber = membershipNumber;
		this.userId = userId;
		this.pwd = pwd;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.pwdQuestion = pwdQuestion;
		this.pwdAnswer = pwdAnswer;
		this.blacklist = blacklist;
		this.userName = userName;
		this.introduce = introduce;
		this.gender = gender;
		this.birth = birth;
		this.prefer = prefer;
	}

	public String getMembershipNumber() {
		return membershipNumber;
	}

	public void setMembershipNumber(String membershipNumber) {
		this.membershipNumber = membershipNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPwdQuestion() {
		return pwdQuestion;
	}

	public void setPwdQuestion(String pwdQuestion) {
		this.pwdQuestion = pwdQuestion;
	}

	public String getPwdAnswer() {
		return pwdAnswer;
	}

	public void setPwdAnswer(String pwdAnswer) {
		this.pwdAnswer = pwdAnswer;
	}

	public String getBlacklist() {
		return blacklist;
	}

	public void setBlacklist(String blacklist) {
		this.blacklist = blacklist;
	}

	public String getuserName() {
		return userName;
	}

	public void setuserName(String userName) {
		this.userName = userName;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getPrefer() {
		return prefer;
	}

	public void setPrefer(String prefer) {
		this.prefer = prefer;
	}

	@Override
	public String toString() {
		return "IndividualMember [membershipNumber=" + membershipNumber + ", userId=" + userId + ", pwd=" + pwd + ", email=" + email + ", phone="
				+ phone + ", address=" + address + ", pwdQuestion=" + pwdQuestion + ", pwdAnswer=" + pwdAnswer
				+ ", blacklist=" + blacklist + ", userName=" + userName + ", introduce=" + introduce + ", gender="
				+ gender + ", birth=" + birth + ", prefer=" + prefer + "]";
	}

}

