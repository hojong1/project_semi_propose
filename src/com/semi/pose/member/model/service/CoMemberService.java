package com.semi.pose.member.model.service;

import static com.semi.pose.common.JDBCTemplate.close;
import static com.semi.pose.common.JDBCTemplate.commit;
import static com.semi.pose.common.JDBCTemplate.rollback;
import static com.semi.pose.common.JDBCTemplate.getConnection;

import java.sql.Connection;

import com.semi.pose.member.model.dao.CoMemberDao;
import com.semi.pose.member.model.vo.CoMember;

public class CoMemberService {
	
	public int insertCorpMember(CoMember newMember) {
		Connection con = getConnection();
		
		int result = 0;
		
		result = new CoMemberDao().insertCorpMember(con,newMember);
		
		if(result>0){
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		
		return result;
	}
	

	public CoMember loginCheck(CoMember Comember) {
		Connection con = getConnection();
		
		CoMember user = new CoMemberDao().loginCheck(con,Comember);
		
		close(con);
		
		return user;
	}

	public int registerCheck(String copId) {
		  
		Connection con = getConnection();
			
			int result = 0 ;
			
			result = new CoMemberDao().registerCheck(con,copId);
			
			if(result>0){
				commit(con);
			}else {
				rollback(con);
			}
			close(con);
			
			
			return result;
		}

}
