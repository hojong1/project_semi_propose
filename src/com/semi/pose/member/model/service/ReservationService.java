package com.semi.pose.member.model.service;

import java.sql.Connection;

import com.semi.pose.member.model.dao.ReservationDao;
import com.semi.pose.member.model.vo.Reservation;
import static com.semi.pose.common.JDBCTemplate.getConnection;
import static com.semi.pose.common.JDBCTemplate.commit;
import static com.semi.pose.common.JDBCTemplate.rollback;
import static com.semi.pose.common.JDBCTemplate.close;

public class ReservationService {

	public int InsertReservation(Reservation newReservation) {

		Connection con = getConnection();
		
		int result = 0;
		
		result = new ReservationDao().insertReservation(con,newReservation);
		
		if(result>0){
			commit(con);
		}else {
			rollback(con);
		}
		
		
		
		return result;
		
	}

	

}
