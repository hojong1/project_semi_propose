package com.semi.pose.member.model.service;

import java.sql.Connection;
import static com.semi.pose.common.JDBCTemplate.getConnection;
import static com.semi.pose.common.JDBCTemplate.commit;
import static com.semi.pose.common.JDBCTemplate.rollback;
import static com.semi.pose.common.JDBCTemplate.close;

import com.semi.pose.member.model.dao.AdminDao;
import com.semi.pose.member.model.vo.Admin;

public class AdminService {

	public Admin loginCheck(Admin requestAdm) {
		Connection con = getConnection();
	      
	      Admin user = new AdminDao().loginCheck(con,requestAdm);
	      
	      close(con);
	      
	      return user;
	}

}
