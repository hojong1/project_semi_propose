package com.semi.pose.member.model.service;

import java.sql.Connection;

import com.semi.pose.member.model.dao.MemberDao;
import com.semi.pose.member.model.vo.IndividualMember;
import static com.semi.pose.common.JDBCTemplate.getConnection;
import static com.semi.pose.common.JDBCTemplate.commit;
import static com.semi.pose.common.JDBCTemplate.rollback;
import static com.semi.pose.common.JDBCTemplate.close;

public class MemberService {

	public int insertIndividualMember(IndividualMember newMember) {
		
		Connection con = getConnection();
		
		int result=0;
		
		result = new MemberDao().insertIndividualMember(con,newMember);
		
		if(result>0){
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		
		return result;
	}

	public IndividualMember updateMemberInformation(IndividualMember modifiedMember) {
		
		Connection con = getConnection();
		
		IndividualMember changedInformationMember = null;
		
		MemberDao md = new MemberDao();
		
		int updateResult = md.updateMemberInformation(con, modifiedMember);
		
		if(updateResult > 0) {
			changedInformationMember = md.selectChangedMemberInformation(con, modifiedMember);
			System.out.println("내정보수정 수정 성공");
			if(changedInformationMember != null) {
				commit(con);
				System.out.println("내정보수정 조회 성공");
			} else {
				rollback(con);
				System.out.println("내정보수정 조회 실패!!");
			}
			
		} else {
			rollback(con);
			System.out.println("내정보수정 수정 실패!!");
		}
		
		close(con);
		
		return changedInformationMember;
	}

	public IndividualMember loginCheck(IndividualMember requestMember) {
	      Connection con = getConnection();
	      
	      IndividualMember user = new MemberDao().loginCheck(con,requestMember);
	      
	      close(con);
	      
	      return user;
	   }
	
	public int registerCheck(String userId) {
		Connection con = getConnection();
		
		int result = 0 ;
		
		result = new MemberDao().registerCheck(con,userId);
		
		if(result>0){
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		
		return result;
	}

	public IndividualMember existId(String userId,String email) {
		Connection con = getConnection();
		
		IndividualMember UserId = new MemberDao().existId(con,userId,email); 
		
		close(con);
		
		
		return UserId;
	}

	public IndividualMember updateMemberPassword(IndividualMember modifiedMember) {
		
		Connection con = getConnection();
		
		IndividualMember changedInformationMember = null;
		
		MemberDao md = new MemberDao();
		
		int updateResult = md.updateMemberPassword(con, modifiedMember);
		
		if(updateResult > 0) {
			changedInformationMember = md.selectChangedMemberInformation(con, modifiedMember);
			System.out.println("내정보수정 수정 성공");
			if(changedInformationMember != null) {
				commit(con);
				System.out.println("내정보수정 조회 성공");
			} else {
				rollback(con);
				System.out.println("내정보수정 조회 실패!!");
			}
			
		} else {
			rollback(con);
			System.out.println("내정보수정 수정 실패!!");
		}
		
		close(con);
		
		return changedInformationMember;
	}
	
}
