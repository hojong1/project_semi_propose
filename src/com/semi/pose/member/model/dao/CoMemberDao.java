package com.semi.pose.member.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.semi.pose.member.model.vo.CoMember;

import static com.semi.pose.common.JDBCTemplate.close;

public class CoMemberDao {
	
	private Properties prop = new Properties();
	
	public CoMemberDao(){
		String fileName = CoMemberDao.class.getResource("/sql/member/member-query(jaemin).properties").getPath();
	
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	
	
	}
	
	
	public int insertCorpMember(Connection con, CoMember newMember) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertCorpMember");
		
		try {
			
			pstmt = con.prepareStatement(query);
			
			
			pstmt.setString(1, newMember.getPwd());
			pstmt.setString(2, newMember.getCopId());
			pstmt.setString(3, newMember.getRepresentativeName());
			pstmt.setString(4, newMember.getEmail());
			pstmt.setString(5, newMember.getPhone());
			pstmt.setString(6, newMember.getAddress());
			pstmt.setString(7, newMember.getPwdFindQuestion());
			pstmt.setString(8, newMember.getPwdFindAnswer());
			pstmt.setString(9, newMember.getCopName());
			pstmt.setString(10, newMember.getCopPhone());
			pstmt.setString(11, newMember.getCopDeclarationEnroll());
			pstmt.setString(12, newMember.getCopDeclarationNo());
			
			
			
			
			result=pstmt.executeUpdate();
			
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}
	
	
	public CoMember loginCheck(Connection con, CoMember comember) {
		 PreparedStatement pstmt = null;
	      ResultSet rset = null;
	      CoMember loginUser = null;
	      
	      String query = "SELECT * FROM CORPORATION_MEMBER WHERE ID=? AND PWD=? AND LEAVEORNOTLEAVE=0";
	      
	      try {
	         pstmt = con.prepareStatement(query);
	         pstmt.setString(1, comember.getCopId());
	         pstmt.setString(2, comember.getPwd());
	         
	         rset=pstmt.executeQuery();
	         
	         if(rset.next()) {
	            loginUser = new CoMember();
	            loginUser.setMembershipNumber(rset.getString(1));
	            loginUser.setPwd(rset.getString(2));
	            loginUser.setCopId(rset.getString(3));
	            loginUser.setRepresentativeName(rset.getString(4));
	            loginUser.setEmail(rset.getString(5));
	            loginUser.setPhone(rset.getString(6));
	            loginUser.setAddress(rset.getString(7));
	            loginUser.setPwdFindQuestion(rset.getString(8));
	            loginUser.setPwdFindAnswer(rset.getString(9));
	            loginUser.setBlackListOrNotBlackList(rset.getString(10));
	            loginUser.setCopName(rset.getString(11));
	            loginUser.setCopPhone(rset.getString(12));
	            loginUser.setCopDeclarationEnroll(rset.getString(13));
	            loginUser.setCopDeclarationNo(rset.getString(14));
	            loginUser.setLeaveOrNotLeave(rset.getString(15));
	            
	         }
	      } catch (SQLException e) {
	         e.printStackTrace();
	      }finally {
	         close(pstmt);
	         close(rset);
	      }
	      
	      return loginUser;
	   }

	

	public int registerCheck(Connection con, String copId) {
		PreparedStatement pstmt= null;
		ResultSet rset = null;
		
		String query = prop.getProperty("checkUserCopId");
		
		try {
			
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, copId);
			
			rset =pstmt.executeQuery();
			
			if(rset.next()||copId.equals("")){//결과가 있다면
				return 0;
			}else {
				return 1;
			}
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}finally {
			
			
				try {
					if(rset!=null){
						rset.close();
					}
					if(pstmt !=null){
						pstmt.close();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			;
			
		}
		
		
		
		return -1; //DB오류일때
	}

}
