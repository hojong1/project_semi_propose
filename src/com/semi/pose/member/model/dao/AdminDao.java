package com.semi.pose.member.model.dao;

import static com.semi.pose.common.JDBCTemplate.close;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.semi.pose.member.model.vo.Admin;

public class AdminDao {

	public Admin loginCheck(Connection con, Admin requestAdm) {
		PreparedStatement pstmt = null;
	      ResultSet rset = null;
	      Admin loginUser = null;
	      
	      String query = "SELECT * FROM ADMIN WHERE ID=? AND PWD=?";
	      
	      try {
	         pstmt = con.prepareStatement(query);
	         pstmt.setString(1, requestAdm.getUserId());
	         pstmt.setString(2, requestAdm.getPwd());
	         
	         rset=pstmt.executeQuery();
	         
	         if(rset.next()) {
	            loginUser = new Admin();
	            loginUser.setUserId(rset.getString(1));
	            loginUser.setPwd(rset.getString(2));
	            loginUser.setName(rset.getString(3));
	            loginUser.setEmail(rset.getString(4));
	            loginUser.setPhone(rset.getString(5));
	           
	            
	         }
	      } catch (SQLException e) {
	         e.printStackTrace();
	      }finally {
	         close(pstmt);
	         close(rset);
	      }
	      
	      return loginUser;
	   }

}
