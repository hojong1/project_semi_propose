package com.semi.pose.member.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.semi.pose.member.model.vo.IndividualMember;
import static com.semi.pose.common.JDBCTemplate.close;

public class MemberDao {

	private Properties prop = new Properties();
	
	
	public MemberDao(){
		
		//String fileName = MemberDao.class.getResource("/sql/member/member-query.properties").getPath();
		//String fileName = MemberDao.class.getResource("/sql/member/member-query-dongyub.properties").getPath();
		String fileName = MemberDao.class.getResource("/sql/member/member-query(jaemin).properties").getPath();
				
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	
	
	public int insertIndividualMember(Connection con, IndividualMember newMember) {
		
		PreparedStatement pstmt = null;
		int result = 0;

	
		String query = prop.getProperty("insertIndividualMember");
		
		System.out.println("newMember : "+newMember);
		try {
			pstmt= con.prepareStatement(query);
			
			pstmt.setString(1, newMember.getUserId());
			pstmt.setString(2, newMember.getPwd());
			pstmt.setString(3, newMember.getEmail());
			pstmt.setString(4, newMember.getPhone());
			pstmt.setString(5, newMember.getAddress());
			pstmt.setString(6, newMember.getPwdQuestion());
			pstmt.setString(7, newMember.getPwdAnswer());
			pstmt.setString(8, newMember.getuserName());
			pstmt.setString(9, newMember.getGender());
			pstmt.setString(10, newMember.getBirth());
			
			
			result=pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		return result;
		
		
	}
	
	public IndividualMember loginCheck(Connection con, IndividualMember requestMember) {
	      
	      PreparedStatement pstmt = null;
	      ResultSet rset = null;
	      IndividualMember loginUser = null;
	      
	      String query = "SELECT * FROM INDIVIDUAL_MEMBER WHERE ID=? AND PWD=? AND LEAVEORNOTLEAVE=0";
	      
	      try {
	         pstmt = con.prepareStatement(query);
	         pstmt.setString(1, requestMember.getUserId());
	         pstmt.setString(2, requestMember.getPwd());
	         
	         rset=pstmt.executeQuery();
	         
	         if(rset.next()) {
	            loginUser = new IndividualMember();
	            loginUser.setMembershipNumber(rset.getString(1));
	            loginUser.setUserId(rset.getString(2));
	            loginUser.setPwd(rset.getString(3));
	            loginUser.setEmail(rset.getString(4));
	            loginUser.setPwdQuestion(rset.getString(5));
	            loginUser.setBlacklist(rset.getString(6));
	            loginUser.setuserName(rset.getString(7));
	            loginUser.setIntroduce(rset.getString(8));
	            loginUser.setGender(rset.getString(9));
	            loginUser.setBirth(rset.getString(10));
	            loginUser.setPrefer(rset.getString(11));
	            
	         }
	      } catch (SQLException e) {
	         e.printStackTrace();
	      }finally {
	         close(pstmt);
	         close(rset);
	      }
	      
	      return loginUser;
	   }

	public int updateMemberInformation(Connection con, IndividualMember modifiedMember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateMember");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, modifiedMember.getuserName());
			pstmt.setString(2, modifiedMember.getEmail());
			pstmt.setString(3, modifiedMember.getPhone());
			pstmt.setString(4, modifiedMember.getAddress());
			pstmt.setString(5, modifiedMember.getPwdQuestion());
			pstmt.setString(6, modifiedMember.getPwdAnswer());
			pstmt.setString(7, modifiedMember.getUserId());
			pstmt.setString(8, modifiedMember.getPwd());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}


	public IndividualMember selectChangedMemberInformation(Connection con, IndividualMember modifiedMember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		IndividualMember changedMemberInformation = null;
		
		String query = prop.getProperty("selectChangedMember");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, modifiedMember.getUserId());
			pstmt.setString(2, modifiedMember.getPwd());
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				changedMemberInformation = new IndividualMember();
				
				changedMemberInformation.setMembershipNumber(rset.getString("MEMBERSHIPNUMBER"));
				changedMemberInformation.setuserName(rset.getString("USERNAME"));
				changedMemberInformation.setEmail(rset.getString("EMAIL"));
				changedMemberInformation.setPhone(rset.getString("PHONE"));
				changedMemberInformation.setAddress(rset.getString("ADDRESS"));
				changedMemberInformation.setPwdQuestion(rset.getString("PASSWORDFINDQUESTION"));
				changedMemberInformation.setPwdAnswer(rset.getString("PASSWORDFINDANSWER"));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return changedMemberInformation;
	}
	
public int registerCheck(Connection con, String userId) {
		
		
		PreparedStatement pstmt= null;
		ResultSet rset = null;
		
		String query = prop.getProperty("checkUserId");
		
		try {
			
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, userId);
			
			rset =pstmt.executeQuery();
			
			if(rset.next()||userId.equals("")){//결과가 있다면
				return 0;
			}else {
				return 1;
			}
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}finally {
			
			
				try {
					if(rset!=null){
						rset.close();
					}
					if(pstmt !=null){
						pstmt.close();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			;
			
		}
		
		
		
		
		return -1; //DB오류일때
	}


public IndividualMember existId(Connection con, String userId, String email) {
	
	
	PreparedStatement pstmt= null;
	ResultSet rset = null;
	IndividualMember existMember = null;
	
	String query = prop.getProperty("existUserId");
	
	
	try {
		
		pstmt = con.prepareStatement(query);
		
		pstmt.setString(1, userId);
		pstmt.setString(2, email);
		
		rset =pstmt.executeQuery();
		
		if(rset.next()) {
			
			existMember = new IndividualMember();
			
			existMember.setMembershipNumber(rset.getString("MEMBERSHIPNUMBER"));
			existMember.setUserId(rset.getString("ID"));
			existMember.setuserName(rset.getString("USERNAME"));
			existMember.setEmail(rset.getString("EMAIL"));
			existMember.setPhone(rset.getString("PHONE"));
			existMember.setAddress(rset.getString("ADDRESS"));
			existMember.setPwdQuestion(rset.getString("PASSWORDFINDQUESTION"));
			existMember.setPwdAnswer(rset.getString("PASSWORDFINDANSWER"));
			
		}
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		close(pstmt);
		close(rset);
		}
	
	return existMember;
	
	}


public int updateMemberPassword(Connection con, IndividualMember modifiedMember) {	
	
	PreparedStatement pstmt = null;
	int result = 0;
	
	String query = prop.getProperty("updatePassword");
	
	
	try {
		pstmt = con.prepareStatement(query);

		pstmt.setString(1, modifiedMember.getPwd());
		pstmt.setString(2, modifiedMember.getUserId());
		
		
		result = pstmt.executeUpdate();
		
	} catch (SQLException e) {
		
		e.printStackTrace();
	} finally {
		close(pstmt);
	}
	
	return result;
}

}
