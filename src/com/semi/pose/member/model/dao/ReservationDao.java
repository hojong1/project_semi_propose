package com.semi.pose.member.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import com.semi.pose.member.model.vo.Reservation;
import static com.semi.pose.common.JDBCTemplate.close;

public class ReservationDao {
	
private Properties prop = new Properties();
	
	
	public ReservationDao(){
		
		//String fileName = MemberDao.class.getResource("/sql/member/member-query.properties").getPath();
		//String fileName = MemberDao.class.getResource("/sql/member/member-query-dongyub.properties").getPath();
		String fileName = MemberDao.class.getResource("/sql/member/member-query(jaemin).properties").getPath();
				
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	
	public int insertReservation(Connection con, Reservation newReservation) {
		
		PreparedStatement pstmt =null;
		int result=0;
		
		String query = prop.getProperty("insertReservation");
		
		
		try {
			
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, newReservation.getUserName());
			pstmt.setString(2, newReservation.getPhone());
			pstmt.setString(3, newReservation.getReservationDate());
			pstmt.setString(4, newReservation.getUserId());
			pstmt.setString(5, newReservation.getPaymethod());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		
		return result;
	}

	

}
