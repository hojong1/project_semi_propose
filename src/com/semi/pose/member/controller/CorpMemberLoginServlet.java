package com.semi.pose.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.semi.pose.member.model.service.CoMemberService;
import com.semi.pose.member.model.vo.CoMember;

/**
 * Servlet implementation class CorpMemberLoginServlet
 */
@WebServlet("/corpLogin.me")
public class CorpMemberLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CorpMemberLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	      
	      String userId=request.getParameter("id");
	      String userPwd=request.getParameter("password");
	      
	      System.out.println("userId : " + userId);
	      System.out.println("userPwd : " + userPwd);
	      
	      CoMember requestCoMember = new CoMember();
	      requestCoMember.setCopId(userId);
	      requestCoMember.setPwd(userPwd);
	      
	      CoMember loginUser= new CoMemberService().loginCheck(requestCoMember);
	      
	      System.out.println(loginUser);
	      
	      String path="";
	      if(loginUser !=null) {
	         HttpSession session = request.getSession();
	         session.setAttribute("loginUser", loginUser);
	         
	         path="views/member/corporate/co_main.jsp";
	         response.sendRedirect(path);
	         
	      } else {
	         request.setAttribute("message", "로그인 실패!");
	         
	         path="views/common/errorPage.jsp";
	         request.getRequestDispatcher(path).forward(request, response);
	      }
	   }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
