package com.semi.pose.member.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.semi.pose.member.model.service.MemberService;
import com.semi.pose.member.model.vo.IndividualMember;

/**
 * Servlet implementation class MemberPasswordSearchServlet
 */
//insertMember.me
//pwdSearch.bo
@WebServlet("/pwdSearch.bo")
public class MemberPasswordSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberPasswordSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		request.setCharacterEncoding("UTF-8");

		
		System.out.println("Servlet 접근!");
		
		String userId = request.getParameter("userId");
		String email = request.getParameter("email");
		String emailPwd = request.getParameter("emailPwd");
		
		System.out.println("emailPwd : "+emailPwd);
		
		System.out.println("userId : "+userId+", email: "+email);
		
		
		IndividualMember m = new MemberService().existId(userId,email);
		
		System.out.println("회원 m 정보 :"+m);
		
		String page="";
		
		
		if(m==null || !m.getEmail().equals(email)){
			System.out.println("예");
			page = "views/common/errorPage.jsp";
			
			
			request	.setAttribute("message", "아이디나 이메일 정보가 맞지 않습니다.");
			request.setAttribute("loc", "/member/searchPw");
			
			
			request.getRequestDispatcher(page).forward(request, response);
			return;
		}
		
		
		//mail server 설정
		String host = "smtp.naver.com";
		String user = email; //자신의 네이버 계정
		String password = emailPwd; //자신의 네이버 패스워드
		
		//메일 받을 주소
		String to_email = m.getEmail();
		
		//SMTP 서버 정보를 설정한다.
		Properties props = new Properties();
		props.put("mail.smtp.host",host);
		props.put("mail.smtp.port",465);
		props.put("mail.smtp.auth","true");
		props.put("mail.smtp.ssl.enable","true");
		
		//인증 번호 생성기
		StringBuffer temp = new StringBuffer();
		Random rnd = new Random();
		
		for(int i=0;i<10;i++)
		{
			int rIndex = rnd.nextInt(3);
			switch(rIndex){
			case 0:
				//a-z
				temp.append((char)(int)(rnd.nextInt(26)+97));
				break;
			case 1:
				//A-Z
				temp.append((char)(int)(rnd.nextInt(26)+65));
				break;
			case 2:
				//0-9
				temp.append((rnd.nextInt(10)));
				break;
				}
			}
		String AuthenticationKey = temp.toString();
		System.out.println(AuthenticationKey);
			
		Session session = Session.getDefaultInstance(props,new javax.mail.Authenticator(){
			
			protected PasswordAuthentication getPasswordAuthentication(){
				
				return new PasswordAuthentication(user,password);
				
				}
			
			});
			
			//email 전송
		
		
			try {
				MimeMessage msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(user,"KH BOOKS"));
				msg.addRecipient(Message.RecipientType.TO,new InternetAddress(to_email));
				
				//메일 제목
				msg.setSubject("안녕하세요 연결 인증 메일입니다.");
				//메일 내용
				msg.setText("인증 번호는 :"+temp );
			
				Transport.send(msg);
				System.out.println("이메일 전송");
				
				
			} catch (UnsupportedEncodingException | MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			HttpSession saveKey = request.getSession();
			saveKey.setAttribute("AuthenticationKey", AuthenticationKey);
			//패스워드 바꿀 때 뭘 바꿀지 조건에 들어가는 id
			request.setAttribute("id", userId);
			request.getRequestDispatcher("/views/common/passwordChange.jsp").forward(request,response);
			
		
		
		
		
		
		
		
		
		
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
