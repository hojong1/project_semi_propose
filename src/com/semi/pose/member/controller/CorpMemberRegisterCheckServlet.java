package com.semi.pose.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.semi.pose.member.model.service.CoMemberService;
import com.semi.pose.member.model.service.MemberService;

/**
 * Servlet implementation class CorpMemberRegisterCheckServlet
 */
@WebServlet("/checkCorpId.bo")
public class CorpMemberRegisterCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CorpMemberRegisterCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String result ="";
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		String copId = request.getParameter("copId");
		//해당 체크함수는 숫자 결과값 나오기 때문에 공백 추가해서 문자열 추가 ""
		//해당 결과가 0, 1 나온 것을 보내니 ajax에서 결과값으로 받아서 처리
		
		response.getWriter().write(new CoMemberService().registerCheck(copId)+"");
		
		System.out.println(result);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
