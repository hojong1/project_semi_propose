package com.semi.pose.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.semi.pose.member.model.service.MemberService;
import com.semi.pose.member.model.vo.IndividualMember;

/**
 * Servlet implementation class NewPasswordServlet
 */
@WebServlet("/newPassword.bo")
public class NewPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewPasswordServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		
	     String AuthenticationKey = (String)request.getSession().getAttribute("AuthenticationKey");
	     String AuthenticationUser = request.getParameter("AuthenticationUser");
	     
	     
	     System.out.println("AuthenticationKey : "+AuthenticationKey+", AuthenticationUser : "+AuthenticationUser);
	     
	     
	     if(!AuthenticationKey.equals(AuthenticationUser))
	        {
	            System.out.println("인증번호 일치하지 않음");
	            request.setAttribute("msg", "인증번호가 일치하지 않습니다");
	            request.setAttribute("loc", "/member/searchPw");
	            request.getRequestDispatcher("/views/common/msg.jsp").forward(request, response);
	            return;
	        }
	     
	     
	     String password = request.getParameter("password");
	     String userId = request.getParameter("userId");
	     
	     System.out.println("새 비밀번호 : "+password);
	     
	     IndividualMember modifiedMember = new IndividualMember();
	     
	     modifiedMember.setUserId(userId);
	     modifiedMember.setPwd(password);
	     
	     
	     IndividualMember changedInformationMember = new MemberService().updateMemberPassword(modifiedMember);
	     
	     
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
