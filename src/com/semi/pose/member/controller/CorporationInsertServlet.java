package com.semi.pose.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.semi.pose.member.model.service.CoMemberService;
import com.semi.pose.member.model.vo.CoMember;

/**
 * Servlet implementation class CorporationInsertServlet
 */
@WebServlet("/insertCorp.me")
public class CorporationInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CorporationInsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String pwd = request.getParameter("pwd");
		String copId = request.getParameter("userId");
		String representativeName = request.getParameter("userName");
		String email = request.getParameter("email");
		String tel1 = request.getParameter("tel1");
		String tel2 = request.getParameter("tel2");
		String tel3 = request.getParameter("tel3");
		String phone = tel1+"-"+tel2+"-"+tel3;
		String corptel1 = request.getParameter("corptel1");
		String corptel2 = request.getParameter("corptel2");
		String corptel3 = request.getParameter("corptel3");
		String copName = request.getParameter("copName");
		String copPhone = corptel1+"-"+corptel2+"-"+corptel3;
		String copDeclarationEnroll = request.getParameter("copDeclarationEnroll");
		String copDeclarationNo =  request.getParameter("copDeclarationNo");
		String zip = request.getParameter("zipCode"); 
		String add1 = request.getParameter("address1");
		String add2 = request.getParameter("address2");
		String address = zip + "$" + add1 + "$" + add2;
		String pwdQuestion = request.getParameter("pwdfindQuestion");
		String pwdAnswer = request.getParameter("pwdfindAnswer");
		
		CoMember newMember = new CoMember();
		
		newMember.setCopId(copId);
		newMember.setPwd(pwd);
		newMember.setRepresentativeName(representativeName);
		newMember.setEmail(email);
		newMember.setPhone(phone);
		newMember.setCopPhone(copPhone);
		newMember.setCopName(copName);
		newMember.setCopDeclarationEnroll(copDeclarationEnroll);
		newMember.setCopDeclarationNo(copDeclarationNo);
		newMember.setAddress(address);
		newMember.setPwdFindQuestion(pwdQuestion);
		newMember.setPwdFindAnswer(pwdAnswer);
		
		System.out.println("insert request corpMember : "+newMember);
		
		//3.서비스 로직 호출
		int result = new CoMemberService().insertCorpMember(newMember);
		
		//4.성공 실패 여부 뷰 연결
		String page ="";
		if(result>0){
			System.out.println("회원가입 성공");
			
			
			page = "/sm/index.jsp";
			
			response.sendRedirect(page);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("message", "회원가입 실패!");
			
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
