package com.semi.pose.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.semi.pose.member.model.service.MemberService;
import com.semi.pose.member.model.vo.IndividualMember;

@WebServlet("/updateMember.me")
public class MemberUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MemberUpdateServlet() {
        super();
       
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("userName");
		String userId = request.getParameter("userId");
		String userPwd = request.getParameter("userPwd");
		String email = request.getParameter("email");
		String tel1 = request.getParameter("tel1");
		String tel2 = request.getParameter("tel2");
		String tel3 = request.getParameter("tel3");
		String phone = tel1 + "-" + tel2 + "-" + tel3;
		String zipCode = request.getParameter("zipCode");
		String address1 = request.getParameter("address1");
		String address2 = request.getParameter("address2");
		String address = zipCode + "$" + address1 + "$" + address2 ;
		String pwdQuestion = request.getParameter("pwdQuestion");
		String pwdAnswer = request.getParameter("pwdAnswer");
		
		System.out.println(request.getParameter("pwdQuestion"));
		System.out.println(request.getParameter("pwdAnswer"));
		
		IndividualMember modifiedMember = new IndividualMember();
		modifiedMember.setUserId(userId);
		modifiedMember.setPwd(userPwd);
		modifiedMember.setEmail(email);
		modifiedMember.setPhone(phone);
		modifiedMember.setAddress(address);
		modifiedMember.setPwdQuestion(pwdQuestion);
		modifiedMember.setPwdAnswer(pwdAnswer);
		modifiedMember.setuserName(userName);
		
		IndividualMember changedInformationMember = new MemberService().updateMemberInformation(modifiedMember);
		
		String path = "";
		if( changedInformationMember != null ) {
			request.getSession().setAttribute("loginUser", changedInformationMember);
			
			request.setAttribute("message", "updateMember");
			path = "views/common/successPage.jsp";
			
			request.getRequestDispatcher(path).forward(request, response);
			
		} else {
			
			request.setAttribute("message", "내 정보 수정 실패!");
			path = "views/common/errorPage.jsp";
			
			response.sendRedirect(path);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
