package com.semi.pose.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.semi.pose.member.model.service.MemberService;
import com.semi.pose.member.model.vo.IndividualMember;

@WebServlet("/insertMember.me")
public class MemberInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MemberInsertServlet() {
        super();
       
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String userId = request.getParameter("userId");
		String userPwd = request.getParameter("userPwd");
		String userName = request.getParameter("userName");
		String email = request.getParameter("email");
		String tel1 = request.getParameter("tel1");
		String tel2 = request.getParameter("tel2");
		String tel3 = request.getParameter("tel3");
		String phone = tel1 + "-"+ tel2 + "-" + tel3 ;
		String zip = request.getParameter("zipCode"); 
		String add1 = request.getParameter("address1");
		String add2 = request.getParameter("address2");
		String address = zip + "$" + add1 + "$" + add2;
		//주소에서 사용하지 않는 기호 사용
		String pwdQuestion = request.getParameter("pwdQuestion");
		String pwdAnswer = request.getParameter("pwdAnswer");
		String gender = request.getParameter("gender");
		String birth = request.getParameter("birth");
		
		
		IndividualMember newMember = new IndividualMember();
		newMember.setUserId(userId);
		newMember.setPwd(userPwd);
		newMember.setuserName(userName);
		newMember.setEmail(email);
		newMember.setPhone(phone);
		newMember.setAddress(address);
		newMember.setPwdQuestion(pwdQuestion);
		newMember.setPwdAnswer(pwdAnswer);
		newMember.setGender(gender);
		newMember.setBirth(birth);
		
		System.out.println("insert request member: "+newMember);
		
		//3.서비스 로직 호출
		int result = new MemberService().insertIndividualMember(newMember);
		
	
		//4.성공 실패 여부 뷰 연결
		String page ="";
		if(result>0){
			System.out.println("회원가입 성공");
			
			
			page = "/sm/index.jsp";
			
			response.sendRedirect(page);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("message", "회원가입 실패!");
			
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
		
	}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
