package com.semi.pose.member.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.semi.pose.member.model.service.MemberService;
import com.semi.pose.member.model.vo.IndividualMember;

/**
 * Servlet implementation class TempFindPwdServlet
 */
@WebServlet("/findPwd.me")
public class TempFindPwdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TempFindPwdServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		System.out.println("Servlet 접근!");
		
		String userId = request.getParameter("userId");
		String email = request.getParameter("email");
		
		System.out.println("userId : "+userId+", email: "+email);
		
		
		IndividualMember m = new MemberService().existId(userId,email);
		
		System.out.println("회원 m 정보 :"+m);
		
		String page="";
		
		
		if(m==null || !m.getEmail().equals(email)){
			System.out.println("메세지");
			request	.setAttribute("message", "아이디나 이메일 정보가 맞지 않습니다.");
			//request.setAttribute("loc", "/member/searchPw");
			
			page = "views/common/errorPage.jsp";
			request.getRequestDispatcher(page).forward(request, response);
			return;
		}
		
		
		//mail server 설정
		String host = "smt.naver.com";
		String user  = ""; //자신의 네이버 계정
		String password = ""; //자신의 네이버 패스워드
		
		//메일 받을 주소
		String to_email = m.getEmail();
		
		//SMTP 서버 정보를 설정한다.
		Properties props = new Properties();
		props.put("mail.smtp.host",host);
		
		props.put("mail.smtp.port",465);
		props.put("mail.smtp.startls.enable","true");
		props.put("mail.smtp.ssl.enable","true");
		props.put("mail.smtp.auth","true");
		props.put("mail.smtp.debug","true");
		props.put("mail.smtp.socketFactory.port","465");
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback","false");	
		
		Authenticator auth = new MyAuthentication();
		
		Session session = Session.getDefaultInstance(props,auth);
		MimeMessage msg = new MimeMessage(session);
		
		
		
		try {
		
			msg.setSentDate(new Date());
		
			InternetAddress from = new InternetAddress("보내는 사람");
			
			//이메일 발신자
			msg.setFrom(from);
			
			//이메일 수신자
			String emailRecipient = request.getParameter("userName");
			//사용자가 입력한 이메일 받아오기
			
			InternetAddress to = new InternetAddress(emailRecipient);
			msg.setRecipient(Message.RecipientType.TO, to);
			
			
			//이메일 제목
			msg.setSubject("비밀번호 인증번호","UTF-8");
			
			//이메일 내용
			String code = request.getParameter("code_check"); //인증번호 값 받기
			
			request.setAttribute("code", code);
			msg.setText(code,"UTF-8");
			
			//이메일 헤더
			msg.setHeader("context-Type", "text/html");
			
			//메일 보내기
			javax.mail.Transport.send(msg);
			System.out.println("보냄!");
			
			
			
		} catch (MessagingException e) {
	
			
			e.printStackTrace();
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("인증번호 확인하는 페이지");
		
		rd.forward(request, response);
		
		
		//인증 번호 생성기
		/*StringBuffer temp = new StringBuffer();
		Random rnd = new Random();
		
		for(int i=0;i<10;i++){
			int rIndex = rnd.nextInt(3);
			switch(rIndex){
			case 0:
				//a-z
				temp.append((char)(int)(rnd.nextInt(26)+97));
				break;
			case 1:
				//A-Z
				temp.append((char)(int)(rnd.nextInt(26)+65));
				break;
			case 2:
				//0-9
				temp.append((rnd.nextInt(10)));
				break;
			
			}
		String AuthenticationKey = temp.toString();
		System.out.println("AuthenticationKey");
		System.out.println(AuthenticationKey);
			
		Session session = Session.getDefaultInstance(props,new javax.mail.Authenticator(){
			
			
			protected PasswordAuthentication getPasswordAuthentication(){
				
				return new PasswordAuthentication(user,password);
				
				}
			
			});
			System.out.println(session);
			//email 전송
			System.out.println("세션전송");
			
			try {
				MimeMessage msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(user,"whlthd1207"));
				msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to_email));
				System.out.println("메일 전송");
				//메일 제목
				msg.setSubject("안녕하세요 연결 인증 메일입니다.");
				//메일 내용
				msg.setText("인증 번호는 :"+temp );
			
				Transport.send(msg);
				System.out.println("이메일 전송");
				
				
			} catch (UnsupportedEncodingException | MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			HttpSession saveKey = request.getSession();
			saveKey.setAttribute("AuthenticationKey", AuthenticationKey);
			//패스워드 바꿀 때 뭘 바꿀지 조건에 들어가는 id
			//request.setAttribute("id", userId);
			//request.getRequestDispatcher(page).forward(request,response);
			
		}*/
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	

	
}

class MyAuthentication extends Authenticator{
	
	PasswordAuthentication pa;
	
	public MyAuthentication(){
		
		String id = "whlthd1207";
		String pw = "sjk@931207";
		
		//ID와 비밀번호를 입력한다.
		pa = new PasswordAuthentication(id,pw);
	}
	
	
	public PasswordAuthentication getPasswordAuthentication(){
		return pa;
	}
	
}
