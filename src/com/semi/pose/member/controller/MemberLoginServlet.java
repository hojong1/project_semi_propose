package com.semi.pose.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.semi.pose.member.model.service.MemberService;
import com.semi.pose.member.model.vo.IndividualMember;

@WebServlet("/login.me")
public class MemberLoginServlet extends HttpServlet {
   private static final long serialVersionUID = 1L;
       
    public MemberLoginServlet() {
        super();
         
    }

   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      
      String userId=request.getParameter("id");
      String userPwd=request.getParameter("password");
      
      System.out.println("userId : " + userId);
      System.out.println("userPwd : " + userPwd);
      
      IndividualMember requestMember = new IndividualMember();
      requestMember.setUserId(userId);
      requestMember.setPwd(userPwd);
      
      IndividualMember loginUser= new MemberService().loginCheck(requestMember);
      
      System.out.println(loginUser);
      
      String path="";
      if(loginUser !=null) {
         HttpSession session = request.getSession();
         session.setAttribute("loginUser", loginUser);
         
         path="views/member/individual/in_main.jsp";
         response.sendRedirect(path);
         
      } else {
         request.setAttribute("message", "로그인 실패!");
         
         path="views/common/errorPage.jsp";
         request.getRequestDispatcher(path).forward(request, response);
      }
   }

   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      
      doGet(request, response);
   }

}
