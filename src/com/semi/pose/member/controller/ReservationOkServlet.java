package com.semi.pose.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.semi.pose.member.model.service.ReservationService;
import com.semi.pose.member.model.vo.Reservation;

/**
 * Servlet implementation class ReservationOkServlet
 */
@WebServlet("/reservationOk.me")
public class ReservationOkServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReservationOkServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	
		String userName = request.getParameter("name");
		String userId = request.getParameter("userId");
		String reservationDate = request.getParameter("regdate");
		String phone = request.getParameter("phone");
		String paymethod = request.getParameter("paymentmethod");
		
		System.out.println("userName : "+userName+", userId : "+userId+", reservationDate : "+reservationDate+", paymethod : "+paymethod);
		
		
		Reservation newReservation = new Reservation();
		
		newReservation.setUserName(userName);
		newReservation.setPhone(phone);
		newReservation.setUserId(userId);
		newReservation.setReservationDate(reservationDate);
		newReservation.setPaymethod(paymethod);
		
		System.out.println(newReservation);
		
		int result = new ReservationService().InsertReservation(newReservation);
	
		
		String page="";
		if(result>0) {
			System.out.println("예약 성공");
			
			page = "/sm/views/common/reservation.html";
			response.sendRedirect(page);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("message", "예약 실패!");
			
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
