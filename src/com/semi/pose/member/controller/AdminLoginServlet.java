package com.semi.pose.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.semi.pose.member.model.service.AdminService;
import com.semi.pose.member.model.vo.Admin;


/**
 * Servlet implementation class AdminLoginServlet
 */
@WebServlet("/adminLogin.me")
public class AdminLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	      
	      String adminId=request.getParameter("id");
	      String pwd=request.getParameter("password");
	      
	      System.out.println("userId : " + adminId);
	      System.out.println("userPwd : " + pwd);
	      
	     Admin requestAdm = new Admin();
	     requestAdm.setUserId(adminId);
	     requestAdm.setPwd(pwd);
	      
	     Admin loginUser= new AdminService().loginCheck(requestAdm);
	      
	      System.out.println("loginUser=" + loginUser);
	      
	      String path="";
	      if(loginUser !=null) {
	         HttpSession session = request.getSession();
	         session.setAttribute("loginUser", loginUser);
	         path="views/admin/adminIndex.jsp";
	         
	      } else {
	         request.setAttribute("message", "로그인 실패!");
	         
	         path="views/common/errorPage.jsp";
	      }
	         request.getRequestDispatcher(path).forward(request, response);
	     
	   }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
