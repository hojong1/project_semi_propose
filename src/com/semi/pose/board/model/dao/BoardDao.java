package com.semi.pose.board.model.dao;



import static com.semi.pose.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.semi.pose.board.model.vo.Board;
import com.semi.pose.board.model.vo.PageInfo;

public class BoardDao {
		private Properties prop = new Properties();
		
		public BoardDao() {
			String fileName = BoardDao.class.getResource("/sql/board/board-query(baekHojong).properties").getPath();
			
			try {
				prop.load(new FileReader(fileName));
				
			} catch (IOException e) {

				e.printStackTrace();
			}
			
		}
		
	public int insertBoard(Connection con, Board newBoard) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertBoard");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, newBoard.getqTitle());
			pstmt.setString(2, newBoard.getqContent());
			pstmt.setString(3,newBoard.getMemberShipNumber());
			pstmt.setInt(4, newBoard.getqCategory());
			//pstmt.setInt(4, newBoard.getqCategory());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}

	public int getListCount(Connection con) {
		Statement stmt = null;
		int listCount = 0;
		ResultSet rset = null;
		
		String query = prop.getProperty("listCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
		
			
			
		} catch (SQLException e) {


			e.printStackTrace();
		}finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

	public ArrayList<Board> selectListWithPaging(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Board> list = null;
		
		String query = prop.getProperty("selectListWithPaging");
		
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			System.out.println(startRow); 
			System.out.println(endRow); 
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			//이꺼 오류
			//pstmt.setString(1, rset.getString("MEMBERSHIPNUMBER"));
			
			
			
			System.out.println("dao 도착..");
			
			rset = pstmt.executeQuery();
			
			System.out.println("쿼리문 실행");
			
			list = new ArrayList<Board>();
			
			while(rset.next()) {
				Board b = new Board();
				
				
				b.setqNo(rset.getString("QUESTIONBOARD_NO"));
				b.setqTitle(rset.getString("QUESTIONBOARD_TITLE"));
				b.setqWriteDate(rset.getDate("QUESTIONBOARD_WRITEDATE"));
				b.setqContent(rset.getString("QUESTIONBOARD_CONTENT"));
				b.setqPageView(rset.getInt("QUESTIONBOARD_PAGEVIEW"));
				b.setqReply(rset.getString("QUESTIONBOARD_REPLYORNOTREPLY"));
				b.setMemberShipNumber(rset.getString("MEMBERSHIPNUMBER"));
				//아이디 로그인 되면 가능 쿼리문에도 membershipnumber 기입
				b.setcName(rset.getString("CNAME"));

				list.add(b);
			}
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int updateCount(Connection con, int num) {
		PreparedStatement pstmt = null;
		int result = 0;

		
		String query = prop.getProperty("updateCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			pstmt.setInt(2, num);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}

	public Board selectOne(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Board board = null;
		
		String query = prop.getProperty("selectOne");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				board= new Board();
				
				board.setqNo(rset.getString("QUESTIONBOARD_NO"));
				board.setqTitle(rset.getString("QUESTIONBOARD_TITLE"));
				board.setqWriteDate(rset.getDate("QUESTIONBOARD_WRITEDATE"));
				board.setqContent(rset.getString("QUESTIONBOARD_CONTENT"));
				board.setqPageView(rset.getInt("QUESTIONBOARD_PAGEVIEW"));
				board.setqReply(rset.getString("QUESTIONBOARD_REPLYORNOTREPLY"));
				board.setMemberShipNumber(rset.getString("MEMBERSHIPNUMBER"));
				board.setcName(rset.getString("CNAME"));
				
				System.out.println("dao보드가 : " + board);
				
			}
			
		} catch (SQLException e) {

			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}
		return board;
	}

	public int insertReply(Connection con, Board reply) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertReply");
		
		try {
			pstmt = con.prepareStatement(query);
			/*pstmt.setString(1, reply.getMemberShipNumber());*/
			pstmt.setString(1, reply.getRefBid());
			pstmt.setString(2, reply.getqContent());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {

			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		
		return result;
	}

	public ArrayList<Board> selectReplyList(Connection con, String refBid) {
		PreparedStatement pstmt = null;
	      ResultSet rset = null;
	      ArrayList<Board> list = null;
	      
	      String query = prop.getProperty("selectReplyList");
	      
	      try {
	         pstmt = con.prepareStatement(query);
	         pstmt.setString(1, refBid);
	         
	         rset = pstmt.executeQuery();
	         
	         //while 문이니까 list 밖으로 빼주기
	         list = new ArrayList<Board>();
	         while(rset.next()) {
	            Board b = new Board();
	            
	           
	            b.setqContent(rset.getString("QUESTIONBOARD_CONTENT"));
	            b.setMemberShipNumber(rset.getString("MEMBERSHIPNUMBER"));
	            b.setqWriteDate(rset.getDate("QUESTIONBOARD_WRITEDATE"));
	           
	            list.add(b);
	         }
	         
	      } catch (SQLException e) {
	         e.printStackTrace();
	      } finally {
	         close(rset);
	         close(pstmt);
	      }
	      
	      return list;
	   }


}
