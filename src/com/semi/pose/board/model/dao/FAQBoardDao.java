package com.semi.pose.board.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import static com.semi.pose.common.JDBCTemplate.close;

import com.semi.pose.board.model.vo.Board;
import com.semi.pose.board.model.vo.FAQBoard;
import com.semi.pose.board.model.vo.PageInfo;

public class FAQBoardDao {
	
	private Properties prop = new Properties();
	
	  public FAQBoardDao(){
		  String fileName = FAQBoardDao.class.getResource("/sql/board/board-query(jaemin).properties").getPath();
		  
		  try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		  
	  }
	
	
	
	public int insertBoard(Connection con, FAQBoard newFAQBoard) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertFAQBoard");
		
		try {
			
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, newFAQBoard.getFaqContent());
			pstmt.setString(2, newFAQBoard.getFaqTitle());
			pstmt.setString(3, newFAQBoard.getFaqWriter());
			
			
			result = pstmt.executeUpdate();
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}



	public ArrayList<FAQBoard> selectBoardList(Connection con,PageInfo pi) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<FAQBoard> list = null;
		
		
		String query = prop.getProperty("selectFAQList");
		
		try {
			pstmt = con.prepareStatement(query);
			
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			System.out.println("dao 도착..");
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<FAQBoard>();
			
			while(rset.next()){
				FAQBoard f = new FAQBoard();
				
				f.setFaqNo(rset.getString("FAQ_NO"));
				f.setFaqContent(rset.getString("FAQ_CONTENT"));
				f.setFaqWriteDate(rset.getDate("FAQ_WRITE_DATE"));
				f.setFaqPageviews(rset.getInt("FAQ_PAGEVIEWS"));
				f.setFaqTitle(rset.getString("FAQ_TITLE"));
				f.setFaqWriter(rset.getString("FAQ_WRITER"));
				
				list.add(f);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
	
		
		
		return list;
	}



	public int getListCount(Connection con) {
		Statement stmt = null;
		int listCount = 0;
		ResultSet rset = null;
		
		
		String query = prop.getProperty("listCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
		
			
			
		} catch (SQLException e) {


			e.printStackTrace();
		}finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}



	
}
