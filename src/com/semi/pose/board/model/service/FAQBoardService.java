package com.semi.pose.board.model.service;

import java.sql.Connection;
import java.util.ArrayList;

import com.semi.pose.board.model.dao.BoardDao;
import com.semi.pose.board.model.dao.FAQBoardDao;
import com.semi.pose.board.model.vo.FAQBoard;
import com.semi.pose.board.model.vo.PageInfo;

import static com.semi.pose.common.JDBCTemplate.close;
import static com.semi.pose.common.JDBCTemplate.getConnection;
import static com.semi.pose.common.JDBCTemplate.rollback;
import static com.semi.pose.common.JDBCTemplate.commit;


public class FAQBoardService {

	public int insertFAQBoard(FAQBoard newFAQBoard) {
	
		Connection con = getConnection();
		
		int result = new FAQBoardDao().insertBoard(con,newFAQBoard);
		
		
		if(result>0){
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		
		return result;
	}

	public ArrayList<FAQBoard> selectFAQBoardList(PageInfo pi) {
	
		Connection con = getConnection();
		
		ArrayList<FAQBoard> list = new FAQBoardDao().selectBoardList(con,pi);
		
		close(con);
		
	
		return list;
	}

	public int getListCount() {
		
		Connection con = getConnection();
		
		int listCount = new FAQBoardDao().getListCount(con);
		
		close(con);
		
		return listCount;
	}

	

	

	
}
