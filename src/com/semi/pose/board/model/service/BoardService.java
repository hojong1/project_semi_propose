package com.semi.pose.board.model.service;


import static com.semi.pose.common.JDBCTemplate.close;
import static com.semi.pose.common.JDBCTemplate.getConnection;
import static com.semi.pose.common.JDBCTemplate.rollback;
import static com.semi.pose.common.JDBCTemplate.commit;

import java.sql.Connection;
import java.util.ArrayList;

import com.semi.pose.board.model.dao.BoardDao;
import com.semi.pose.board.model.vo.Board;
import com.semi.pose.board.model.vo.PageInfo;

public class BoardService {

	public int insertBoard(Board newBoard) {
		Connection con = getConnection();
			
		int result = new BoardDao().insertBoard(con, newBoard);
		
		if(result > 0) {
			commit(con);
			
			
		} else {
			rollback(con);
			
			
		}
		
		close(con);
		
		return result;
	}

	public int getListCount() {
		Connection con = getConnection();
		
		int listCount = new BoardDao().getListCount(con);
		
		close(con);
		
		
		return listCount;
	}

	public ArrayList<Board> selectListWithPaging(PageInfo pi) {
		Connection con = getConnection();
		
		System.out.println("dao로 전달");
		ArrayList<Board> list = new BoardDao().selectListWithPaging(con,pi);

		close(con);
		
		return list;
	}

	public Board selectOne(int num) {
		Connection con = getConnection();
		
		BoardDao bd = new BoardDao();
		int result = bd.updateCount(con, num);
		
		Board board = null;
		if(result > 0) {
			board = bd.selectOne(con, num);
			
			if(board != null) {
				commit(con);
				
			} else {
				rollback(con);
			}
		} else {
			rollback(con);
		}
		
		close(con);
		
		return board;
	}

	public ArrayList<Board> insertReply(Board reply) {
		Connection con = getConnection();
	      
	      BoardDao bd = new BoardDao();
	      
	      int result = bd.insertReply(con, reply);
	      System.out.println("인서트리플라이서비스 리절트:" + result);
	      ArrayList<Board> replyList = null;
	      if(result > 0) {
	         replyList = bd.selectReplyList(con, reply.getRefBid());
	         System.out.println("리플라이리스트" + replyList);
	         if(replyList != null) {
	            commit(con);
	         } else {
	            rollback(con);
	         }
	      } else {
	         rollback(con);
	      }
	      
	      close(con);
	      
	      return replyList;
	   }
}
