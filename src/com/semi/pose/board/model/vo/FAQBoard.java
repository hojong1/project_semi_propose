package com.semi.pose.board.model.vo;

import java.util.Date;

public class FAQBoard {
	
	private String faqNo;
	private String faqContent;
	private String faqAnswer;
	private Date faqWriteDate;
	private int faqPageviews;
	private String faqTitle;
	private String faqWriter;
	private String btTypeNo;
	
		
	public FAQBoard(){}


	public FAQBoard(String faqNo, String faqContent, String faqAnswer, Date faqWriteDate, int faqPageviews,
			String faqTitle, String faqWriter, String btTypeNo) {
		super();
		this.faqNo = faqNo;
		this.faqContent = faqContent;
		this.faqAnswer = faqAnswer;
		this.faqWriteDate = faqWriteDate;
		this.faqPageviews = faqPageviews;
		this.faqTitle = faqTitle;
		this.faqWriter = faqWriter;
		this.btTypeNo = btTypeNo;
	}


	public String getFaqNo() {
		return faqNo;
	}


	public void setFaqNo(String faqNo) {
		this.faqNo = faqNo;
	}


	public String getFaqContent() {
		return faqContent;
	}


	public void setFaqContent(String faqContent) {
		this.faqContent = faqContent;
	}


	public String getFaqAnswer() {
		return faqAnswer;
	}


	public void setFaqAnswer(String faqAnswer) {
		this.faqAnswer = faqAnswer;
	}


	public Date getFaqWriteDate() {
		return faqWriteDate;
	}


	public void setFaqWriteDate(Date faqWriteDate) {
		this.faqWriteDate = faqWriteDate;
	}


	public int getFaqPageviews() {
		return faqPageviews;
	}


	public void setFaqPageviews(int faqPageviews) {
		this.faqPageviews = faqPageviews;
	}


	public String getFaqTitle() {
		return faqTitle;
	}


	public void setFaqTitle(String faqTitle) {
		this.faqTitle = faqTitle;
	}


	public String getFaqWriter() {
		return faqWriter;
	}


	public void setFaqWriter(String faqWriter) {
		this.faqWriter = faqWriter;
	}


	public String getBtTypeNo() {
		return btTypeNo;
	}


	public void setBtTypeNo(String btTypeNo) {
		this.btTypeNo = btTypeNo;
	}


	@Override
	public String toString() {
		return "FAQBoard [faqNo=" + faqNo + ", faqContent=" + faqContent + ", faqAnswer=" + faqAnswer
				+ ", faqWriteDate=" + faqWriteDate + ", faqPageviews=" + faqPageviews + ", faqTitle=" + faqTitle
				+ ", faqWriter=" + faqWriter + ", btTypeNo=" + btTypeNo + "]";
	}
	
	
	
	
	
	
}
