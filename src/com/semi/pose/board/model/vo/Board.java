package com.semi.pose.board.model.vo;

import java.sql.Date;

public class Board implements java.io.Serializable{
	private String qNo;
	private String qTitle;
	private Date qWriteDate;
	private String qContent;
	private int qPageView;
	private String qReply;
	private String memberShipNumber;
	private int qCategory;
	private String cName;
	private int bType;
	private String refBid;
	private String refContent;
	
	
	public Board() {}


	public Board(String qNo, String qTitle, Date qWriteDate, String qContent, int qPageView, String qReply,
			String memberShipNumber, int qCategory, String cName, int bType, String refBid, String refContent) {
		super();
		this.qNo = qNo;
		this.qTitle = qTitle;
		this.qWriteDate = qWriteDate;
		this.qContent = qContent;
		this.qPageView = qPageView;
		this.qReply = qReply;
		this.memberShipNumber = memberShipNumber;
		this.qCategory = qCategory;
		this.cName = cName;
		this.bType = bType;
		this.refBid = refBid;
		this.refContent = refContent;
	}


	public String getqNo() {
		return qNo;
	}


	public void setqNo(String qNo) {
		this.qNo = qNo;
	}


	public String getqTitle() {
		return qTitle;
	}


	public void setqTitle(String qTitle) {
		this.qTitle = qTitle;
	}


	public Date getqWriteDate() {
		return qWriteDate;
	}


	public void setqWriteDate(Date qWriteDate) {
		this.qWriteDate = qWriteDate;
	}


	public String getqContent() {
		return qContent;
	}


	public void setqContent(String qContent) {
		this.qContent = qContent;
	}


	public int getqPageView() {
		return qPageView;
	}


	public void setqPageView(int qPageView) {
		this.qPageView = qPageView;
	}


	public String getqReply() {
		return qReply;
	}


	public void setqReply(String qReply) {
		this.qReply = qReply;
	}


	public String getMemberShipNumber() {
		return memberShipNumber;
	}


	public void setMemberShipNumber(String memberShipNumber) {
		this.memberShipNumber = memberShipNumber;
	}


	public int getqCategory() {
		return qCategory;
	}


	public void setqCategory(int qCategory) {
		this.qCategory = qCategory;
	}


	public String getcName() {
		return cName;
	}


	public void setcName(String cName) {
		this.cName = cName;
	}


	public int getbType() {
		return bType;
	}


	public void setbType(int bType) {
		this.bType = bType;
	}


	public String getRefBid() {
		return refBid;
	}


	public void setRefBid(String refBid) {
		this.refBid = refBid;
	}


	public String getRefContent() {
		return refContent;
	}


	public void setRefContent(String refContent) {
		this.refContent = refContent;
	}


	@Override
	public String toString() {
		return "Board [qNo=" + qNo + ", qTitle=" + qTitle + ", qWriteDate=" + qWriteDate + ", qContent=" + qContent
				+ ", qPageView=" + qPageView + ", qReply=" + qReply + ", memberShipNumber=" + memberShipNumber
				+ ", qCategory=" + qCategory + ", cName=" + cName + ", bType=" + bType + ", refBid=" + refBid
				+ ", refContent=" + refContent + "]";
	}

	

}
