package com.semi.pose.board.gallery.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.oreilly.servlet.MultipartRequest;
import com.semi.pose.board.gallery.model.service.EventService;
import com.semi.pose.board.gallery.model.vo.BoardGallery;
import com.semi.pose.common.MyFileRenamePolicy;

/**
 * Servlet implementation class EventInsertServlet
 */
@WebServlet("/insertEvent.tn")
public class EventInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EventInsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(ServletFileUpload.isMultipartContent(request)) {
			int maxSize = 1024 * 1024 * 30; //사진 최대 크기는 30MB
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			System.out.println(root);
			
			String filePath = root + "resources/eventThumbnail_uploadFiles/";
			
			MultipartRequest multiRequest = 
					new MultipartRequest(request, filePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			//저장할 파일 이름 보관
			ArrayList<String> saveFiles = new ArrayList<>();
			//원래 파일 이름 보관
			ArrayList<String> originFiles = new ArrayList<>();
			
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				
				System.out.println("name : " + name);
				
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			System.out.println("fileSystem name : " + saveFiles);
			System.out.println("originFile name : " + originFiles);
			
			String multiMainCategory = multiRequest.getParameter("mainCategory");
			String multiSubdivision = multiRequest.getParameter("subdivision");
			String multiProductName = multiRequest.getParameter("productName");
			int multiPrice = Integer.parseInt(multiRequest.getParameter("price"));
			String multiRadioAnswser = multiRequest.getParameter("radio-answer");
			
			System.out.println(multiRadioAnswser);
			
			//vo에 담아서 위에 파라미터 전달
			
				//잘들어갔는지 확인
				System.out.println();
			
			//한번에 전송하기 위해 하나의 파일당 하나의 vo 이용
			ArrayList<BoardGallery> fileList = new ArrayList<BoardGallery>();				//잘들어갔는지 확인
			for(int i = originFiles.size() - 1; i >= 0 ; i--) {
				BoardGallery at = new BoardGallery();
				at.setFilePath(filePath);
				at.setOriginName(originFiles.get(i));
				at.setChangeName(saveFiles.get(i));
				
				//fileLevel: 0 - 타이틀 사진, fileLevel : 1 - 나머지
				if(i == originFiles.size() - 1) {
					at.setFileLevel(0);
				} else {
					at.setFileLevel(1);
				}
				
				fileList.add(at);
			}
			
				//잘들어갔는지 확인
				System.out.println();
			
			//위의 두 vo를 다 넣기 위해 Map 이용
			Map<String, Object> requestData = new HashMap<String, Object>();
			
				//잘들어갔는지 확인
				System.out.println(requestData);
			
			//service, dao와 연결되는 부분
			int result = new EventService().insertEvent();
			
			String path = "";
			if(result > 0) {
				path = "views/common/successPage.jsp";
				response.sendRedirect(path);
 			} else {
 				//실패시 저장된 사진 삭제
				for(int i = 0; i < saveFiles.size(); i++) {
					File faildFile = new File(filePath + saveFiles.get(i));
					
					faildFile.delete();
				}
				
				request.setAttribute("message", "업체 이벤트 등록 실패!!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);

 			}
			
			/*
			int bWriter = ((Member) request.getSession().getAttribute("loginUser")).getUno();
			
			Board board = new Board();
			board.setbTitle(multilTitle);
			board.setbContent(multiContent);
			board.setbWriter(bWriter);
			
			//한번에 전송하기위해 하나의 파일당 하나의 Attachment객체사용하기 때문에 arrayList 사용
			ArrayList<Attachment> fileList = new ArrayList<Attachment>();
			for(int i = originFiles.size() - 1; i >= 0 ; i--) {
				Attachment at = new Attachment();
				at.setFilePath(filePath);
				at.setOriginName(originFiles.get(i));
				at.setChangeName(saveFiles.get(i));
				
				//타이틀 사진일 경우 fileLevel을 0으로 설정해서 대표사진이란걸알리고
				//나머지는 1로 둠
				if(i == originFiles.size() - 1) {
					at.setFileLevel(0);
				} else {
					at.setFileLevel(1);
				}
				
				fileList.add(at);
			}
			
			System.out.println("upload fileList : " + fileList);
			
			Map<String, Object> requestData = new HashMap<String, Object>();
			requestData.put("board", board);
			requestData.put("fileList", fileList);
			
			System.out.println("requestData : " + requestData);
			
		}*/
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
