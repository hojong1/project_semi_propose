package com.semi.pose.board.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.semi.pose.board.model.service.BoardService;
import com.semi.pose.board.model.vo.Board;


/**
 * Servlet implementation class ReplyInsertServlet
 */
@WebServlet("/insertReply.bo")
public class ReplyInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReplyInsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String writer = request.getParameter("writer");
		String bid = request.getParameter("bid");
		String content = request.getParameter("content");
//		int userId = Integer.parseInt(request.getParameter("loginUser"));
		/*int bid = Integer.parseInt(request.getParameter("bid"));*/
		
		System.out.println("writer :"+writer);
		System.out.println("content :"+content);
		System.out.println("bid :"+bid);
		
		Board reply = new Board();
		reply.setMemberShipNumber(writer);
		reply.setqContent(content);
		reply.setRefBid(bid);
		
		ArrayList<Board> replyList = new BoardService().insertReply(reply);
		
		System.out.println(replyList);
		response.setContentType("application/json; charset=UTF-8");
		new Gson().toJson(replyList, response.getWriter());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
