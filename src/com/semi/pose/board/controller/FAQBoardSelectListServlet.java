package com.semi.pose.board.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.semi.pose.board.model.service.FAQBoardService;
import com.semi.pose.board.model.vo.FAQBoard;
import com.semi.pose.board.model.vo.PageInfo;

/**
 * Servlet implementation class FAQBoardSelectListServlet
 */
@WebServlet("/selectFAQList.bo")
public class FAQBoardSelectListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FAQBoardSelectListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int currentPage;		//현재 페이지를 표시할 변수
		int limit;				//한 페이지에 게시글이 몇 개가 보여질 것인지 표시
		int maxPage;			//전체 페이지에서 가장 마지막 페이지
		int startPage;			//한 번에 표시될 페이지가 시작할 페이지
		int endPage;			//한 번에 표시될 페이지가 끝나는 페이지
		
		
		currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		limit = 10;
		
		int listCount = new FAQBoardService().getListCount();
		
		 System.out.println("전체 게시물 목록 갯수 : " + listCount);
		 
		 maxPage = (int) ((double) listCount / limit + 0.9);
		
		//시작 페이지 수 계산
		//1, 11, 21, 31, ...
		startPage = (((int)((double) currentPage / limit + 0.9)) - 1) * 10 + 1;
		endPage = startPage + 10 - 1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		//페이징에 필요한 정보들을 담을 vo
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		
	    ArrayList<FAQBoard> faqlist = new FAQBoardService().selectFAQBoardList(pi);
		
	    
	    
	    System.out.println("faqboard List"+faqlist);
	   
	    
	    
	    String path="";
	    if(faqlist!=null){
	    	path="views/board/FrequentlyAskedQuestionsBoard.jsp";
	    	request.setAttribute("list", faqlist);
	    	request.setAttribute("pi", pi);
	    	
	    }else {
	    	path="views/common/errorPage.jsp";
	    	request.setAttribute("msg","자주묻는게시판 조회 실패!");
	    	
	    	
	    }
	    
	    request.getRequestDispatcher(path).forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
