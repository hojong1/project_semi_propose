package com.semi.pose.board.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.semi.pose.board.model.service.BoardService;
import com.semi.pose.board.model.vo.Board;
import com.semi.pose.member.model.vo.IndividualMember;

/**
 * Servlet implementation class BoardInsertServlet
 */
@WebServlet("/insert.bo")
public class BoardInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BoardInsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		int category = Integer.parseInt(request.getParameter("category"));
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		
		HttpSession session = request.getSession();
		IndividualMember loginUser = (IndividualMember) session.getAttribute("loginUser");
		
		String writer = loginUser.getMembershipNumber();
		
		Board newBoard = new Board();
		newBoard.setqCategory(category);
		newBoard.setqTitle(title);
		newBoard.setqContent(content);
		newBoard.setMemberShipNumber(writer);
		
		System.out.println("newBoard의 값~!"+newBoard);
		

		int result = new BoardService().insertBoard(newBoard);
		
		String path= "";
		if(result > 0 ) {
			System.out.println("게시글 등록 성공");
			
			
			response.sendRedirect(request.getContextPath()+"/selectList.bo");
			
		}else {
			path="views/common/errorPage.jsp";
			
			request.setAttribute("message", "게시판 작성 시르패~!");
			request.getRequestDispatcher(path).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
