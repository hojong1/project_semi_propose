package com.semi.pose.board.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.semi.pose.board.model.service.FAQBoardService;
import com.semi.pose.board.model.vo.FAQBoard;

/**
 * Servlet implementation class FAQBoardInsertServlet
 */
@WebServlet("/faqInsert.bo")
public class FAQBoardInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FAQBoardInsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		String faqTitle = request.getParameter("title");
		String faqWriter = request.getParameter("adminId");
		String faqContent = request.getParameter("content");
		
		
		
		FAQBoard newFAQBoard = new FAQBoard();
		
		newFAQBoard.setFaqTitle(faqTitle);
		newFAQBoard.setFaqWriter(faqWriter);
		newFAQBoard.setFaqContent(faqContent);
		
		
		
		int result = new FAQBoardService().insertFAQBoard(newFAQBoard);
		
		String path="";
		
		if(result>0){
			System.out.println("입력 성공");
			path="selectFAQList.bo";
			response.sendRedirect(request.getContextPath()+"/selectFAQList.bo");
			
		}else {
			path="views/common/errorPage.jsp";
			request.setAttribute("message", "자주묻는게시판 등록 실패");
			request.getRequestDispatcher(path).forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
