package com.semi.pose.board.corporate.controller;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.semi.pose.board.corporate.model.vo.Eventinsert;
import com.semi.pose.common.MyFileRenamePolicy;
import com.oreilly.servlet.MultipartRequest;







/**
 * Servlet implementation class EventinsertServlet
 */
@WebServlet("/insertevent")
public class EventinsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public EventinsertServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(ServletFileUpload.isMultipartContent(request)) {
		
			int maxSize = 1024 * 1024 * 10;
		
			String root = request.getSession().getServletContext().getRealPath("/");
			
			System.out.println(root);
			
			String filePath = root + "resources/image/sunjung/";

			MultipartRequest multiRequest =
					new MultipartRequest(request, filePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			ArrayList<String> saveFiles = new ArrayList<>();
			
			ArrayList<String> originFiles = new ArrayList<>();
			
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				
				System.out.println("name : " + name);
				
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			
			System.out.println("fileSystem name : " + saveFiles);
			System.out.println("originFile name : " + originFiles);
		
			String multiTitle = multiRequest.getParameter("title");
			String multiContent = multiRequest.getParameter("content");
			
			System.out.println("title : " + multiTitle);
			System.out.println("content : " + multiContent);
			

			/*int bWriter = ((Member) request.getSession().getAttribute("loginUser")).getUno();
			
			Board board = new Board();
			board.setbTitle(multiTitle);
			board.setbContent(multiContent);
			board.setbWriter(bWriter);
			
			ArrayList<Eventinsert> fileList = new ArrayList<Eventinsert>();
			for(int i = originFiles.size() - 1; i >= 0; i--) {
				Eventinsert et = new Eventinsert();
				et.setFilePath(filePath);
				et.setOriginName(originFiles.get(i));
				et.setChangeName(saveFiles.get(i));
				
				if(i == originFiles.size() - 1) {
					et.setFileLevel(0);
				} else {
					et.setFileLevel(1);
				}
				
				fileList.add(et);
			}
		
			System.out.println("upload fileList : " + fileList);
			
			Map<String, Object> requestData = new HashMap<String, Object>();
			requestData.put("board", board);
			requestData.put("fileList", fileList);
			
			System.out.println("requestData : " + requestData);
			
			int result = new BoardService().insertThumbnail(requestData);
			
			if(result > 0) {
				response.sendRedirect(request.getContextPath() + "/selectList.tn");
			} else {
				//실패시 저장된 사진 삭제
				for(int i = 0; i < saveFiles.size(); i++) {
					File faildFile = new File(filePath + saveFiles.get(i));
					
					faildFile.delete();
				}
				
				request.setAttribute("message", "사진 게시판 등록 실패!!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}
			*/
		
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
