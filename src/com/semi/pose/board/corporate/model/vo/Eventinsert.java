package com.semi.pose.board.corporate.model.vo;

public class Eventinsert implements java.io.Serializable {
	private String evtCode;
	private String evtTitle;
	private String process;
	private String price;
	private String premium;
	private String evtDate;
	private String content;
	private String mbrNumber;
	
	public Eventinsert() {}

	public Eventinsert(String evtCode, String evtTitle, String process, String price, String premium, String evtDate,
			String content, String mbrNumber) {
		super();
		this.evtCode = evtCode;
		this.evtTitle = evtTitle;
		this.process = process;
		this.price = price;
		this.premium = premium;
		this.evtDate = evtDate;
		this.content = content;
		this.mbrNumber = mbrNumber;
	}

	public String getEvtCode() {
		return evtCode;
	}

	public void setEvtCode(String evtCode) {
		this.evtCode = evtCode;
	}

	public String getEvtTitle() {
		return evtTitle;
	}

	public void setEvtTitle(String evtTitle) {
		this.evtTitle = evtTitle;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getEvtDate() {
		return evtDate;
	}

	public void setEvtDate(String evtDate) {
		this.evtDate = evtDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMbrNumber() {
		return mbrNumber;
	}

	public void setMbrNumber(String mbrNumber) {
		this.mbrNumber = mbrNumber;
	}

	@Override
	public String toString() {
		return "Eventinsert [evtCode=" + evtCode + ", evtTitle=" + evtTitle + ", process=" + process + ", price="
				+ price + ", premium=" + premium + ", evtDate=" + evtDate + ", content=" + content + ", mbrNumber="
				+ mbrNumber + "]";
	}
	
	
	
}
