package com.semi.pose.notice.controller;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.semi.pose.notice.model.service.NoticeService;
import com.semi.pose.notice.model.vo.Notice;



/**
 * Servlet implementation class NoticeInsertServlet
 */
@WebServlet("/insert.no")
public class NoticeInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NoticeInsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String title = request.getParameter("title");
		String writer = request.getParameter("writer");
		String uno = request.getParameter("uno");
		String date = request.getParameter("date");
		String content = request.getParameter("content");
		
		
		java.sql.Date day=null;
		
		if(date != "") {

			day=java.sql.Date.valueOf(date); //이렇게하는게 위에보다 쉽다
			
		}else {
			day = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		
		Notice newNotice = new Notice();
		newNotice.setnTitle(title);
		newNotice.setnWriter(uno);
		newNotice.setnContent(content);
		newNotice.setnWriteDate(day);

		
		System.out.println("new Notice 는 : " + newNotice);
		
		int result = new NoticeService().insertNotice(newNotice);
		
		String path = "";
		if(result > 0) {
			path="selectList.no";
			response.sendRedirect(path);
			
		}else {
			path = "views/common/errorPage.jsp";
			request.setAttribute("message", "공지사항 등록 쉬르패");
			request.getRequestDispatcher(path).forward(request, response);
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
