package com.semi.pose.notice.model.vo;

import java.sql.Date;

public class Notice {
	private String nNo;
	private String nTitle;
	private String nWriter;
	private int nPageViews;
	private Date nWriteDate;
	private String nContent;
	private String memberShipNumber;
	private int bTypeNo;
	
	public Notice() {}

	public Notice(String nNo, String nTitle, String nWriter, int nPageViews, Date nWriteDate, String nContent,
			String memberShipNumber, int bTypeNo) {
		super();
		this.nNo = nNo;
		this.nTitle = nTitle;
		this.nWriter = nWriter;
		this.nPageViews = nPageViews;
		this.nWriteDate = nWriteDate;
		this.nContent = nContent;
		this.memberShipNumber = memberShipNumber;
		this.bTypeNo = bTypeNo;
	}

	public String getnNo() {
		return nNo;
	}

	public void setnNo(String nNo) {
		this.nNo = nNo;
	}

	public String getnTitle() {
		return nTitle;
	}

	public void setnTitle(String nTitle) {
		this.nTitle = nTitle;
	}

	public String getnWriter() {
		return nWriter;
	}

	public void setnWriter(String nWriter) {
		this.nWriter = nWriter;
	}

	public int getnPageViews() {
		return nPageViews;
	}

	public void setnPageViews(int nPageViews) {
		this.nPageViews = nPageViews;
	}

	public Date getnWriteDate() {
		return nWriteDate;
	}

	public void setnWriteDate(Date nWriteDate) {
		this.nWriteDate = nWriteDate;
	}

	public String getnContent() {
		return nContent;
	}

	public void setnContent(String nContent) {
		this.nContent = nContent;
	}

	public String getMemberShipNumber() {
		return memberShipNumber;
	}

	public void setMemberShipNumber(String memberShipNumber) {
		this.memberShipNumber = memberShipNumber;
	}

	public int getbTypeNo() {
		return bTypeNo;
	}

	public void setbTypeNo(int bTypeNo) {
		this.bTypeNo = bTypeNo;
	}

	@Override
	public String toString() {
		return "Notice [nNo=" + nNo + ", nTitle=" + nTitle + ", nWriter=" + nWriter + ", nPageViews=" + nPageViews
				+ ", nWriteDate=" + nWriteDate + ", nContent=" + nContent + ", memberShipNumber=" + memberShipNumber
				+ ", bTypeNo=" + bTypeNo + "]";
	}

}

