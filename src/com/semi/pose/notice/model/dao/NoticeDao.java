package com.semi.pose.notice.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.semi.pose.notice.model.vo.Notice;

import static com.semi.pose.common.JDBCTemplate.close;

public class NoticeDao {
	private Properties prop = new Properties();
	
	public NoticeDao() {
		String fileName = NoticeDao.class.getResource("/sql/notice/notice-query(baekhojong).properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public int insertNotice(Connection con, Notice newNotice) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertNotice");
		
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setString(1, newNotice.getnTitle());
			pstmt.setString(2, newNotice.getnContent());
			/*pstmt.setString(3, newNotice.getnWriter());*/
			/*pstmt.setDate(3, newNotice.getnWriteDate());*/
			
			result = pstmt.executeUpdate();
		
		} catch (SQLException e) {

			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<Notice> selectNoticeList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<Notice> list = null;
		
		String query = prop.getProperty("selectList");
		
		try {
			stmt = con.createStatement();
			rset =stmt.executeQuery(query);
			
			list = new ArrayList<Notice>();
			
			while(rset.next()) {
				Notice n = new Notice();
				n.setnNo(rset.getString("NOTICE_NO"));
				n.setnTitle(rset.getString("NOTICE_TITLE"));
				/*n.setnWriter(rset.getString("NOTICE_WRITER"));*/
				n.setnWriteDate(rset.getDate("WRITE_DATE"));
				n.setnContent(rset.getString("NOTICE_CONTENT"));
				/*n.setMemberShipNumber(rset.getString("MEMBERSHIPNUMBER"));*/

				list.add(n);
			}
			
		} catch (SQLException e) {

			e.printStackTrace();
		}finally {
			close(stmt);
			close(rset);
		}
		
		return list;
	}

	public int updateCount(Connection con, String num) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Notice selectOne(Connection con, String num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Notice notice = null;
		
		String query = prop.getProperty("selectOne");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, num);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				notice = new Notice();
				
				notice.setnNo(rset.getString("NOTICE_NO"));
				notice.setnTitle(rset.getString("NOTICE_TITLE"));
				notice.setnWriter(rset.getString("NOTICE_WRITER"));
				notice.setnPageViews(rset.getInt("NOTICE_PAGEVIEWS"));
				notice.setnWriteDate(rset.getDate("WRITE_DATE"));
				notice.setnContent(rset.getString("NOTICE_CONTENT"));
				notice.setMemberShipNumber(rset.getString("MEMBERSHIPNUMBER"));
				notice.setbTypeNo(rset.getInt("BT_TYPENO"));
			}
			
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(rset);
				close(pstmt);
			}
			
			return notice;

	}
}





