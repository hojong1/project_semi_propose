package com.semi.pose.notice.model.service;

import java.sql.Connection;
import java.util.ArrayList;

import com.semi.pose.notice.model.dao.NoticeDao;
import com.semi.pose.notice.model.vo.Notice;

import static com.semi.pose.common.JDBCTemplate.getConnection;
import static com.semi.pose.common.JDBCTemplate.close;
import static com.semi.pose.common.JDBCTemplate.commit;
import static com.semi.pose.common.JDBCTemplate.rollback;

public class NoticeService {

	public int insertNotice(Notice newNotice) {
		Connection con = getConnection();
		
		int result = new NoticeDao().insertNotice(con,newNotice);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public ArrayList<Notice> selectNoticeList() {
		Connection con = getConnection();
		
		ArrayList<Notice> list= new NoticeDao().selectNoticeList(con);
		
		close(con);
		
		return list;
	}

	public Notice selectOneNoticeByNno(String num) {
	Connection con = getConnection();
		
		NoticeDao nd = new NoticeDao();

		Notice notice = null;
		
		int result = nd.updateCount(con, num);
		if(result > 0) {
			notice = nd.selectOne(con, num);
			if(notice != null) {
				commit(con);
			} else {
				rollback(con);
			}
		} else {
			rollback(con);
		}
		
		close(con);
		
		return notice;
	}


}
