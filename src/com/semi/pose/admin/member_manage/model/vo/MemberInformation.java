package com.semi.pose.admin.member_manage.model.vo;

public class MemberInformation {
	
	private String membershipNumber;
	private String userId;
	private String userName;
	private String purchaseAmount;
	private String numberReviews;
	private String orderProgress;
	private String phone;
	private String email;
	private String joinedDay;
	private String lastconnection;
	
	
	public MemberInformation() {}


	public MemberInformation(String membershipNumber, String userId, String userName, String purchaseAmount,
			String numberReviews, String orderProgress, String phone, String email, String joinedDay,
			String lastconnection) {
		super();
		this.membershipNumber = membershipNumber;
		this.userId = userId;
		this.userName = userName;
		this.purchaseAmount = purchaseAmount;
		this.numberReviews = numberReviews;
		this.orderProgress = orderProgress;
		this.phone = phone;
		this.email = email;
		this.joinedDay = joinedDay;
		this.lastconnection = lastconnection;
	}


	public String getMembershipNumber() {
		return membershipNumber;
	}


	public void setMembershipNumber(String membershipNumber) {
		this.membershipNumber = membershipNumber;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPurchaseAmount() {
		return purchaseAmount;
	}


	public void setPurchaseAmount(String purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}


	public String getNumberReviews() {
		return numberReviews;
	}


	public void setNumberReviews(String numberReviews) {
		this.numberReviews = numberReviews;
	}


	public String getOrderProgress() {
		return orderProgress;
	}


	public void setOrderProgress(String orderProgress) {
		this.orderProgress = orderProgress;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getJoinedDay() {
		return joinedDay;
	}


	public void setJoinedDay(String joinedDay) {
		this.joinedDay = joinedDay;
	}


	public String getLastconnection() {
		return lastconnection;
	}


	public void setLastconnection(String lastconnection) {
		this.lastconnection = lastconnection;
	}


	@Override
	public String toString() {
		return "MemberInformation [membershipNumber=" + membershipNumber + ", userId=" + userId + ", userName="
				+ userName + ", purchaseAmount=" + purchaseAmount + ", numberReviews=" + numberReviews
				+ ", orderProgress=" + orderProgress + ", phone=" + phone + ", email=" + email + ", joinedDay="
				+ joinedDay + ", lastconnection=" + lastconnection + "]";
	}
	
	
	
	
	
}
