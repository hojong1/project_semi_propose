package com.semi.pose.admin.member_manage.model.vo;

public class ReservationInformation {

	private String reservationNumber;
	private String userName;
	private String phone;
	private String price;
	private String reservationDate;
	private String eventCode;
	private String id;
	private String paymentMethod;
	
	public ReservationInformation(){}

	public ReservationInformation(String reservationNumber, String userName, String phone, String price,
			String reservationDate, String eventCode, String id, String paymentMethod) {
		super();
		this.reservationNumber = reservationNumber;
		this.userName = userName;
		this.phone = phone;
		this.price = price;
		this.reservationDate = reservationDate;
		this.eventCode = eventCode;
		this.id = id;
		this.paymentMethod = paymentMethod;
	}

	public String getReservationNumber() {
		return reservationNumber;
	}

	public void setReservationNumber(String reservationNumber) {
		this.reservationNumber = reservationNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(String reservationDate) {
		this.reservationDate = reservationDate;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	@Override
	public String toString() {
		return "ReservationInformation [reservationNumber=" + reservationNumber + ", userName=" + userName + ", phone="
				+ phone + ", price=" + price + ", reservationDate=" + reservationDate + ", eventCode=" + eventCode
				+ ", id=" + id + ", paymentMethod=" + paymentMethod + "]";
	}

	
	
	
}
