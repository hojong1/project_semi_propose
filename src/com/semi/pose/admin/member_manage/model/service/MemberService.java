package com.semi.pose.admin.member_manage.model.service;

import static com.semi.pose.common.JDBCTemplate.close;
import static com.semi.pose.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;

import com.semi.pose.admin.member_manage.model.dao.MemberDao;
import com.semi.pose.admin.member_manage.model.vo.PageInfo;
import com.semi.pose.member.model.vo.IndividualMember;


public class MemberService {

	public int getListCount() {
		Connection con = getConnection();
		
		int listCount = new MemberDao().getListCount(con);
		
		close(con);
		
		
		return listCount;

	}

	public ArrayList<IndividualMember> selectListWithPaging(PageInfo pi) {
		Connection con = getConnection();
		
		System.out.println("dao로 전달");
		ArrayList<IndividualMember> list = new MemberDao().selectListWithPaging(con,pi);

		close(con);
		
		return list;

	}

}
