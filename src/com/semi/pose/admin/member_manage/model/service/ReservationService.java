package com.semi.pose.admin.member_manage.model.service;

import static com.semi.pose.common.JDBCTemplate.close;
import static com.semi.pose.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;

import com.semi.pose.admin.member_manage.model.dao.MemberDao;
import com.semi.pose.admin.member_manage.model.dao.ReservationDao;
import com.semi.pose.admin.member_manage.model.vo.PageInfo;
import com.semi.pose.admin.member_manage.model.vo.ReservationInformation;
import com.semi.pose.member.model.vo.IndividualMember;


public class ReservationService {

	public int getListCount() {
	
		Connection con = getConnection();
		
		int listCount = new ReservationDao().getListCount(con);
		
		close(con);
		
		return listCount;
	}

	public ArrayList<ReservationInformation> selectListWithPaging(PageInfo pi) {
		
		Connection con = getConnection();
		
		System.out.println("dao로 전달");
		
		ArrayList<ReservationInformation> list = new ReservationDao().selectListWithPaging(con,pi);

		close(con);
		
		return list;
	}


	
	
	
}
