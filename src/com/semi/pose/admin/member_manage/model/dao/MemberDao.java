package com.semi.pose.admin.member_manage.model.dao;

import static com.semi.pose.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.semi.pose.admin.member_manage.model.vo.PageInfo;
import com.semi.pose.member.model.vo.IndividualMember;


public class MemberDao {
	
	private Properties prop = new Properties();
	
	public MemberDao() {
		String fileName = MemberDao.class.getResource("/sql/member/member-query(seonjung).properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
	
	public int getListCount(Connection con) {
		Statement stmt = null;
		int listCount = 0;
		ResultSet rset = null;
		
		String query = prop.getProperty("listCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
		
			
			
		} catch (SQLException e) {


			e.printStackTrace();
		}finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;

	}

	public ArrayList<IndividualMember> selectListWithPaging(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<IndividualMember> list = null;
		
		String query = prop.getProperty("selectListWithPaging");
		
		

		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			//pstmt.setInt(1, startRow);
			//pstmt.setInt(2, endRow);
			
			
			System.out.println("dao 도착..");
			
			rset = pstmt.executeQuery();
			
			System.out.println("쿼리문 실행");
			
			list = new ArrayList<IndividualMember>();
			
			while(rset.next()) {
				IndividualMember m = new IndividualMember();
				
				m.setMembershipNumber(rset.getString("MEMBERSHIPNUMBER"));
				m.setUserId(rset.getString("ID"));
				m.setPwd(rset.getString("PWD"));
				m.setEmail(rset.getString("EMAIL"));
				m.setPhone(rset.getString("PHONE"));
				m.setuserName(rset.getString("USERNAME"));
				
				
				
				list.add(m);
				
			}
			
			System.out.println("쿼리문 종료 ");
			
	} catch (SQLException e) {
		
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
		return list;
	}
	
	
}








