package com.semi.pose.admin.member_manage.model.dao;

import static com.semi.pose.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.semi.pose.admin.member_manage.model.vo.PageInfo;
import com.semi.pose.admin.member_manage.model.vo.ReservationInformation;


public class ReservationDao {

	private Properties prop = new Properties();
	
	public ReservationDao(){
		
		String fileName = MemberDao.class.getResource("/sql/board/board-query(jaemin).properties").getPath();
		
		
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		
	}
	
	
	
	
	public ArrayList<ReservationInformation> selectListWithPaging(Connection con, PageInfo pi) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<ReservationInformation> list = null;
		
		String query = prop.getProperty("selectListWithPaging");
		
		

		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			//pstmt.setInt(1, startRow);
			//pstmt.setInt(2, endRow);
			
			
			System.out.println("dao 도착..");
			
			rset = pstmt.executeQuery();
			
			System.out.println("쿼리문 실행");
			
			list = new ArrayList<ReservationInformation>();
			
			while(rset.next()) {
				ReservationInformation r = new ReservationInformation();
				
				r.setReservationNumber(rset.getString("RESERVATIONNUMBER"));
				r.setUserName(rset.getString("USERNAME"));
				r.setPhone(rset.getString("PHONE"));
				r.setReservationDate(rset.getString("RESERVATIONDATE"));
				r.setId(rset.getString("ID"));
				r.setPaymentMethod("PAYMENTMETHOD");
				
				
				list.add(r);
				
			}
			
			System.out.println("쿼리문 종료 ");
			
	} catch (SQLException e) {
		
		e.printStackTrace();
	} finally {
		close(pstmt);
		close(rset);
	}
	
		return list;
}
		

	
	public int getListCount(Connection con) {
		
		Statement stmt = null;
		int listCount = 0;
		ResultSet rset = null;
		
		String query = prop.getProperty("listCount");
		
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
		
			
			
		} catch (SQLException e) {


			e.printStackTrace();
		}finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

}
