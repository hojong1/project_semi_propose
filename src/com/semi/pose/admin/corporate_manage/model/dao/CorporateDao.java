package com.semi.pose.admin.corporate_manage.model.dao;

import static com.semi.pose.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.semi.pose.admin.corporate_manage.model.vo.PageInfo;
import com.semi.pose.member.model.vo.CoMember;

public class CorporateDao {
	
	private Properties prop = new Properties();
	
	public CorporateDao() {
		String fileName = CorporateDao.class.getResource("/sql/member/member-query(seonjung).properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
	
	public int getListCounts(Connection con) {
		Statement stmt =  null;
		int listCounts = 0;
		ResultSet rset = null;
		
		String query = prop.getProperty("listCounts");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCounts = rset.getInt(1);
			}
			
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCounts;
	}

	public ArrayList<CoMember> selectListAndPaging(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<CoMember> list = null;
		
		String query = prop.getProperty("selectListAndPaging");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			//pstmt.setInt(1, startRow);
			//pstmt.setInt(2, endRow);
			
			System.out.println("dao 도착..");
			
			rset = pstmt.executeQuery();
			
			System.out.println("쿼리문 실행");
			
			list = new ArrayList<CoMember>();
			
			while(rset.next()) {
				CoMember c = new CoMember();
				
				c.setMembershipNumber(rset.getString("MEMBERSHIPNUMBER"));
				c.setPwd(rset.getString("PWD"));
				c.setCopId(rset.getString("ID"));
				c.setEmail(rset.getString("EMAIL"));
				c.setPhone(rset.getString("PHONE"));
				c.setRepresentativeName(rset.getString("REPRESENTATIVENAME"));
				c.setCopName(rset.getString("COPNAME"));
				
				
				
				list.add(c);
				
				
				
			}
			
			System.out.println("쿼리문종료");
			
			
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return list;
	}

}
