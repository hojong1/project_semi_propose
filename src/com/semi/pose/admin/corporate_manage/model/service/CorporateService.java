package com.semi.pose.admin.corporate_manage.model.service;

import static com.semi.pose.common.JDBCTemplate.close;
import static com.semi.pose.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;

import com.semi.pose.admin.corporate_manage.model.dao.CorporateDao;
import com.semi.pose.admin.corporate_manage.model.vo.PageInfo;
import com.semi.pose.member.model.vo.CoMember;

public class CorporateService {

	public int getListCount() {
		Connection con = getConnection();
		
		int listCounts = new CorporateDao().getListCounts(con);
		
		close(con);
		
		
		
		return listCounts;
	}

	public ArrayList<CoMember> selectListAndPaging(PageInfo pi) {
		Connection con = getConnection();
		
		System.out.println("dao로 전달");
		ArrayList<CoMember> list = new CorporateDao().selectListAndPaging(con,pi);
		
		close(con);
		
		
		return list;
	}

}
