package com.semi.pose.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet Filter implementation class CommonEncodingFilter
 */
@WebFilter("/*")
public class CommonEncodingFilter implements Filter {


    public CommonEncodingFilter() {
    	
    }


	public void destroy() {
		
	}


	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest hr = (HttpServletRequest) request;
		System.out.println("요청 방식 : " + hr.getMethod());
		if(hr.getMethod().equals("POST")) {
			request.setCharacterEncoding("UTF-8");
			System.out.println("인코딩 완료(필터처리)");
		}
		
		// 필터처리하는 부분
		chain.doFilter(request, response);
	}


	public void init(FilterConfig fConfig) throws ServletException {

	}

}
